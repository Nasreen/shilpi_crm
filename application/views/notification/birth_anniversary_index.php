<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="card-box table-responsive">
               <div class="dropdown pull-right">
               </div>
               <h4 class="header-title m-t-0 m-b-30"><?=@$head_title?></h4>
               <input type="hidden" name="type" id="type" value="<?=@$type?>">
               <table id="reminderTable" class="table table-bordered custdatatable table-responsive">
                   <thead>
                      <tr>
                       <th width="7" class="col4">#</th>
                       <th class="col4">Company Name</th>
                       <th class="col4">Name</th>
                       <th class="col4">Email Id</th>
                       <th class="col4">Mobile No</th>
                       <th class="col3">Event Date</th>
                    </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
