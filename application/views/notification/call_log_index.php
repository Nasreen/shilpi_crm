<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="card-box table-responsive">
               <div class="dropdown pull-right">
               </div>
               <h4 class="header-title m-t-0 m-b-30"><?=@$head_title?></h4>
               <form id="allcp-form" name="dateRangeForm">
               <div class="col-lg-12">
                    <div class="clearfix"></div>
                      <div class="m-b-30">
                        <input type="hidden" name="call" id="call" value="<?=@$call?>">
                       <!-- <input type="hidden" name="list" value="list">
                       <input type="hidden" name="draw" value="1"> -->
                       <div class="form-group col-sm-12 custommarbottam">
                          <div class="col-sm-2">
                           <input name="start_date" id="start_date" placeholder="Start Date" class="form-control dp" type="text" value="<?=@$start_date?>">
                          </div>
                          <div class="col-sm-2">
                           <input name="end_date" id="end_date" placeholder="End Date" class="form-control dp" type="text" value="<?=@$end_date?>">
                          </div>
                          <span class="text-danger" id="end_date_error"></span>
                          <div class="col-sm-2">
                            <input name="commit" value="Search" class="btn btn-primary" type="button" onclick="export_submit('get','notification/call_log')" id="search-input-click">
                          </div>
                          <div class="col-sm-2">
                           <?php $this->load->view('common/remove_filters'); ?>
                          </div>
                       </div>
                    </div>
                 </div>
                </form>
               <table id="callLogTable" class="table table-bordered custdatatable table-responsive">
                   <thead>
                      <tr>
                       <th width="7" class="col4">#</th>
                       <th class="col4">Company Name</th>
                       <th class="col4">Name</th>
                       <th class="col4">Email Id</th>
                       <th class="col4">Mobile No</th>
                       <th class="col3">Call Date</th>
                       <th class="col3">Follow Up Date</th>
                       <th class="col3">Description</th>
                       <th class="col3"></th>
                    </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
