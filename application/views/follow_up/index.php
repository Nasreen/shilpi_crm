<?php
  $target=get_target();
?>
<div class="content-page" id="CallVisit">
  <div class="content">
     <div class="container">
        <div class="row">
           <div class="col-sm-12">
             <form method="post"  class="form-horizontal" id="clientFilterForm" name="clientFilterForm" action="<?=ADMIN_PATH.'export_excel/export_client'?>">
              <div class="card-box table-responsive">
                 <div class="dropdown pull-right btnGroup">
                    <a href="<?=ADMIN_PATH?>client/create" class="btn btn-custom btn-bordred waves-effect waves-light btn-primary w-md m-b-5">ADD CLIENT</a> 
                    <a href="javascript:void(0);" class="btn btn-custom btn-bordred waves-effect waves-light btn-warning w-md m-b-5" onclick="document.getElementById('clientFilterForm').submit();">EXPORT CLIENT</a> 
                    <a href="<?=ADMIN_PATH?>client/import_excel" class="btn btn-custom btn-bordred waves-effect waves-light btn-info w-md m-b-5">IMPORT CLIENT</a> 
                 </div>
                 <h4 class="header-title m-t-0 m-b-30"><?=@$head_title?></h4>
                 <div class="col-lg-12">
                    <div class="clearfix"></div>
                      <div class="m-b-30">
                       <input type="hidden" name="list" value="list">
                       <div class="form-group col-sm-12 custommarbottam">
                          <div class="col-sm-2">
                              <select class="form-control" name="client[category]" id="category">
                                <option value="">Select Category</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                             </select>
                          </div>
                          <div class="col-sm-2">
                            <select class="form-control" name="client[target]" id="target">
                              <option value="">Select Target</option>
                              <?php foreach ($target as $tkey => $tvalue) { ?>
                                 <option value="<?=@$tkey?>"><?=@$tvalue?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="col-sm-2">
                             <select class="form-control select2 model_class" name="client[source]" id="source">
                                <option value="">Select Source</option>
                                <?php foreach ($source as $skey => $svalue) { ?>
                                   <option value="<?=@$svalue['id']?>"><?=@$svalue['name']?></option>
                                <?php } ?>
                             </select>
                          </div>
                          <div class="col-sm-2">
                             <select class="form-control select2 model_class" name="client[city]" id="city" data-placeholder="Select City..">
                                <option value="">Select City</option>
                                <?php foreach ($city as $ckey => $cvalue) { ?>
                                   <option value="<?=@$cvalue['city']?>"><?=@$cvalue['city']?></option>
                                <?php } ?>
                             </select>
                          </div>
                          <div class="col-sm-2">
                           <input name="client[name]" id="name" placeholder="Name" class="form-control" type="text">
                          </div>
                          <div class="col-sm-2">
                           <input name="client[mobile_no]" id="mobile_no" placeholder="Mobile No" class="form-control" type="text">
                          </div>
                       </div>
                       <div class="form-group col-sm-12 custommarbottam">
                          <div class="col-sm-6">
                             <input name="client[company]" id="company" placeholder="Company Name" class="form-control" type="text">
                          </div>
                          <div class="col-sm-2">
                             <input name="commit" value="Search" class="btn btn-primary" type="button" id="search-input-click">
                          </div>
                       </div>
                    </div>
                 </div>
                 <table id="clientTable" class="table table-bordered custdatatable table-responsive">
                     <thead>
                         <tr>
                          <th width="7" class="col4">#</th>
                          <th class="col4">Contact Person</th>
                          <th class="col4">City</th>
                          <th class="col4">State</th>
                          <th class="col4">Mobile No</th>
                          <th class="col4">Company</th>
                          <th class="col4">Call Description</th>
                          <th class="col4">Calls Date</th>
                          <th class="col4">Calls Count</th>
                          <th class="col3"></th>
                       </tr>
                     </thead>
                 </table>
                </div>
              </form>
           </div>
        </div>
     </div>
  </div>
</div>