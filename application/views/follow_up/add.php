<?php
   $visit_type=get_visit_type();
  $cancel_url=ADMIN_PATH.'client';
  if($from_page=='visited_customer'){
      $cancel_url=ADMIN_PATH.'visited_customer';
  }else if($from_page=='called_customer'){
      $cancel_url=ADMIN_PATH.'called_customer';
  }else if($from_page=='call_visit'){
      if(isset($v_id) && $v_id!=""){
         $id=$v_id;
         $funct_name='visit_status';
      }else{
         $id=$f_id;
         $funct_name='call_status';
      }
      $cancel_url=ADMIN_PATH.$funct_name.'/'.$client['id'].'/'.$id;
  }else if($from_page=='call_log'){
      $cancel_url=ADMIN_PATH.'notification/call_log';
  }
?>
<div class="content-page" id="CallVisitCreat">
   <!-- Start content -->
   <div class="content">
      <div class="container">
         <h4>Call/visit details</h4>
         <div class="row">
            <div class="col-lg-3 col-md-6">
               <div class="card-box themed-bg-blue">
                  <h4 class="header-title m-t-0 m-b-10 clrWhite">Client & Company Name :</h4>
                  <div class="widget-chart-1">
                     <p class="clrWhite">
                       <?=@$client['name']?> - <?=@$client['company']?>
                     </p>
                  </div>
               </div>
            </div>
            <!-- end col --
            <div class="col-lg-3 col-md-6">
               <div class="card-box themed-bg-success">
                  <h4 class="header-title m-t-0 m-b-10 clrWhite">Email :</h4>
                  <div class="widget-chart-1">
                     <p class="clrWhite">
                        <?=@$client['email_id']?>
                     </p>
                  </div>
               </div>
            </div>
            <!-- end col -->
            <div class="col-lg-3 col-md-6">
               <div class="card-box themed-bg-warning">
                  <h4 class="header-title m-t-0 m-b-10 clrWhite">Contact Details :</h4>
                  <div class="widget-chart-1">
                     <p class="clrWhite">
                        <b>Mobile No :</b> <?=@$client['mobile_no']?>
                     </p>
                  </div>                  
                  <div class="widget-chart-1">
                     <p class="clrWhite">
                        <b>Email id :</b> <?=@$client['email_id']?>
                     </p>
                  </div>
               </div>
            </div>
            <div class="col-lg-3 col-md-6">
               <div class="card-box themed-bg-danger">
                  <h4 class="header-title m-t-0 m-b-10 clrWhite">Address :</h4>
                  <div class="widget-chart-1">
                     <p class="clrWhite">
                        <?=@$client['address']?>
                     </p>
                  </div>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-12 m-b-10 m-t-20 radio">
               <input id="visit1" type="radio" name="visittype" checked="true">
               <label class="radio-inline mgL10" for="visit1"> Visit</label>

               <input id="visit2" type="radio" name="visittype">
               <label class="radio-inline" for="visit2"> Call</label>  
            </div>
            <div class="col-lg-4">
               <div class="card-box" id="Visit">
                  <h4 class="header-title m-t-0 m-b-10">Visit</h4>
                  <form class="visitForm" id="visitForm" accept-charset="UTF-8" method="post">
                     <input type="hidden" id="client_redirect_url" value="<?=$cancel_url?>">
                     <input value="<?=@$client['id']?>" name="visit[client_id]" id="visit_client_id" type="hidden">
                     <input type="hidden" name="v_id" id="v_id" value="<?=@$v_id?>">
                     <input value="Open" name="visit[status]" id="status" type="hidden">
                     <div class="form-group radio"> 

                        <input value="1" name="visit[visit_done]" id="visit_visit_done_true" type="radio" onchange="change_visitdate_label('Visit')" checked="true">
                        <label class="radio-inline" for="visit_visit_done_true"> Visit Done</label>

                        <input value="0" name="visit[visit_done]" id="visit_visit_done_false" type="radio" onchange="change_visitdate_label('Appointment')">
                        <label class="radio-inline" for="visit_visit_done_false"> Appointment</label>

                     </div>
                     <div>
                        <label for="pass1">Visit Type <span class="asterisk">*</span></label>
                        <select class="form-control" id="visit_type" name="visit[type]">
                           <option value="">Select Type</option>
                           <?php foreach ($visit_type as $v_key => $v_value) { ?>
                             <option value="<?=@$v_key?>"><?=@$v_value?></option>
                           <?php } ?>
                        </select>
                        <span class="text-danger" id="visit_type_error"></span>
                     </div>
                     <div class="form-group">
                        <label for="pass1"><span id="visit_date_label">Appointment</span> Date <span class="asterisk">*</span></label>
                        <input class="form-control dp" placeholder="Visit date" name="visit[visit_date]" id="visit_visit_date" type="text">
                        <span class="text-danger" id="visit_date_error"></span>
                     </div>
                      <div class="form-group">
                        <label for="pass1">Description</label>
                        <textarea class="form-control" placeholder="Description" name="visit[description]" id="visit_description"></textarea>
                     </div>
                     <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light new_cust_by_admin" type="button" onclick="save_visit('store_visit')">
                        Submit
                        </button>
                        <a class="btn btn-default waves-effect waves-light Montserrat-font add-contractor-cta" href="<?=@$cancel_url?>"><i class="ace-icon"></i> Cancel</a>
                     </div>
                  </form>
               </div>
               <div class="card-box" id="AddCall">
                  <h4 class="header-title m-t-0 m-b-10">Add Call</h4>
                  <form class="followupForm" id="followupForm" method="post">
                      <input type="hidden" name="f_id" id="f_id" value="<?=@$f_id?>">
                      <input value="<?=@$client['id']?>" name="follow_up[client_id]" id="follow_up_client_id" type="hidden">
                      <input value="Open" name="follow_up[status]" id="follow_up_status" type="hidden">
                     <div class="form-group">
                        <label for="pass1">Call Type</label>
                        <select class="form-control" id="type" name="follow_up[type]">
                           <option value="1">Call Done</option>
                           <option value="2">Appointment</option>
                        </select>
                     </div>
                     <div class="form-group">
                        <label for="pass1">Call/Appointment Date<span class="asterisk">*</span></label>
                        <input class="form-control dp" placeholder="Date"  name="follow_up[follow_up_date]" id="follow_up_follow_up_date" type="text">
                        <span class="text-danger" id="follow_up_date_error"></span>
                     </div>
                     <div class="form-group">
                        <label for="pass1">Description</label>
                        <textarea class="form-control" placeholder="Description" name="follow_up[description]" id="follow_up_description"></textarea>
                     </div>
                     <div class="form-group text-right m-b-0">
                        <button class="btn btn-primary waves-effect waves-light new_cust_by_admin" type="button" onclick="save_follow_up('store_follow_up')">
                        Submit
                        </button>
                        <a class="btn btn-default waves-effect waves-light Montserrat-font add-contractor-cta" href="<?=@$cancel_url?>"><i class="ace-icon"></i> Cancel</a>
                     </div>
                  </form>
               </div>
            </div>
            <!-- end col -->
            <div class="col-lg-8">
               <div class="">
                  <div class="panel">
                     <div class="panel-body">
                        <h4 class="header-title m-t-0 m-b-10">Visits and Calls</h4>
                        <div class="editable-responsive">
                           <div id="datatable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                              <div class="row">
                                 <div class="col-sm-6"></div>
                                 <div class="col-sm-6"></div>
                              </div>
                              <div class="row">
                                 <div class="col-sm-12">
                                    <table class="table table-bordered custdatatable table-responsive" id="visitTable">
                                       <thead>
                                          <tr role="row">
                                             <th>Description</th>
                                             <th>Visit / Call Date</th>
                                             <th>Type</th>
                                             <th>Visit</th>
                                             <th>Visit Type</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
