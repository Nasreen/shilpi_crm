<div class="content-page">
  <!-- Start content -->
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="ex-page-content text-center">
              <div class="text-error"><i class="fa fa-eye-slash"></i></div>
                <h3 class="text-uppercase font-600">Access Denied</h3>
                <p>You don't have a permission to access this page</p>
              </div>
            </div>
         </div>
      </div>
   </div>
</div>

