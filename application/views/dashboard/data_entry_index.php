<div class="content-page">
   <div class="content">
      <div class="container">
         <div class="row">
            <div class="col-sm-12 card-box-wrap">
               <div class="card-box padmobile dash-board mr-20">
                  <h4 class="header-title business_manager_header_title m-b-30 bmsmheader bmsmheaderabm">
                     <span>CLIENTS</span>
                  </h4>
                  <div class="row marg_bottom">
                     <div class="col-md-12 col-xs-12 no_pad">
                        <div class="row">
                           <div class="col-lg-3 col-md-3 col-sm-6">
                              <a href="<?=ADMIN_PATH.'client'?>">
                                 <div class="card-box dash-box box-shadow">
                                    <h4 class="header-title m-t-0 m-b-30">Total Clients</h4>
                                    <div class="widget-chart-1">
                                       <div class="widget-detail-1">
                                          <h2 class="number-size light-green-font"> <?=@$total_client['count']?> </h2>
                                          <span class="label label-gray">Clients</span>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <!-- end col -->
                           <?php
                              if(sizeof($category_wise_clients)>0){
                                 foreach ($category_wise_clients as $ckey => $cvalue) {
                            ?>
                           <div class="col-lg-3 col-md-3 col-sm-6">
                              <a href="<?=ADMIN_PATH.'client/index?category=A'?>">
                                 <div class="card-box dash-box box-shadow">
                                    <h4 class="header-title m-t-0 m-b-30">Category <?=@$cvalue['category']?> Clients</h4>
                                    <div class="widget-chart-1">
                                       <div class="widget-detail-1">
                                          <h2 class="number-size light-green-font">
                                             <?=@$cvalue['count']?>
                                          </h2>
                                          <span class="label label-gray">Clients</span>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <?php } } ?>
                           <!-- end col -->
                           <div class="col-lg-3 col-md-3 col-sm-6">
                              <a href="<?=ADMIN_PATH.'client/index?target=6'?>">
                                 <div class="card-box dash-box box-shadow">
                                    <h4 class="header-title m-t-0 m-b-30">Targeted Clients</h4>
                                    <div class="widget-chart-1">
                                       <div class="widget-detail-1">
                                          <h2 class="number-size light-green-font"> <?=@$targeted_client['count']?> </h2>
                                          <span class="label label-gray">Clients</span>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <!-- end col -->
                           <div class="col-lg-3 col-md-3 col-sm-6">
                              <a href="<?=ADMIN_PATH.'client/index?form=incomplete'?>">
                                 <div class="card-box dash-box box-shadow">
                                    <h4 class="header-title m-t-0 m-b-30">Incomplete Forms</h4>
                                    <div class="widget-chart-1">
                                       <div class="widget-detail-1">
                                          <h2 class="number-size light-green-font"> <?=@$incomplete_forms['count']?> </h2>
                                          <span class="label label-gray">Clients</span>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <!-- end col -->
                        </div>
                     </div>
                  </div>
                  <!-- Reminder Start -->
                  <h4 class="header-title business_manager_header_title m-b-30 bmsmheader bmsmheaderabm">
                        <span>REMINDER</span>
                  </h4>
                  <div class="row marg_bottom">
                     <div class="col-md-12 col-xs-12 no_pad">
                        <div class="row">
                           <div class="col-lg-3 col-md-3 col-sm-6">
                              <a href="<?=ADMIN_PATH.'notification/reminder?type=birth_date'?>">
                                 <div class="card-box dash-box box-shadow">
                                    <h4 class="header-title m-t-0 m-b-30">Birthday</h4>
                                    <div class="widget-chart-1">
                                       <div class="widget-detail-1">
                                          <h2 class="number-size light-blue-font"> <?=@$birthday_reminder['count']?> </h2>
                                          <span class="label label-gray">Clients</span>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <!-- end col -->
                           <div class="col-lg-3 col-md-3 col-sm-6">
                              <a href="<?=ADMIN_PATH.'notification/reminder?type=showroom_anniversary'?>">
                                 <div class="card-box dash-box box-shadow">
                                    <h4 class="header-title m-t-0 m-b-30">Anniversary</h4>
                                    <div class="widget-chart-1">
                                       <div class="widget-detail-1">
                                          <h2 class="number-size light-blue-font">
                                             <?=@$anniversary_reminder['count']?>
                                          </h2>
                                          <span class="label label-gray">Clients</span>
                                       </div>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <!-- end col -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- end content-page -->
