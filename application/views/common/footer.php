  <footer class="footer">
    <?=date('Y')?> © Shilpi CRM.
  </footer>
</div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
</div>
     <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="model_status">Scan product barcode</h4>
                </div>
                <div class="modal-body">
                   <!--  <h4>Text in a modal</h4> -->
                    <p id="model_status">Please confirm to change user's status.</p>
                </div>
                <div class="modal-footer">  
                    <button id="model_btn" type="button" class="btn btn-primary waves-effect waves-light">Confirm</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div id="myDeleteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="delete_model_status">Delete Record</h4>
                </div>
                <div class="modal-body">
                   <!--  <h4>Text in a modal</h4> -->
                    <p id="delete_model_status">You want to delete this record?</p>
                </div>
                <div class="modal-footer">  
                    <button id="delete_model_btn" type="button" class="btn btn-primary waves-effect waves-light">Confirm</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    </div>

    <div id="img_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                 <div class="row">
                         <a id="prev_img" class="col-md-2" style="margin-top:50%"><i class="fa fa-angle-double-left fa-5x" aria-hidden="true"></i></a>
                

                         <img id="modal_img_tag" src="" class="col-md-8">
                   

                        <a id="next_img" class="col-md-2" style="margin-top:50%"><i class="fa fa-angle-double-right fa-5x" aria-hidden="true"></i></a>
                  </div>
                 </div> 
              </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    



        <script>
            var resizefunc = [];
            var urls = '<?= ADMIN_PATH?>';
            var page_title='<?=@$page_title?>';
            var head_title='<?=@$head_title?>'
            var table_size='<?=@$table_size?>';
            var dataTable='';
            var randomnumber = Math.floor(Math.random() * (5000 - 1000 + 1)) + 1000;
            var page_form_title ='<?=@$page_form_title?>';
        </script>

        <!-- jQuery  -->
        <script src="<?=ADMIN_JS_PATH?>jquery.min.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery-ui-1.10.4.min.js"></script>
        <script src="<?=ADMIN_JS_PATH?>bootstrap.min.js"></script>
        <script src="<?=ADMIN_JS_PATH?>detect.js"></script>
        <script src="<?=ADMIN_JS_PATH?>fastclick.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.slimscroll.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.blockUI.js"></script>
        <script src="<?=ADMIN_JS_PATH?>waves.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.nicescroll.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.scrollTo.min.js"></script>

        <script src="<?=ADMIN_JS_PATH?>moment-with-locales.js"></script>
        
        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="<?=ADMIN_PLUGINS_PATH?>jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="<?=ADMIN_PLUGINS_PATH?>jquery-knob/jquery.knob.js"></script>

        <!-- Datatables-->
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/jquery.dataTables.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.bootstrap.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.buttons.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/buttons.bootstrap.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/jszip.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/pdfmake.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/vfs_fonts.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/buttons.html5.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/buttons.print.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.fixedHeader.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.keyTable.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.responsive.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/responsive.bootstrap.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datatables/dataTables.scroller.min.js"></script>
        <!-- mcsrollbar js -->
        <script src="<?=ADMIN_JS_PATH?>jquery.mCustomScrollbar.concat.min.js"></script>
        
        <!-- datepicker -->
        <script src="<?=ADMIN_PLUGINS_PATH?>bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>timepicker/bootstrap-timepicker.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>datetimepicker/build/jquery.datetimepicker.full.js"></script>
       
        <!-- select2 js -->
        <script src="<?=ADMIN_PLUGINS_PATH?>select2/dist/js/select2.min.js" type="text/javascript"></script>

        <!-- Datatable init js -->
        <script src="<?=ADMIN_PAGES_PATH?>datatables.init.js"></script>
        
        <!-- chosen -->
        <script src="<?=ADMIN_PLUGINS_PATH?>chosen_v1.7.0/chosen.jquery.js" type="text/javascript"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>chosen_v1.7.0/docsupport/init.js" type="text/javascript"></script>
        <!-- Toastr js -->
        <script src="<?=ADMIN_PLUGINS_PATH?>toastr/toastr.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>bootstrap-sweetalert/sweet-alert.min.js"></script>
        <!-- App js -->
        <!-- Modal-Effect -->
        <script src="<?=ADMIN_PLUGINS_PATH?>custombox/dist/custombox.min.js"></script>
        <script src="<?=ADMIN_PLUGINS_PATH?>custombox/dist/legacy.min.js"></script>
        
	<!-- Common Function Js-->
        <script src="<?=ADMIN_JS_PATH?>common.js?<?=time()?>"></script>
        <script src="<?=ADMIN_JS_PATH?>forms.js?<?=time()?>"></script>
        <script src="<?=ADMIN_JS_PATH?>formTable.js?<?=time()?>"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.core.js"></script>
        <script src="<?=ADMIN_JS_PATH?>jquery.app.js?<?=time()?>"></script>


        <script type="text/javascript">
            $(window).load(function(){
            if ($(window).width() < 768) {
               setTimeout(function(){
                    $('#datatable').wrapAll('<div>');
                    $("#datatable").parent().addClass("table-responsive");
                }, 500);
            }
            
        });

$(document).ready(function(){

    if($(window).width()>767){

        var check=true;
        $("#ExpandMenu").on("click", function(){
            if(check==true){
                $(".left.side-menu").animate({width: '50px'});
                $("div.content-page").animate({marginLeft: '50px'});
                $(".list-unstyled").css('display', 'none');
                $('.topbar .topbar-left').animate({width:'75px', height:'75px', marginLeft: '-60px'});
                $('.topbar .topbar-left a img.logo').animate({height:'30px', marginTop: '17px'});
                $("#sidebar-menu ul li a.waves-effect").animate({left:'-250px'}).hide(); 
                $("ul li.menu_border").children("ul.list-unstyled").addClass("hoverSubmenu");
                $("ul li.menu_border").addClass("hoverSubmenuPatents");
                $("body").addClass("fullscreen");
                $(".slimScrollDiv").css("overflow", "visible");
               $(".left.side-menu").css("overflow", "visible");
               $(".sidebar-inner.slimscrollleft").css("overflow", "visible");                         
                /*-------Sub Menu -------*/
                check=false;
            }else{
                check=true;                            
                $(".left.side-menu").animate({width: '250px'});
                $("div.content-page").animate({marginLeft: '250px'});                            
                $('.topbar .topbar-left').animate({width:'185px', height:'75px', marginLeft: '0px'});
                $('.topbar .topbar-left a img.logo').animate({height:'55px', marginTop: '0px'});
                $("#sidebar-menu ul li a.waves-effect").animate({left:'0px'}).fadeIn(30);
                $("ul li.menu_border").children("ul.list-unstyled").removeClass("hoverSubmenu");
                $("ul li.menu_border").removeClass("hoverSubmenuPatents");
                $(".slimScrollDiv").css("overflow", "hidden");
                $(".left.side-menu").css("overflow", "hidden");
                $(".sidebar-inner.slimscrollleft").css("overflow", "hidden");
                $("body").removeClass("fullscreen");
                };                
                localStorage.setItem('favs', check); 
        });
        
        if (localStorage.getItem('favs')){
            var ssss = localStorage.getItem("favs");
                if (ssss=="false") {
                $(".left.side-menu").animate({width: '50px'});
                $("div.content-page").animate({marginLeft: '50px'});
                $(".list-unstyled").css('display', 'none');
                $('.topbar .topbar-left').animate({width:'75px', height:'75px', marginLeft: '-60px'});
                $('.topbar .topbar-left a img.logo').animate({height:'30px', marginTop: '17px'});
                $("#sidebar-menu ul li a.waves-effect").animate({left:'-250px'}).hide(); 
                $("ul li.menu_border").children("ul.list-unstyled").addClass("hoverSubmenu");
                $("ul li.menu_border").addClass("hoverSubmenuPatents");
                $("body").addClass("fullscreen");
                $(".slimScrollDiv").css("overflow", "visible");
                $(".left.side-menu").css("overflow", "visible");
                $(".sidebar-inner.slimscrollleft").css("overflow", "visible");
                check=false;
            }
            
        }   
    }

    
});

$(document).ready(function(){
    $("#AddCall").hide();

    $("#visit2").click(function(){
        $("#AddCall").show() && $("#Visit").hide();
    });
    $("#visit1").click(function(){
        $("#Visit").show() && $("#AddCall").hide();
    });
});




        </script>
    </body>
