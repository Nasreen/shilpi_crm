<body class="fixed-left">
 	<!-- preloader start-->
 	<div class="preloader">
		<div class="loading">
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		</div>
	</div>
	<!-- preloader end-->

	<!-- preloader start-->
 	<div class="ajaxLoader">
		<div class="loading">
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		  <div class="loading-bar"></div>
		</div>
	</div>
	<!-- preloader end-->

	<!-- Begin page -->
	<div id="wrapper">
		<!-- Top Bar Start -->
		<div class="topbar">			
			<!-- Button mobile view to collapse sidebar menu -->
			<div class="navbar navbar-default navbar-fixed-top ams-navbar-dashboard" role="navigation">
				<div class="container">
				  	<a type="button"  class="navbar-toggle pull-left hmenuicon" >
			       	<i id="hmenuicon" class="zmdi zmdi-menu"></i>
			       	<i id="hmenuiconClose" class="zmdi zmdi-close" style="display: none;"></i>
			      	</a>
					<!-- LOGO -->
					<div class="topbar-left navbar-header ams-dashboard-topbar-left logoBox">
						<a href="<?=ADMIN_PATH?>"><img src="<?= ADMIN_PATH?>assets/images/logo.png" alt="logo" class="logo desktopLogo"></a>
					<!-- 	<a href="<?=ADMIN_PATH?>" class="logo desktopLogo"><img src="<?=ADMIN_IMAGES_PATH ?>logo.jpg" /></a>
						<a href="<?=ADMIN_PATH?>" class="logo ipadLogo"><img src="<?=ADMIN_IMAGES_PATH ?>logo1.jpg" /></a> -->
						<h4 class="page-title" style="font-size: 25px; color:#139fda"></h4>
					</div>
						<a class="ExpandMenu" id="ExpandMenu" href="javascript:void(0)"><i class="ti-menu"></i></a>
					 <ul class=" nav navbar-nav navbar-right logOutDiv"> 
						<li><a class="ams-header-logout-link" href="javascript:void(0);">
						<i class="zmdi zmdi-settings"></i></a></li>
					 	<!-- <li>
						 <a href="javascript:void(0);" class="right-bar-toggle notification_bell_wrap">
							<i class="zmdi zmdi-notifications-none"></i>
							<span id="not_count"><span id="notification_count">0</span></span></span>
						 </a>
						</li> -->			
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="profile-text">Hello: <?=@$this->session->userdata('name'); ?></span> <b class="caret"></b></a> 
							<ul class="dropdown-menu profile-log navbar-right">
								<li><a class="ams-header-logout-link" href="<?=ADMIN_PATH?>auth/logout">
                    				<i class="zmdi zmdi-sign-in"></i> Log out</a>
                    			</li>
                    			<li>		<div class="divider"></div>
                    					<a href="javascript:void(0)">USER ID: <?=@$this->session->userdata('userid')?></a>
                					 
            					</li>
        					</ul>
    					</li>						
					 </ul> 
					<!-- Page title -->
					<div class="bedcrumb_header"></div>										
				</div>				
				<!-- end container -->
			</div>
			<!-- end navbar -->
		</div>
		<!-- Top Bar End -->
		<!-- ========== Left Sidebar Start ========== -->
		<div id="sidbarLeft" class="left side-menu <?php if($page_title=='Dashboard'){echo 'dashboard-side-menu';}?>">
			<div class="sidebar-inner slimscrollleft">
				<div id="sidebar-menu">
					<ul>
						<?php
							if(sizeof($navigation)>0){
								foreach($navigation as $key => $value) {
									$active_class="";
									$has_sub="";
									$page = ADMIN_PATH.$value[0]['name'];
									$page_name =$this->uri->segment(1);
									$user_modals = array_map(function ($ar) {return $ar['name'];}, $value);
									if(sizeof($value)>1){
										$has_sub="has_sub";
										$page = "javascript:void(0);";
									}
									if(in_array($page_name, $user_modals)){
							 			$active_class ="subdrop";
									}
						?>
							<li class="menu_border <?=$has_sub?>">
								
									<span class="iconBox"><?=@$value[0]['icon']?></span>
								
								<a href="<?=@$page?>" class="waves-effect <?=$active_class?>">
									<span><?=@$key?></span>
									<?php if($has_sub!=""){ ?> 
										<span class="menu-arrow"></span>
									<?php } ?>
								</a>
								<?php if($has_sub!=""){ ?>  
								 	<ul class="list-unstyled" <?php if($active_class!=""){ ?>style="display:block;" <?php } ?>>
								 		<?php foreach($value as $sub_key => $sub_value) { ?>
		                            	<li class="<?php echo ($page_name == $sub_value['name']) ? 'active' : ''?>">
											<a href="<?= ADMIN_PATH.$sub_value['name']?>"><?=$sub_value['display_name']?> </a>
										</li>
										<?php } ?>
	                        		</ul>
								<?php } ?>
								<!-- <a href="<?=ADMIN_PATH.$value[0]['name']?>" class="waves-effect <?=($this->uri->segment(1)==$value[0]['name'])? 'active':''?>">	<span><?=@$value[0]['display_name']?></span>
								</a> -->
							</li>
						<?php	
								}
							}
							if(isset($_SESSION['user_role']) && $_SESSION['user_role']=='Admin'){
						?>
							<li class="menu_border">							   
							   		<span class="iconBox">M</span>
							   <a href="<?=ADMIN_PATH.'manage_user'?>" class="waves-effect <?=($this->uri->segment(1)=='manage_user')? 'active':''?>">
							   		<span>Manage Users</span>
							   </a>
							</li>
						<?php } ?>
					</ul>
					<div class="clearfix"></div>
				</div>
				<!-- Sidebar -->
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="side-bar right-bar">
				<a href="javascript:void(0);" class="right-bar-toggle" id="notification_close">
					<i class="zmdi zmdi-close-circle-o"></i>
				</a>

				<h4 class="">Notifications</h4>
				<div class="notification-list nicescroll">
					<ul class="list-group list-no-border user-list" id="notificationList">
						<div id="updated_notif"></div>						 
						<li></li>
					</ul>
				</div>
		</div>
	</div>
	<div id="fixBack" style="display: none;"></div>
	<script type="text/javascript">
	$(document).ready(function(){
	    $("select").change(function(){
	        $(this).find("option:selected").each(function(){
	            if($(this).attr("value")=="red"){
	                $(".box").not(".red").hide();
	                $(".red").show();
	            }
	            else if($(this).attr("value")=="green"){
	                $(".box").not(".green").hide();
	                $(".green").show();
	            }
	            else if($(this).attr("value")=="blue"){
	                $(".box").not(".blue").hide();
	                $(".blue").show();
	            }
	            else if($(this).attr("value")=="black"){
	                $(".box").not(".black").hide();
	                $(".black").show();
	            }
	            else{
	                $(".box").hide();
	            }
	        });
	    }).change();   
	 
	});
	</script>

