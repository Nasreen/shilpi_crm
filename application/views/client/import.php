<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=@$head_title?></span>
               </h4>
               <div class="col-lg-12">
                  <div class="alert alert-success">
                    <h4>Note:</h4>
                   <p><i>For Start Date,Showroom Anniversary & Birth Date fields - format should be <strong>dd-mm-YYYY(12-08-2015)</strong></i></p>
                   <p><i>For Target field - Enter the values between 0 to 6:</p>
                    <span>0 = For Not Contacted</span>,
                    <span>1 = For Attempted to Contact</span>,
                    <span>2 = For Contact in Future</span>,
                    <span>3 = For Contacted</span>,
                    <span>4 = For Junk Lead</span>,
                    <span>5 = For Lost Lead</span> &
                    <span>6 = For Lead</span></i>
                  </div>
               </div>
               <div class="col-lg-12">
                  <form name="importClientForm" id="importClientForm" role="form" class="form-horizontal" method="post">
                    <!-- <div class="form-group"> -->
                       <a class="download form-group margin-5" href="<?=ADMIN_PATH.'export_excel/export_sample/clients.xlsx'?>" id="import-article">Download Sample</a>
                     <!-- </div> -->
                    <div class="form-group">
                      <p class="text-danger errors"></p>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Upload Xlsx <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="file" id="file" class="form-control" name="file">
                           <span class="text-danger" id="file_error"></span>
                        </div>
                     </div>
                    <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                         <button class="btn btn-primary waves-effect waves-light btn-bordred btn-lg" name="commit" type="button" onclick="save_import_client('store_client'); ">
                         SAVE
                         </button>
                         <a href="<?=ADMIN_PATH.'client'?>" class="btn btn-default waves-effect waves-light m-l-5 btn-lg" type="reset">
                           CANCEL
                           </a>
                     </div>
                  </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>