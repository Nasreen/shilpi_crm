<?php
  $target =get_target();
  $cancel_url=ADMIN_PATH.'client';
?>
<div class="content-page">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box padmobile">
            <h4 class="header-title m-t-0 m-b-30">Add Client</h4>
            <div class="set_pripary" style="float:right"></div>
            <div class="row">
               <div class="form-horizontal">
                  <form class="new_client" name="clientFrom" id="clientFrom" method="post">
                    <input value="Open" name="client[lead_status]" id="client_lead_status" type="hidden">
                    <input type="hidden" id="client_redirect_url" value="<?=$cancel_url?>">
                   <!--  <input value="true" name="client[customer_form]" id="client_customer_form" type="hidden"> -->
                     <input type="hidden" id="id" name="id">
                     <div class="col-lg-12">
                        <div class="panel panel-default">
                           <div class="panel-body">
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Company Name <span class="asterisk">*</span></label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Company Name" name="client[company]" id="company" type="text">
                                          <span class="text-danger" id="company_error"></span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Start Date</label>
                                       <div class="col-md-8">
                                          <input class="form-control dp" placeholder="Start Date" name="client[start_date]" id="start_date" type="text" readonly>
                                          <span class="text-danger" id="start_date_error"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Category<span class="asterisk">*</span></label>
                                       <div class="col-md-8">
                                          <select class="form-control" name="client[category]" id="category">
                                             <option value="">Select Category</option>
                                             <?php if(sizeof($categories)>0){
                                                foreach ($categories as $ckey => $cvalue) {
                                             ?>
                                                <option value="<?=@$cvalue['name']?>"><?=@$cvalue['name']?></option>
                                             <?php } } ?>
                                          </select>
                                          <span class="text-danger" id="category_error"></span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Name <span class="asterisk">*</span></label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="First Name" name="client[name]" id="name" type="text">
                                          <span class="text-danger" id="name_error"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Designation</label>
                                       <div class="col-md-8">
                                          <select class="form-control" placeholder="Designation" name="client[designation]" id="designation">
                                             <option value="">Select Designation</option>
                                             <?php if(sizeof($designation)>0){
                                                foreach ($designation as $dkey => $dvalue) {
                                             ?>
                                                <option value="<?=$dvalue['id']?>"><?=$dvalue['name']?></option>
                                             <?php        
                                                   }
                                                } 
                                             ?>
                                          </select>
                                          <span class="text-danger" id="designation_error"></span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Email Id<span class="asterisk">*</span></label>
                                       <div class="col-md-8">
                                          <input class="form-control " placeholder="Email" name="client[email_id]" id="client_email" type="text">
                                          <span class="text-danger" id="email_id_error"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Mobile No.<span class="asterisk">*</span></label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Mobile Number" name="client[mobile_no]" id="client_phone_no" type="text" maxlength="10" maxlength="10">
                                          <span class="text-danger" id="mobile_no_error"></span>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Target <!-- <span class="asterisk">*</span> --></label>
                                       <div class="col-md-8">
                                          <select id="target" name="client[target]" class="form-control">
                                             <option value="">Select Target</option>
                                             <?php foreach ($target as $tkey => $tvalue) {
                                             ?>
                                                <option value="<?=@$tkey?>"><?=@$tvalue?></option> 
                                           <?php } ?>
                                          </select>
                                          <span class="text-danger" id="target_error"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">1. Person Details</div>
                           <div class="panel-body">
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Name</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Name" name="client[person_1_name]" id="client_person_name_1" type="text">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Mobile No.</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Mobile Number" name="client[person_1_mobile_no]" id="client_person_1_mobile" type="text" maxlength="10">
                                          <span class="text-danger" id="person_1_mobile_no_error"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Designation</label>
                                       <div class="col-md-8">
                                          <select class="form-control" placeholder="Designation" name="client[person_1_designation]" id="person_1_designation">
                                             <option value="">Select Designation</option>
                                             <?php if(sizeof($designation)>0){
                                                foreach ($designation as $dkey => $dvalue) {
                                             ?>
                                                <option value="<?=$dvalue['id']?>"><?=$dvalue['name']?></option>
                                             <?php        
                                                   }
                                                } 
                                             ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Email Id</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Email" name="client[person_1_email_id]" id="client_person_1_email" type="text">
                                          <span class="text-danger" id="person_1_email_id_error"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                  <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Birth Date</label>
                                       <div class="col-md-8">
                                          <input class="form-control dp" placeholder="Birth Date" name="client[person_1_birth_date]" id="person_1_birth_date" type="text" readonly>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">2. Person Details</div>
                           <div class="panel-body">
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Name</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Name" name="client[person_2_name]" id="client_person_name_2" type="text">
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Mobile No.</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Mobile Number" name="client[person_2_mobile_no]" id="client_person_2_mobile" type="text" maxlength="10">
                                          <span class="text-danger" id="person_2_mobile_no_error"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Designation</label>
                                       <div class="col-md-8">
                                          <select class="form-control" placeholder="Designation" name="client[person_2_designation]" id="person_2_designation">
                                             <option value="">Select Designation</option>
                                             <?php if(sizeof($designation)>0){
                                                foreach ($designation as $dkey => $dvalue) {
                                             ?>
                                                <option value="<?=$dvalue['id']?>"><?=$dvalue['name']?></option>
                                             <?php        
                                                   }
                                                } 
                                             ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Email Id</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Email" name="client[person_2_email_id]" id="client_person_2_email" type="text">
                                          <span class="text-danger" id="person_2_email_id_error"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                  <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Birth Date</label>
                                       <div class="col-md-8">
                                          <input class="form-control dp" placeholder="Birth Date" name="client[person_2_birth_date]" id="person_2_birth_date" type="text" readonly>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">3. Address Details</div>
                           <div class="panel-body">
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Address</label>
                                       <div class="col-md-8">
                                          <textarea class="form-control" placeholder="Address" name="client[address]" id="client_address"></textarea>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <!-- <div class="form-group">
                                       <label class="col-md-3 control-label">Area</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Area" name="client[area]" id="client_area" type="text">
                                       </div>
                                    </div> -->
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Country</label>
                                       <div class="col-md-8">
                                          <select class="form-control" name="client[country]" id="client_country" type="text" onchange="getStateByCountry(this)">
                                             <option value="">Select Country</option>
                                             <?php foreach ($country as $country_key => $country_value) { ?>
                                                <option value="<?=$country_value['name']?>"><?=$country_value['name']?></option>
                                             <?php } ?>
                                          </select>
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">State</label>
                                       <div class="col-md-8">
                                          <select class="form-control" name="client[state]" id="client_state" type="text" onchange="getCityByState(this)">
                                             <option value="">Select State</option>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 clear">
                                 <div class="form-group">
                                    <label class="col-md-3 control-label">City</label>
                                    <div class="col-md-8">
                                       <select class="form-control" name="client[city]" id="client_city" type="text">
                                          <option value="">Select City</option>
                                       </select>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">                                    
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Pincode</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Pincode" name="client[pincode]" id="client_pincode" type="text" maxlength="6">
                                          <span class="text-danger" id="pincode_error"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Landline 1</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Landline 1" name="client[landline_no1]" id="client_landline_no1" type="text">
                                          <span class="text-danger" id="landline_no1_error"></span>
                                       </div>
                                    </div>                                    
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="form-group">
                                    <label class="col-md-3 control-label">Landline 2</label>
                                    <div class="col-md-8">
                                       <input class="form-control" placeholder="Landline 2" name="client[landline_no2]" id="client_landline_no2" type="text">
                                       <span class="text-danger" id="landline_no2_error"></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">4. Other Details</div>
                           <div class="panel-body">
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Birth Date</label>
                                       <div class="col-md-8">
                                          <input class="form-control dp" placeholder="Birth Date" name="client[birth_date]" id="client_birth_date" type="text" readonly>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Showroom Anniversary</label>
                                       <div class="col-md-8">
                                          <input class="form-control dp" placeholder="Showroom Anniversary" name="client[showroom_anniversary]" id="client_showroom_aniversary" type="text" readonly>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Source</label>
                                       <div class="col-md-8">
                                          <select class="form-control" name="client[source]" id="client_lead_source">
                                             <option value="">Select Source</option>
                                             <?php if(sizeof($source)>0){
                                                foreach ($source as $skey => $svalue) {
                                              ?>
                                                <option value="<?=@$svalue['id']?>"><?=@$svalue['name']?></option>
                                              <?php  
                                                  }
                                                } 
                                              ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Key A/c Manager</label>
                                       <div class="col-md-8">
                                          <select class="form-control" name="client[key_ac_manager]" id="client_key_ac_manager">
                                             <option value="">Select User</option>
                                             <?php if(sizeof($key_manager)>0){
                                                foreach ($key_manager as $kkey => $kvalue) {
                                              ?>
                                             <option value="<?=@$kvalue['id']?>"><?=@$kvalue['name']?></option>
                                             <?php  
                                                  }
                                                } 
                                              ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Referred By 1</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Referred By 1" name="client[referred_by_1]" id="referred_by_1" type="text">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Referred Mobile No.1</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Referred Mobile No 1" name="client[referred_mobile_no_1]" id="referred_mobile_no_1" type="text" maxlength="10">
                                          <span class="text-danger" id="referred_mobile_no_1_error"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Referred By 2</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Referred By 2" name="client[referred_by_2]" id="referred_by_2" type="text">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="clearfix">
                                    <div class="form-group">
                                       <label class="col-md-3 control-label">Referred Mobile No.2</label>
                                       <div class="col-md-8">
                                          <input class="form-control" placeholder="Referred Mobile No 2" name="client[referred_mobile_no_2]" id="referred_mobile_no_2" type="text" maxlength="10">
                                          <span class="text-danger" id="referred_mobile_no_2_error"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="clearfix">
                        <div class="form-group text-center m-b-0 sub-button">
                           <button class="btn btn-primary waves-effect waves-light new_cust_by_admin" type="button" onclick="save_client('store')">
                           Submit
                           </button>
                           <a class="btn btn-default waves-effect waves-light Montserrat-font add-contractor-cta" href="<?=ADMIN_PATH.'client'?>"><i class="ace-icon"></i> Cancel</a>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>