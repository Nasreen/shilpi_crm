<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=@$head_title?></span>
               </h4>
               <div class="col-lg-12">
                  <form name="emailClientForm" id="emailClientForm" role="form" class="form-horizontal" method="post">
                    <input type="hidden" id="filter_type" name="filter_type" value="1">
                     <!-- <input type="hidden" name="send_to_all" id="send_to_all" value> -->
                     <input type="hidden" id="validation_flag" value="1">
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Subject <span class="asterisk">*</span></label>
                        <div class="col-sm-3">
                           <input type="text" id="subject" class="form-control" name="subject">
                           <span class="text-danger" id="subject_error"></span>
                        </div>
                        <label class="col-sm-2 control-label" for="inputEmail3"> Attachment </label>
                        <div class="col-sm-3">
                           <input type="file" id="file" class="form-control" name="file"></textarea>
                           <span class="text-danger" id="file_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3"> Email Body <span class="asterisk">*</span></label>
                        <div class="col-sm-8">
                           <textarea id="email_body" class="form-control" name="email_body"></textarea>
                           <span class="text-danger" id="email_body_error"></span>
                        </div>
                     </div>
                     <div class="form-group custommarbottam">
                        <div class="col-sm-2">
                          <input name="client[company]" id="company" placeholder="Company Name" class="form-control" type="text">
                        </div>
                        <div class="col-sm-2">
                          <input name="client[name]" id="name" placeholder="Contact Person" class="form-control" type="text">
                        </div>
                        <div class="col-sm-2">
                          <input name="client[mobile_no]" id="mobile_no" placeholder="Mobile No" class="form-control" type="text" maxlength="10">
                         <span class="text-required text-danger" id="mobile_no_error"></span>
                        </div>
                        <div class="col-sm-2">
                           <select class="form-control select2 model_class" name="client[state]" id="states" data-placeholder="Select State.." onchange="getCityByState(this)">
                              <option value="">Select State</option>
                              <?php foreach ($states as $stkey => $stvalue) { ?>
                                 <option value="<?=@$stvalue['name']?>"><?=@$stvalue['name']?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-sm-2">
                           <select class="form-control select2 model_class" name="client[city]" id="city" data-placeholder="Select City..">
                              <option value="">Select City</option>
                           </select>
                        </div>
                        <div class="col-sm-2">
                           <input name="commit" value="Search" class="btn btn-primary" type="button" id="search-input-click">
                        </div>
                        <span class="text-required text-danger" id="search_error"></span>
                     </div>
                     <table class="table table-bordered" id="clientEmailTable">
                       <thead>
                          <tr>
                            <th width="7">#</th>
                            <th>Clients</th>
                            <th>
                              <div class="form-group">
                                <div class="checkbox checkbox-primary">
                                  <input name="select_all" id="select_all" type="checkbox"><label for="select_all">Select All Clients</label>
                                </div>
                              </div>
                            </th>
                          </tr>
                       </thead>
                     </table>
                     <p>
                       <span class="text-danger text-required" id="client_error"></span>
                     </p>
                    <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                         <button class="btn btn-primary waves-effect waves-light btn-bordred btn-lg" name="commit" type="button" onclick="save_client_email('send_client_email'); ">
                         SEND EMAIL
                         </button>
                         <a href="<?=ADMIN_PATH.'client'?>" class="btn btn-default waves-effect waves-light m-l-5 btn-lg" type="reset">
                           CANCEL
                           </a>
                     </div>
                  </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>