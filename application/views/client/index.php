<?php
  $target=get_target();
?>
<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
           <form method="post"  class="form-horizontal" id="clientFilterForm" name="clientFilterForm" action="<?=ADMIN_PATH.'export_excel/export_client'?>">
            <div class="card-box padmobile ">
               <div class="dropdown pull-right btnGroup">
                  <a href="<?=ADMIN_PATH?>client/create" class="btn waves-effect waves-light btn-primary w-md m-b-5">ADD CLIENT</a> 
                  <?php if(isset($_SESSION['user_role']) && $_SESSION['user_role']=='Admin'){ ?>
                  <a href="javascript:void(0);" class="btn waves-effect waves-light btn-warning w-md m-b-5" onclick="document.getElementById('clientFilterForm').submit();">EXPORT CLIENT</a> 
                  <?php } ?>
                  <a href="<?=ADMIN_PATH?>client/import_excel" class="btn waves-effect waves-light btn-info w-md m-b-5">IMPORT CLIENT</a>
                  <?php if(isset($_SESSION['user_role']) && $_SESSION['user_role']=='Admin'){ ?>
                  <a href="<?=ADMIN_PATH?>client/send_email" class="btn  waves-effect waves-light btn-danger w-md m-b-5">SEND EMAIL</a>
                  <?php } ?>
               </div>
               <h4 class="header-title m-t-0 m-b-30"><?=@$head_title?></h4>
                <div class="col-lg-12">
                  <div class="clearfix"></div>
                    <div class="m-b-30">
                     <input type="hidden" name="list" value="list">
                     <input type="hidden" id="filter_type" name="filter_type" value="1">
                     <input type="hidden" name="target_filter" id="target_filter" value="<?=@$target_filter?>">
                     <input type="hidden" name="category_filter" id="category_filter" value="<?=@$category_filter?>">
                     <input type="hidden" name="call_filter" id="call_filter" value="<?=@$call_filter?>">
                     <input type="hidden" name="form_filter" id="form_filter" value="<?=@$form_filter?>">
                     <input type="hidden" id="validation_flag" value="1">
                     <div class="form-group custommarbottam m-t-10">
                        <div class="row">
                          <div class="col-sm-2">
                            <select class="form-control" name="client[category]" id="category">
                              <option value="">Select Category</option>
                              <?php if(sizeof($categories)>0){
                                  foreach ($categories as $ckey => $cvalue) {
                              ?>
                                  <option value="<?=@$cvalue['name']?>" <?=($category_filter==$cvalue['name']) ? 'selected' : ''?>><?=@$cvalue['name']?></option>
                              <?php } } ?>
                           </select>
                        </div>
                        <div class="col-sm-2">
                          <select class="form-control" name="client[target]" id="target">
                            <option value="">Select Target</option>
                            <?php foreach ($target as $tkey => $tvalue) { ?>
                               <option value="<?=@$tkey?>" <?=($target_filter==6) ? 'selected' : ''?>><?=@$tvalue?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="col-sm-2">
                           <select class="form-control select2 model_class" name="client[source]" id="source">
                              <option value="">Select Source</option>
                              <?php foreach ($source as $skey => $svalue) { ?>
                                 <option value="<?=@$svalue['id']?>"><?=@$svalue['name']?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-sm-2">
                           <select class="form-control select2 model_class" name="client[state]" id="states" data-placeholder="Select State.." onchange="getCityByState(this)">
                              <option value="">Select State</option>
                              <?php foreach ($states as $stkey => $stvalue) { ?>
                                 <option value="<?=@$stvalue['name']?>"><?=@$stvalue['name']?></option>
                              <?php } ?>
                           </select>
                        </div>
                        <div class="col-sm-2">
                           <select class="form-control select2 model_class" name="client[city]" id="city" data-placeholder="Select City..">
                              <option value="">Select City</option>
                           </select>
                        </div>
                        <div class="col-sm-2">
                         <input name="client[name]" id="name" placeholder="Name" class="form-control" type="text">
                        </div>
                        <!-- <div class="col-sm-2">
                         <input name="client[mobile_no]" id="mobile_no" placeholder="Mobile No" class="form-control" type="text" maxlength="10">
                         <span class="text-required text-danger" id="mobile_no_error"></span>
                        </div> -->
                        </div>
                     </div>
                      <div class="form-group custommarbottam">
                        <div class="row">
                          <div class="col-sm-6">
                           <input name="client[company]" id="company" placeholder="Company Name" class="form-control" type="text">
                          </div>
                          <div class="col-sm-2">
                             <input name="commit" value="Search" class="btn btn-primary" type="button" id="search-input-click">
                          </div>
                          <span class="text-required text-danger" id="search_error"></span>
                        </div>
                      </div>
                    </div>
                </div>
              <!--Report Filter -->
               <div class="col-lg-12">
                  <div class="clearfix"></div>
                    <div class="m-b-30">
                     <div class="form-group custommarbottam">
                        <div class="row">
                          <div class="col-sm-2">
                            <select class="form-control" name="report[type]" id="report_type">
                              <option value="">Select Report Type</option>
                              <option value="1">Called Customers</option>
                              <option value="2">Visited Customers</option>
                           </select>
                        </div>
                        <div class="col-sm-2">
                         <input name="report[start_date]" id="start_date" placeholder="Start Date" class="form-control dp" type="text">
                        </div>
                        <div class="col-sm-2">
                          <input name="report[end_date]" id="end_date" placeholder="End Date" class="form-control dp" type="text" maxlength="10">
                          <span class="text-required text-danger" id="end_date_error"></span>
                        </div>
                        <div class="col-sm-2">
                           <input name="commit" value="Go" class="btn btn-primary" type="button" id="report-search-input-click">
                        </div>
                        <span class="text-required text-danger" id="report_error"></span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                 <div class="col-lg-12">
                   <div class="table-responsive">
                   <table id="clientTable" class="table table-bordered custdatatable">
                       <thead>
                           <tr>
                            <th width="7" class="col4"></th>
                            <th class="col4 client_details" id="ContDetails">Contact Deatils</th>
                            <th class="col4">City</th>
                            <th class="col4">State</th>
                            <!-- <th class="col4">Mobile No</th> -->
                            <!-- <th class="col4">Company & Mobile No</th> -->
                            <th class="col4" id="report_description">Call Description</th>
                            <th class="col4" id="report_date">Calls Date</th>
                            <th class="col4" id="report_count">Calls Count</th>
                            <th class="col3"></th>
                            <th class="col3"></th>
                         </tr>
                       </thead>
                   </table>
                 </div>
                 </div>
               </div>
              </div>
            </form>
         </div>
      </div>
   </div>
</div>
