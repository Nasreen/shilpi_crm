<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="card-box padmobile table-responsive">
              <form id="allcp-form" name="dateRangeForm" action="<?=ADMIN_PATH;?>visited_customer" method="get">
               <div class="dropdown pull-right btnGrop">
                  <?php if(isset($_SESSION['user_role']) && $_SESSION['user_role']=='Admin'){ ?>
                    <a href="javascript:void(0);" onclick="export_submit('post','export_excel/export_visited_client')" class="btn btn-custom btn-bordred waves-effect waves-light btn-primary w-md m-b-5">EXPORT CLIENT</a>  
                  <?php } ?>
               </div>
                  <h4 class="header-title m-t-0 m-b-30"><?=@$head_title?></h4>
                  <div class="row">
                    <div class="col-lg-12">
                    <div class="clearfix"></div>
                      <div class="m-b-30">
                        <input type="hidden" name="visit_date" id="visit_date" value="<?=@$visit_date?>">
                       <!-- <input type="hidden" name="list" value="list">
                       <input type="hidden" name="draw" value="1"> -->
                        <div class="form-group custommarbottam">
                          <div class="row">
                            <div class="col-sm-2">
                           <input name="start_date" id="start_date" placeholder="Start Date" class="form-control dp" type="text" value="<?=@$start_date?>">
                           <span class="text-danger" id="start_date_error"></span>
                          </div>
                          <div class="col-sm-2">
                           <input name="end_date" id="end_date" placeholder="End Date" class="form-control dp" type="text" value="<?=@$end_date?>">
                          </div>
                          <span class="text-danger" id="end_date_error"></span>
                          <div class="col-sm-2">
                            <input name="commit" value="Search" class="btn btn-primary" type="button" onclick="export_submit('get','visited_customer')" id="search-input-click">
                          </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  </div>
               </form>
               <table id="visitedTable" class="table table-bordered custdatatable table-responsive">
                   <thead>
                       <tr>
                        <th width="7" class="col4">#</th>
                        <th class="col4">Company Name</th>
                        <th class="col3">Contact Person</th>
                        <th class="col3">Contact No</th>
                        <th class="col3">Visit Description</th>
                        <th class="col3">Visit Date</th>
                        <th class="col3">Visit Count</th>
                        <th></th>
                     </tr>
                   </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
