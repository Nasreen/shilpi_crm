<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="card-box padmobile table-responsive">
               <h4 class="header-title m-t-0 m-b-30">
                 <?php
                    if($head_title=='Pending Calls'){
                      echo 'Pending Calls';
                    }else if($head_title=='Todays calls'){
                      echo 'Today\'s calls';
                    }else if($head_title=='Tomorrows calls'){
                      echo 'Tomorrow\'s calls';
                    }else{
                      echo 'Next week\'s calls';
                    }
                 ?>
               </h4>
                <input type="hidden" name="type" id="type" value="<?=@$type?>">
               <table id="callStatusTable" class="table table-bordered custdatatable table-responsive">
                   <thead>
                       <tr>
                        <th width="7" class="col4">#</th>
                        <th class="col4">Company Name</th>
                        <th class="col3">Contact Person</th>
                        <th class="col3">Call Description</th>
                        <th class="col3">Call Date</th>
                        <th></th>
                     </tr>
                   </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
