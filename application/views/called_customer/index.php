<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="card-box padmobile table-responsive">
              <form id="allcp-form" name="dateRangeForm" action="<?=ADMIN_PATH;?>called_customer" method="get">
               <div class="dropdown pull-right btnGroup">
                  <?php if(isset($_SESSION['user_role']) && $_SESSION['user_role']=='Admin'){ ?>
                    <a onclick="export_submit('post','export_excel/export_called_client')" href="javascript:void(0);" class="btn btn-custom btn-bordred waves-effect waves-light btn-primary w-md m-b-5">EXPORT CLIENT</a>  
                  <?php } ?>
               </div>
               <h4 class="header-title m-t-0 m-b-30"><?=@$head_title?></h4>
                 <div class="row">
                   <div class="col-lg-12">
                    <div class="clearfix"></div>
                      <div class="m-b-30">
                        <input type="hidden" name="call" id="call" value="<?=@$call?>">
                       <!-- <input type="hidden" name="list" value="list">
                       <input type="hidden" name="draw" value="1"> -->
                       <div class="form-group custommarbottam">
                          <div class="row">
                            <div class="col-sm-2">
                             <input name="start_date" id="start_date" placeholder="Start Date" class="form-control dp" type="text" value="<?=@$start_date?>">
                            </div>
                            <div class="col-sm-2">
                             <input name="end_date" id="end_date" placeholder="End Date" class="form-control dp" type="text" value="<?=@$end_date?>">
                            </div>
                            <span class="text-danger" id="end_date_error"></span>
                            <div class="col-sm-2">
                               <input name="commit" value="Search" class="btn btn-primary" type="button" onclick="export_submit('get','called_customer')" id="search-input-click">
                            </div>
                          </div>
                       </div>
                    </div>
                 </div>
                 </div>
               </form>
               <table id="calledTable" class="table table-bordered custdatatable table-responsive">
                   <thead>
                       <tr>
                        <th width="7" class="col4">#</th>
                        <th class="col4">Company Name</th>
                        <th class="col3">Contact Person</th>
                        <th class="col3">Contact No</th>
                        <th class="col3">Call Description</th>
                        <th class="col3">Call Date</th>
                        <th class="col3">Call Count</th>
                        <th></th>
                     </tr>
                   </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
</div>
