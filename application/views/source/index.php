<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><?=@$head_title?></span>
               </h4>
                <a href="<?= ADMIN_PATH?>source/create"><button  type="button" class="add_senior_manager_button btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5 btn-bordred btn-md">ADD SOURCE </button></a>
               <div class="clearfix"></div>
               <div class="table-rep-plugin">
                  <table class="table table-bordered custdatatable" id="sourceTable">
                     <thead>
                        <tr>
                           <th width="7" class="col4">#</th>
                           <th class="col4">Name</th>
                           <th class="col3"></th>
                        </tr>
                     </thead>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>