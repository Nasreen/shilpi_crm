<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=@$head_title?></span>
               </h4>
               <div class="col-lg-12">
                  <form name="statesForm" id="statesForm" role="form" class="form-horizontal" method="post">
                    <input type="hidden" name="table_name" name="table_name" value="states">
                    <input type="hidden" name="id" name="id" value="<?=@$states['id']?>">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Country <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <select id="country" class="form-control" name="states[country]">
                            <option value="">Select Country</option>
                            <?php
                                if(sizeof($country)>0){
                                  foreach ($country as $key => $value) {
                            ?>
                                <option value="<?=@$value['name']?>" <?=(isset($states['country']) && $states['country']==$value['name'])? 'selected':''?>><?=@$value['name']?></option>
                            <?php 
                                  }
                                }
                            ?>
                           </select>
                           <span class="text-danger" id="country_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Name <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Name" id="name" class="form-control" name="states[name]" value="<?=@$states['name']?>">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                    <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                         <button class="btn btn-primary waves-effect waves-light btn-bordred btn-lg" name="commit" type="button" onclick="save_states('update'); ">
                         SAVE
                         </button>
                         <a href="<?=ADMIN_PATH.'states'?>" class="btn btn-default waves-effect waves-light m-l-5 btn-bordred btn-lg" type="reset">
                           CANCEL
                           </a>
                     </div>
                  </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>