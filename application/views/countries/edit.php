<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=@$head_title?></span>
               </h4>
               <div class="col-lg-12">
                  <form name="countriesForm" id="countriesForm" role="form" class="form-horizontal" method="post">
                    <input type="hidden" name="table_name" name="table_name" value="countries">
                    <input type="hidden" name="id" name="id" value="<?=@$countries['id']?>">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Enter Name <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Name" id="name" class="form-control" name="countries[name]" value="<?=@$countries['name']?>">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                    <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                         <button class="btn btn-primary waves-effect waves-light btn-bordred btn-lg" name="commit" type="button" onclick="save_countries('update'); ">
                         SAVE
                         </button>
                         <a href="<?=ADMIN_PATH.'countries'?>" class="btn btn-default waves-effect waves-light m-l-5 btn-bordred btn-lg" type="reset">
                           CANCEL
                           </a>
                     </div>
                  </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>