<?php
  $user_role=get_user_role();
?>
<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><span><?=@$head_title?></span>
               </h4>
               <div class="col-lg-12">
                  <form name="userForm" id="userForm" role="form" class="form-horizontal" method="post">
                  <input type="hidden" name="user[user_role]" id="user_role" value="User">
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> User Role <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <select id="user_role" class="form-control" name="user[user_role]">
                             <option value="">Select User Role</option>
                             <?php foreach ($user_role as $key => $value) { ?>
                               <option value="<?=$key?>"><?=$value?></option>
                             <?php } ?>
                           </select>
                           <span class="text-danger" id="user_role_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Name <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Name" id="name" class="form-control" name="user[name]">
                           <span class="text-danger" id="name_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Email Id <span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" placeholder="Enter Email Id" id="email" class="form-control" name="user[email]">
                           <span class="text-danger" id="email_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Password<span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="password" placeholder="Enter Password" id="password" class="form-control" name="user[encrypted_password]">
                           <span class="text-danger" id="password_error"></span>
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Confirm Password<span class="asterisk">*</span></label>
                        <div class="col-sm-4">
                           <input type="password" placeholder="Enter Confirm Password" id="confirm_password" class="form-control" name="confirm_password">
                           <span class="text-danger" id="confirm_password_error"></span>
                        </div>
                     </div>
                   <!--   <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3"> Master <span class="asterisk">*</span></label>
                        <div class="col-sm-8"></div>
                        <div class="col-sm-12">
                           <?php foreach ($master as $key => $val) { ?>
                           <div class="col-sm-3">
                             <table class="table table-bordered custdatatable">
                               <thead>
                                 <tr>
                                   <th><input type="checkbox" class="check_all"></th>
                                   <th><?=@$val[0]['display_name']?></th>
                                 </tr>
                               </thead>
                               <tbody>
                               <?php foreach ($val as $controller) { ?>
                                 <tr>
                                   <td><input type="checkbox" value="<?=$controller['id']?>" class="checkbox" name="user_access[controller_id][<?=$controller['id']?>]"></td>
                                   <td><?=@$controller['display_name']?></td>
                                 </tr>
                               <?php } ?>
                               </tbody>
                             </table>
                           </div>
                           <?php } ?>
                        </div>
                        <span class="text-danger" id="controller_id_error"></span>
                     </div> -->
                    <div class="form-group">
                     <div class="col-sm-offset-5 col-sm-8 texalin">
                         <button class="btn btn-primary waves-effect waves-light btn-bordred btn-lg" name="commit" type="button" onclick="save_user('store'); ">
                         SAVE
                         </button>
                         <a href="<?=ADMIN_PATH.'manage_user'?>" class="btn btn-default waves-effect waves-light m-l-5 btn-lg" type="reset">
                           CANCEL
                           </a>
                     </div>
                  </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</div>