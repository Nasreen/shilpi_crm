<div class="content-page">
<div class="content">
   <div class="container">
      <div class="row">
         <div class="col-sm-12 card-box-wrap">
            <div class="card-box padmobile">
               <h4 class="header-title business_manager_header_title m-t-0 m-b-30 bmsmheader bmsmheaderabm"><?=@$head_title?></span>
               </h4>
                <a href="<?= ADMIN_PATH?>manage_user/create"><button  type="button" class="add_senior_manager_button btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5 btn-bordred btn-md">ADD USER </button></a>
               <div class="clearfix"></div>
               <div class="table-responsive">
                  <table class="table table-bordered custdatatable" id="userTable">
                  <thead>
                     <tr>
                        <th width="7" class="col4">#</th>
                        <th class="col4">Name</th>
                        <th class="col4">Email Id</th>
                        <th class="col4">No. of Clients</th>
                        <th class="col3"></th>
                     </tr>
                  </thead>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>