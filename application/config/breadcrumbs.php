<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// $config['crumb_divider'] = '<span class="divider">/</span>';
// $config['tag_open'] = '<ul class="nav navbar-nav navbar-left">';
// $config['tag_close'] = '</ul>';
// $config['crumb_open'] = '<li>';
// $config['crumb_last_open'] = '<li class="">';
// $config['crumb_close'] = '</li>';
$config['crumb_divider'] = ' <span> <i class="fa fa-angle-double-right" aria-hidden="true"> </i></span> ';
$config['tag_open'] = '<h4 class="page-title"> ';
$config['tag_close'] = '</h4>';
$config['crumb_open'] = '';
$config['crumb_last_open'] = '';
$config['crumb_close'] = '';