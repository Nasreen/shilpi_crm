<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['login'] = array(
    array(
        'field' => 'username',
        'label' => 'Email',
        'rules' => 'trim|required|verifyCredentials'
    ),
    array(
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'trim|required|md5'
    )
);
$config['source'] = array(
    array(
        'field' => 'source[name]',
        'label' => 'Name',
        'rules' => 'trim|required|check_source_name',
        'errors' =>array(
            "check_source_name" => "Source Name is already exits"
        )
    ),
);
$config['client'] = array(
    array(
        'field' => 'client[company]',
        'label' => 'company',
        'rules' => 'trim|required|check_showroom_name',
        'errors' =>array(
            "required" => "Please enter Company Name.",
            "check_showroom_name" => "Company name is already exist.",
        )
    ),
    array(
        'field' => 'client[start_date]',
        'label' => 'start_date',
        'rules' => 'trim|valid_date'
    ),
    array(
        'field' => 'client[category]',
        'label' => 'category',
        'rules' => 'trim',
        'errors' =>array(
            "required" => "Please select Category.",
           // "check_category" => "Invalid category. Category should be 'A','B' Or 'C'"
        )
    ),
    array(
        'field' => 'client[name]',
        'label' => 'Name',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[designation]',
        'label' => 'designation',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[email_id]',
        'label' => 'email_id',
        'rules' => 'trim',
        'errors' =>array(
            "required" => "Please enter Email Id.",
            "valid_email" => "Please enter valid Email Id.",
        )
    ),
    array(
        'field' => 'client[mobile_no]',
        'label' => 'Mobile No',
        'rules' => 'trim',
        'errors' =>array(
            "numeric" => "Invalid Mobile No.",
            "max_length" => "Please enter valid Mobile No.",
            "min_length" => "Please enter valid Mobile No.",
        )
    ),
    array(
        'field' => 'client[person_1_name]',
        'label' => 'person_1_name',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[person_1_mobile_no]',
        'label' => 'Mobile No.',
        'rules' => 'trim|numeric|max_length[10]|min_length[10]',
        'errors' =>array(
            "numeric" => "Invalid Mobile No.",
            "max_length" => "Please enter valid Mobile No.",
            "min_length" => "Please enter valid Mobile No.",
        )
    ),
    array(
        'field' => 'client[person_1_designation]',
        'label' => 'person_1_designation',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[person_1_email_id]',
        'label' => 'Email Id',
        'rules' => 'trim',
        'errors' =>array(
            "valid_email" => "Please enter valid Email Id.",
        )
    ),
    array(
        'field' => 'client[person_2_name]',
        'label' => 'person_2_name',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[person_2_mobile_no]',
        'label' => 'Mobile No',
        'rules' => 'trim|numeric|max_length[10]|min_length[10]',
        'errors' =>array(
            "numeric" => "Invalid Mobile No.",
            "max_length" => "Please enter valid Mobile No.",
            "min_length" => "Please enter valid Mobile No.",
        )
    ),
    array(
        'field' => 'client[person_2_designation]',
        'label' => 'person_2_designation',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[person_2_email_id]',
        'label' => 'Email Id',
        'rules' => 'trim',
        'errors' =>array(
            "valid_email" => "Please enter valid Email Id.",
        )
    ),
    array(
        'field' => 'client[address]',
        'label' => 'address',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[city]',
        'label' => 'city',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[area]',
        'label' => 'city',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[city]',
        'label' => 'city',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[state]',
        'label' => 'city',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[pincode]',
        'label' => 'Pincode',
        'rules' => 'trim|numeric'
    ),
    array(
        'field' => 'client[landline_no2]',
        'label' => 'Landline 2',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[landline_no1]',
        'label' => 'Landline 1',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[source]',
        'label' => 'source',
        'rules' => 'trim',
        'errors' =>array(
            "check_source_exist" => "Added Source is not exists in system",
        )
    ),
    array(
        'field' => 'client[reference]',
        'label' => 'reference',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[key_ac_manager]',
        'label' => 'key_ac_manager',
        'rules' => 'trim'
    ),
    array(
        'field' => 'client[birth_date]',
        'label' => 'birth_date',
        'rules' => 'trim|valid_date'
    ),
    array(
        'field' => 'client[showroom_anniversary]',
        'label' => 'showroom_anniversary',
        'rules' => 'trim|valid_date'
    ),
    array(
        'field' => 'client[target]',
        'label' => 'source',
        'rules' => 'trim|check_target',
        'errors' =>array(
            "check_target" => "Invalid value added for Target.Please add value between 0 to 6",
        )
    ),
    array(
        'field' => 'client[person_1_birth_date]',
        'label' => 'Person 1 Birth Date',
        'rules' => 'trim|valid_date'
    ),
    array(
        'field' => 'client[person_2_birth_date]',
        'label' => 'Person 2 Birth Date',
        'rules' => 'trim|valid_date'
    ),
    array(
        'field' => 'client[referred_mobile_no_1]',
        'label' => 'Referred Mobile No 1',
        'rules' => 'trim',
         'errors' =>array(
            "numeric" => "Invalid Referred Mobile No. 1",
            "max_length" => "Please enter valid Referred Mobile No. 1",
            "min_length" => "Please enter valid Referred Mobile No. 1",
        )
    ),
    array(
        'field' => 'client[referred_mobile_no_2]',
        'label' => 'Referred Mobile No 2',
        'rules' => 'trim',
         'errors' =>array(
            "numeric" => "Invalid Referred Mobile No. 2",
            "max_length" => "Please enter valid Referred Mobile No. 2",
            "min_length" => "Please enter valid Referred Mobile No. 2",
        )
    ),
);

$config['visit'] = array(
    array(
        'field' => 'visit[visit_date]',
        'label' => 'Visit Date',
        'rules' => 'trim|required|valid_date'
    ),
    array(
        'field' => 'visit[type]',
        'label' => 'Visit Type',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'visit[description]',
        'label' => 'description',
        'rules' => 'trim'
    )
);
$config['follow_up'] = array(
    array(
        'field' => 'follow_up[follow_up_date]',
        'label' => 'Call Date',
        'rules' => 'trim|required|valid_date'
    ),
     array(
        'field' => 'follow_up[description]',
        'label' => 'description',
        'rules' => 'trim'
    )
);
$config['client_email'] = array(
    array(
        'field' => 'subject',
        'label' => 'Subject',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'email_body',
        'label' => 'Email Body',
        'rules' => 'trim|required'
    )
);
$config['manage_user'] = array(
    array(
        'field' => 'user[name]',
        'label' => 'Name',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'user[email]',
        'label' => 'Email Id',
        'rules' => 'trim|required|valid_email|check_user',
        'errors' =>array(
            "valid_email" => "Please enter valid Email Id.",
            "check_user" => "This Email Id is already exists.",
        )
    ),
    array(
        'field' => 'user[encrypted_password]',
        'label' => 'Password',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'confirm_password',
        'label' => 'Confirm Password',
        'rules' => 'trim|required|matches[user[encrypted_password]]',
        'errors' =>array(
            "matches" => "The Confirm Password field does not match the Password field. ",
        )
    ),
    array(
        'field' => 'user[user_role]',
        'label' => 'User Role',
        'rules' => 'trim|required'
    ),
);
$config['designations'] = array(
    array(
        'field' => 'designations[name]',
        'label' => 'Name',
        'rules' => 'trim|required|check_designation_name',
        'errors' =>array(
            "check_designation_name" => "Designation Name is already exits"
        )
    ),
);
$config['categories'] = array(
    array(
        'field' => 'categories[name]',
        'label' => 'Name',
        'rules' => 'trim|required|check_category_name',
        'errors' =>array(
            "check_category_name" => "Category is already exits"
        )
    ),
);
$config['countries'] = array(
    array(
        'field' => 'countries[name]',
        'label' => 'Name',
        'rules' => 'trim|required|validate_table_name',
        'errors' =>array(
            "validate_table_name" => "Country Name is already exits"
        )
    ),
);
$config['states'] = array(
    array(
        'field' => 'states[country]',
        'label' => 'Country',
        'rules' => 'trim|required',
    ),
    array(
        'field' => 'states[name]',
        'label' => 'Name',
        'rules' => 'trim|required|validate_table_name',
        'errors' =>array(
            "validate_table_name" => "State Name is already exits"
        )
    ),
);
