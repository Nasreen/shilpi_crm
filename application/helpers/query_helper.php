<?php

defined('BASEPATH') OR exit('No direct script access allowed.');

if (!function_exists('get_city')) {
    function get_city($state) {
        $ci = &get_instance();
        $ci->db->where('state',$state);
        $result =  $ci->db->get('cities')->result_array();
        return $result;
    }
}
if (!function_exists('get_state')) {
    function get_state($country="") {
        $ci = &get_instance();
        if($country!="")
            $ci->db->where('country',$country);
        $result =  $ci->db->get('states')->result_array();
        return $result;
    }
}
if (!function_exists('get_country')) {
    function get_country() {
        $ci = &get_instance();
        $result =  $ci->db->get('countries')->result_array();
        return $result;
    }
}



