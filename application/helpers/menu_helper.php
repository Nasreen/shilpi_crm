<?php
if (!function_exists('get_menu')) {
    function get_menu() {
        $menu =Array
        (
            'Dashboard' => Array
                (
                    0 => Array
                        (
                            'name' => 'dashboard',
                            'display_name' => 'Dashboard',
                            'master_controller_name' => 'Dashboard',
                            'icon' => 'D'
                        )
                ),

            'Master' => Array
                (
                    0 => Array
                        (
                            'name' => 'source',
                            'display_name' => 'Source Master',
                            'master_controller_name' => 'Master',
                            'icon' => 'M',
                        ),

                    1 => Array
                        (
                            'name' => 'designation',
                            'display_name' => 'Designation Master',
                            'master_controller_name' => 'Master',
                            'icon' => 'fa fa-tasks'
                        ),

                    2 => Array
                        (
                            'name' => 'categories',
                            'display_name' => 'Categories',
                            'master_controller_name' => 'Master',
                            'icon' => 'fa fa-bars',
                        ),

                    /*3 => Array
                        (
                            'name' => 'countries',
                            'display_name' => 'Countries',
                            'master_controller_name' => 'Master',
                            'icon' => 'fa fa-map-marker',
                        ),

                    4 => Array
                        (
                            'name' => 'states',
                            'display_name' => 'States',
                            'master_controller_name' => 'Master',
                            'icon' => 'fa fa-map-marker',
                        ),

                    5 => Array
                        (
                            'name' => 'cities',
                            'display_name' => 'Cities',
                            'master_controller_name' => 'Master',
                            'icon' => 'fa fa-map-marker',
                        )*/
                ),

            'Clients' => Array
                (
                    0 => Array
                    (
                        'name' => 'client',
                        'display_name' => 'Clients',
                        'master_controller_name' => 'Clients',
                        'icon' => 'C',
                    )
                ),

            'Visited Clients' => Array
                (
                    0 => Array
                    (
                        'name' => 'visited_customer',
                        'display_name' => 'Visited Customers',
                        'master_controller_name' => 'Visited Customers',
                        'icon' => 'V'
                    )
                ),

            'Called Clients' => Array
            (
                0 => Array
                (
                    'name' => 'called_customer',
                    'display_name' => 'Called Customers',
                    'master_controller_name' => 'Called Customers',
                    'icon' => 'C',
                )
            )
        );
        return $menu;
    }
}

if (!function_exists('get_menu_role_wise')) {
    function get_menu_role_wise($key){
        if($key!=""){
             $menu_role_wise=array(
                'data_entry_operator'=>array('Master','Dashboard','Clients'),
                'caller'=>array('Master','Dashboard','Clients','Visited Clients','Called Clients'),
                'outdoor_visit'=>array('Master','Dashboard','Clients','Visited Clients')
            );
            return $menu_role_wise[$key];
        }
    }
}

if (!function_exists('get_action_access')) {
    function get_action_access($controller,$key){
        $ci = &get_instance();
        $actions['data_entry_operator']=array(
            'client'=>array('index','create','edit','store','validation','update','import_excel'),
            'source'=>array('index','create','edit','store','validation','update'),
            'designation'=>array('index','create','edit','store','validation','update'),
            'categories'=>array('index','create','edit','store','validation','update'),
            'dashboard'=>array('index'),
        );
        $actions['caller']=array(
            'client'=>array('index','create','edit','store','validation','update','import_excel','send_email'),
            'source'=>array('index','create','edit','store','validation','update'),
            'designation'=>array('index','create','edit','store','validation','update'),
            'categories'=>array('index','create','edit','store','validation','update'),
            'dashboard'=>array('index'),
            'called_customer'=>array('index'),
            'visited_customer'=>array('index'),
        );
        $actions['outdoor_visit']=array(
            'client'=>array('index','create','edit','store','update','validation','import_excel','send_email'),
            'source'=>array('index','create','edit','store','update','validation'),
            'designation'=>array('index','create','store','update','edit','validation'),
            'categories'=>array('index','create','store','update','edit','validation'),
            'dashboard'=>array('index'),
            'visited_customer'=>array('index'),
        );
        if($key!='Admin'){
            if(!in_array(strtolower($ci->uri->segment(2)),$actions[$key][$controller]) && !empty($ci->uri->segment(2))){
                redirect(ADMIN_PATH.'permission');die;
            }
        }
    }
}


