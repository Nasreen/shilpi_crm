<?php

defined('BASEPATH') OR exit('No direct script access allowed.');

if (!function_exists('userdataFromUsername')) {

    function userdataFromUsername($username) {
        $ci = &get_instance();
        $result=$ci->db->get_where("users",array("email"=>$username))->row_array();
        return $result;
    }

}
if (!function_exists('user_session')) {

    function user_session($user) {
        $ci = &get_instance();
        $sess_array = array();
        foreach ($user as $row) {
            $sess_array = array(
                'id' => $row->id,
                'email' => $row->email,
                'profilepic' => $row->profilepic,
                'firstname' => $row->firstname,
                'lastname' => $row->lastname,
                'logged_in' => TRUE,
                'isverify' => $row->isverify,
                'mobno' => $row->mobno
            );
            $ci->session->set_userdata($sess_array);
        }
    }

}
if (!function_exists('query_record')) {

    function query_record($record, $params) {
        $data = array();
         $j=0;
        foreach ($record['list'] as $key => $value) {
            $i = 0;
            foreach ($value as $v) {
                $data[$key][$i] = $v;
                $i++;
            }
            $j++;
        }
        if(isset($record['total'])){
            $data[$j] =$record['total'];
        }
        $json_data = array(
            "draw" => intval($params['draw']),
            "recordsTotal" => intval($record['totalRecords']),
            "recordsFiltered" => intval($record['totalRecords']),
            "data" => $data
        );
        return json_encode($json_data);
    }
}
if (!function_exists('get_successMsg')) {

    function get_successMsg($id = "") {
        $msg = 'Added';
        if ($id != "")
            $msg = 'Updated';
        $success_msg = array(
            "status" => "success",
            "data" => $msg . " Successfully!!!"
        );
        return $success_msg;
    }

}
if (!function_exists('get_errorMsg')) {

    function get_errorMsg($msg = "") {
        if ($msg == "")
            $msg = "Oops! Error.  Please try again later!!!";
        $error_msg = array(
            "status" => "error",
            "data" => $msg
        );
        return $error_msg;
    }

}

if (!function_exists('get_target')) {

    function get_target() {
       $targe=array(
            '0'=>'Not Contacted',
            '1'=>'Attempted to Contact',
            '2'=>'Contact in Future',
            '3'=>'Contacted',
            '4'=>'Junk Lead',
            '5'=>'Lost Lead',
            '6'=>'Lead',
        );
        return $targe;
    }

}
if (!function_exists('get_last_url_segment')) {
    function get_last_url_segment($url,$key='') {
        $keys = parse_url($url); // parse the url
        $path = explode("/", $keys['path']); // splitting the path
        if($key==2)
            $result=$path[$key];
        else
            $result = end($path);
        return $result;
    }
} 

if(!function_exists('get_default_controller'))
{
    function get_default_controller($user_id,$user_role)
    {
        if($user_role=='Admin'){
            $result='dashboard';
        }else{
            $ci = &get_instance();
            $ci->db->where('u.user_id',$user_id);
            $ci->db->select('c.name');
            $ci->db->from('user_controllers u');
            $ci->db->join('controllers c','c.id=u.controller_id','left');
            $response=$ci->db->get()->row_array();
            $result=@$response['name'];
        }
        return $result;
    }
}
if (!function_exists('get_user_role')) {
    function get_user_role() {
       $user_role=array(
            'data_entry_operator'=>'Data Entry Operator',
            'caller'=>'Caller',
            'outdoor_visit'=>'Outdoor Marketing',
        );
        return $user_role;
    }
}
if (!function_exists('get_visit_type')) {
    function get_visit_type() {
        $visit_type=array(
          '1'=>'Office Visit',
          '2'=>'Marketing Visit',
        );
        if(isset($_SESSION['user_role']) && $_SESSION['user_role']=='outdoor_visit'){
            unset($visit_type[1]);
        }
        return $visit_type;
    }
}

