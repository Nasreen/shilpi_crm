 <?php

class Called_customer_model extends CI_Model {

    function __construct() {
    	parent::__construct();
        $this->table_name='follow_ups';
    }
    public function get($params,$start_date='',$end_date='')
    {
        $result = array();
        $data = array();
        $counter = 0;
        $cnt = isset($params['start']) ? $params['start']:0;
        $result['totalRecords']= sizeof($this->getData($params,'count',$start_date,$end_date));
        $fetch_result = $this->getData($params,'',$start_date,$end_date);
        if (sizeof($fetch_result) > 0) {
            foreach ($fetch_result as $key => $value) {
            
                $data[$counter]['sr'] = $cnt+1;
                $data[$counter]['company'] = $value['company'];
                $data[$counter]['name'] = $value['name'];
                $data[$counter]['mobile_no'] = $value['mobile_no'];
                $data[$counter]['description'] = $value['description'];
                $data[$counter]['follow_up_date'] = ($value['follow_up_date']!="") ? date('d-m-Y',strtotime($value['follow_up_date'])):'-';
                $data[$counter]['called_count'] = $value['called_count'];
                $action = '<a class="btn btn-sm btn-info waves-effect waves-light" href="' . ADMIN_PATH . 'client/edit/' .$value['client_id'] . '">EDIT CLIENT</a>&nbsp;<a class="btn btn-sm btn-warning waves-effect waves-light" href="' . ADMIN_PATH . 'follow_up/create/' .$value['client_id'] . '">CALL/VISIT</a>';
                $data[$counter]['action'] =$action;
                $counter++;
                $cnt++;
            }
        }
        $result['list']=$data;
        
        return $result;
    }
    public function getData($params,$type,$start_date='',$end_date='')
    {
        if($start_date!="" && $end_date!=""){
            $this->db->where("f2.follow_up_date BETWEEN '".date('Y-m-d',strtotime($start_date))."' AND '".date('Y-m-d',strtotime($end_date))."'");
        }else if(isset($params['call']) && $params['call']=="today"){
            $this->db->where("f.follow_up_date",date('Y-m-d'));
        }else if(isset($params['call']) && $params['call']=="tomorrow"){
            $this->db->where("f.follow_up_date",date('Y-m-d',strtotime('+1 days')));
        }else if(isset($params['call']) && $params['call']=="next_week"){
            $start_date = date( 'Y-m-d', strtotime( 'monday next week' ) );
            $end_date = date( 'Y-m-d', strtotime( 'saturday next week' ) );
            $this->db->where("f.follow_up_date between '".$start_date."' and '".$end_date."'");
        }
        if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
            $this->db->where('key_ac_manager',$_SESSION['user_id']);
        }
        $this->db->select("c.id as client_id,c.company,c.name,c.mobile_no,f2.description,f2.follow_up_date,count(*) as called_count") ;
        $this->db->from($this->table_name." f");
        $this->db->join('clients c','c.id=f.client_id','left');
        $this->db->join('follow_ups f2','f2.id = (SELECT MAX(f1.id) FROM follow_ups as f1 WHERE f1.client_id=f.client_id)','left');
        $this->db->order_by("f.id",'desc');
        $this->db->group_by("f.client_id");
        if($type!='count')
        {
            $this->db->limit($params['length'],$params['start']);
        }
        $result = $this->db->get()->result_array();
        return $result;
    }
}    