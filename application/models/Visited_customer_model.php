 <?php

class Visited_customer_model extends CI_Model {

    function __construct() {
    	parent::__construct();
        $this->table_name='visits';
    }
    public function get($params,$start_date='',$end_date='')
    {
        $result = array();
        $data = array();
        $counter = 0;
        $cnt = isset($params['start']) ? $params['start']:0;
        $result['totalRecords']= sizeof($this->getData($params,'count',$start_date,$end_date));
        $fetch_result = $this->getData($params,'',$start_date,$end_date);
        if (sizeof($fetch_result) > 0) {
            foreach ($fetch_result as $key => $value) {
            
                $data[$counter]['sr'] = $cnt+1;
                $data[$counter]['company'] = $value['company'];
                $data[$counter]['name'] = $value['name'];
                $data[$counter]['mobile_no'] = $value['mobile_no'];
                $data[$counter]['description'] = $value['description'];
                $data[$counter]['visit_date'] = ($value['visit_date']!="") ? date('d-m-Y',strtotime($value['visit_date'])):'-';
                $data[$counter]['visited_count'] = $value['visited_count'];
                $action = '<a class="btn btn-sm btn-info waves-effect waves-light" href="' . ADMIN_PATH . 'client/edit/' .$value['client_id'] . '">EDIT CLIENT</a>&nbsp;<a class="btn btn-sm btn-warning waves-effect waves-light" href="' . ADMIN_PATH . 'follow_up/create/' .$value['client_id'] . '">CALL/VISIT</a>';
                $data[$counter]['action'] =$action;
                $counter++;
                $cnt++;
            }
        }
        $result['list']=$data;
        
        return $result;
    }
    public function getData($params,$type,$start_date='',$end_date='')
    {
        if($start_date!="" && $end_date!=""){
            $this->db->where("v2.visit_date BETWEEN '".date('Y-m-d',strtotime($start_date))."' AND '".date('Y-m-d',strtotime($end_date))."'");
        }else if(isset($params['visit_date']) && $params['visit_date']==1){
            $this->db->where('v.visit_done',0);
            $this->db->where("v.visit_date BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d',strtotime('+5 days'))."'");
        }else if(isset($params['visit_date']) && $params['visit_date']==2){
            $this->db->where('v.visit_done',0);
            $this->db->where("v.visit_date BETWEEN '".date('Y-m-d', strtotime('-30 days'))."' AND '".date('Y-m-d')."'");
        }
        if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
            $this->db->where('key_ac_manager',$_SESSION['user_id']);
        }
        $this->db->select("c.id as client_id,c.company,c.name,c.mobile_no,v2.description,v2.visit_date,count(*) as visited_count") ;
        $this->db->from($this->table_name." v");
        $this->db->join('clients c','c.id=v.client_id','left');
        $this->db->join('visits v2','v2.id = (SELECT MAX(v1.id) FROM visits as v1 WHERE v1.client_id=v.client_id)','left');
        $this->db->order_by("v.id",'desc');
        $this->db->group_by("v.client_id");
        if($type!='count')
        {
            $this->db->limit($params['length'],$params['start']);
        }
        $result = $this->db->get()->result_array();
        return $result;
    }
}    