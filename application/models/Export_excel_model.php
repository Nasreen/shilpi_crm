<?php

class Export_excel_model extends CI_Model {

  function __construct() {
      parent::__construct();
  }
  public function exportClients($data)
  {
      $postData=$this->input->post();
  		$search_arr =array('category','target','source','city','name','mobile_no','company');
        foreach ($search_arr as $skey => $svalue) {
            if(isset($data[$svalue]) && $data[$svalue]!=""){
                $this->db->where($svalue,trim($data[$svalue]));
            }
        }
        if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
            $this->db->where('key_ac_manager',$_SESSION['user_id']);
        }
        if(isset($postData['form_filter']) && $postData['form_filter']=='incomplete'){
             $this->db->where('(person_1_name is null or person_2_name is null) or (person_1_designation is  null or person_2_designation is null) or (person_1_email_id is  null or person_2_email_id is null) or (person_1_mobile_no is null or person_2_mobile_no is null) or (person_1_birth_date="0000-00-00" or person_1_birth_date="0000-00-00")');
        }
        $this->db->select("c.name,c.designation,c.mobile_no,c.email_id,c.company,c.category,DATE_FORMAT(c.start_date,'%d-%m-%Y') as start_date,c.person_1_name,c.person_1_designation,c.person_1_mobile_no,c.person_1_email_id,DATE_FORMAT(c.person_1_birth_date,'%d-%m-%Y') as person_1_birth_date,c.person_2_name,c.person_2_designation,c.person_2_mobile_no,c.person_2_email_id,DATE_FORMAT(c.person_2_birth_date,'%d-%m-%Y') as person_2_birth_date,c.address,c.area,c.city,c.state,c.pincode,c.landline_no1,c.landline_no2,DATE_FORMAT(c.birth_date,'%d-%m-%Y') as birth_date,DATE_FORMAT(c.showroom_anniversary,'%d-%m-%Y') as showroom_anniversary,s.name as source,(CASE WHEN c.target='0' THEN 'Not Contacted' WHEN c.target='1' THEN 'Attempted to Contact' WHEN c.target='2' THEN 'Contact in Future' WHEN c.target='3' THEN 'Contacted' WHEN c.target='4' THEN 'Junk Lead' WHEN c.target='5' THEN 'Lost Lead' WHEN c.target='6' THEN 'Lead' ELSE '' END) as target,c.referred_by_1,c.referred_mobile_no_1,c.referred_by_2,c.referred_mobile_no_2");
        $this->db->from("clients c");
        $this->db->join('sources s','s.id=c.source','left');
        $this->db->order_by("c.id",'desc');
        $result = $this->db->get()->result_array();
       
        $column = array('Name','Designation','Mobile No','Email Id','Company','Category','Start Date','Person 1 Name','Person 1 Designation','Person 1 Mobile No.','Person 1 Email Id','Person 1 Birth Date','Person 2 Name','Person 2 Designation','Person 2 Mobile No.','Person 2 Email Id','Person 2 Birth Date','Address','Area','City','State','Pincode','Landline No1','Landline No2','Birth Date','Showroom Anniversary','Source','Target','Referred By 1','Referred Mobile No 1','Referred By 2','Referred Mobile No 2');
        header('Content-Type: text/csv; charset=utf-8');  
        header('Content-Disposition: attachment; filename=clients.csv');  
        $output = fopen("php://output", "w");  
        fputcsv($output, $column);
        foreach ($result as $ckey => $cvalue) {
            fputcsv($output, $cvalue);
        }
        fclose($output);   
  }
  public function exportVisitedClients($data)
  {
      if(isset($data['start_date']) && $data['start_date']!="" && isset($data['end_date']) && $data['end_date']!=""){
            $this->db->where("v2.visit_date BETWEEN '".date('Y-m-d',strtotime($data['start_date']))."' AND '".date('Y-m-d',strtotime($data['end_date']))."'");
        }else if(isset($data['start_date']) && $data['start_date']!=""){
            $this->db->where("v2.visit_date BETWEEN '".date('Y-m-d',strtotime($data['start_date']))."' AND '".date('Y-m-d',strtotime($data['start_date']))."'");
        }
        if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
            $this->db->where('key_ac_manager',$_SESSION['user_id']);
        }
     $this->db->select("c.name,c.designation,c.mobile_no,c.email_id,c.company,c.category,DATE_FORMAT(c.start_date,'%d-%m-%Y') as start_date,c.person_1_name,c.person_1_designation,c.person_1_mobile_no,c.person_1_email_id,c.person_2_name,c.person_2_designation,c.person_2_mobile_no,c.person_2_email_id,c.address,c.area,c.city,c.state,c.pincode,c.landline_no1,c.landline_no2,DATE_FORMAT(c.birth_date,'%d-%m-%Y') as birth_date,DATE_FORMAT(c.showroom_anniversary,'%d-%m-%Y') as showroom_anniversary,s.name as source,(CASE WHEN c.target='0' THEN 'Not Contacted' WHEN c.target='1' THEN 'Attempted to Contact' WHEN c.target='2' THEN 'Contact in Future' WHEN c.target='3' THEN 'Contacted' WHEN c.target='4' THEN 'Junk Lead' WHEN c.target='5' THEN 'Lost Lead' WHEN c.target='6' THEN 'Lead' ELSE '' END) as target,c.reference");
      $this->db->from("visits v");
      $this->db->join('clients c','c.id=v.client_id','left');
      $this->db->join('visits v2','v2.id = (SELECT MAX(v1.id) FROM visits as v1 WHERE v1.client_id=v.client_id)','left');
      $this->db->join('sources s','s.id=c.source','left');
      $this->db->order_by("v.id",'desc');
      $this->db->group_by("v.client_id");
      $result = $this->db->get()->result_array();

      $column = array('Name','Designation','Mobile No','Email Id','Company','Category','Start Date','Person 1 Name','Person 1 Designation','Person 1 Mobile No.','Person 1 Email Id','Person 2 Name','Person 2 Designation','Person 2 Mobile No.','Person 2 Email Id','Address','Area','City','State','Pincode','Landline No1','Landline No2','Birth Date','Showroom Anniversary','Source','Target','Reference');
      header('Content-Type: text/csv; charset=utf-8');  
      header('Content-Disposition: attachment; filename=visited_customers.csv');  
      $output = fopen("php://output", "w");  
      fputcsv($output, $column);
      foreach ($result as $ckey => $cvalue) {
          fputcsv($output, $cvalue);
      }
      fclose($output);
  }
  public function exportCalledClients($data)
    {
        if(isset($data['start_date']) && $data['start_date']!="" && isset($data['end_date']) && $data['end_date']!=""){
            $this->db->where("f2.follow_up_date BETWEEN '".date('Y-m-d',strtotime($data['start_date']))."' AND '".date('Y-m-d',strtotime($data['end_date']))."'");
        }else if(isset($data['start_date']) && $data['start_date']!=""){
            $this->db->where("f2.follow_up_date BETWEEN '".date('Y-m-d',strtotime($data['start_date']))."' AND '".date('Y-m-d',strtotime($data['start_date']))."'");
        }
        if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
            $this->db->where('key_ac_manager',$_SESSION['user_id']);
        }
        $this->db->select("c.name,c.designation,c.mobile_no,c.email_id,c.company,c.category,DATE_FORMAT(c.start_date,'%d-%m-%Y') as start_date,c.person_1_name,c.person_1_designation,c.person_1_mobile_no,c.person_1_email_id,c.person_2_name,c.person_2_designation,c.person_2_mobile_no,c.person_2_email_id,c.address,c.area,c.city,c.state,c.pincode,c.landline_no1,c.landline_no2,DATE_FORMAT(c.birth_date,'%d-%m-%Y') as birth_date,DATE_FORMAT(c.showroom_anniversary,'%d-%m-%Y') as showroom_anniversary,s.name as source,(CASE WHEN c.target='0' THEN 'Not Contacted' WHEN c.target='1' THEN 'Attempted to Contact' WHEN c.target='2' THEN 'Contact in Future' WHEN c.target='3' THEN 'Contacted' WHEN c.target='4' THEN 'Junk Lead' WHEN c.target='5' THEN 'Lost Lead' WHEN c.target='6' THEN 'Lead' ELSE '' END) as target,c.reference");
        $this->db->from("follow_ups f");
        $this->db->join('clients c','c.id=f.client_id','left');
        $this->db->join('follow_ups f2','f2.id = (SELECT MAX(f1.id) FROM follow_ups as f1 WHERE f1.client_id=f.client_id)','left');
        $this->db->join('sources s','s.id=c.source','left');
        $this->db->order_by("f.id",'desc');
        $this->db->group_by("f.client_id");
        $result = $this->db->get()->result_array();

       $column = array('Name','Designation','Mobile No','Email Id','Company','Category','Start Date','Person 1 Name','Person 1 Designation','Person 1 Mobile No.','Person 1 Email Id','Person 2 Name','Person 2 Designation','Person 2 Mobile No.','Person 2 Email Id','Address','Area','City','State','Pincode','Landline No1','Landline No2','Birth Date','Showroom Anniversary','Source','Target','Reference');
        header('Content-Type: text/csv; charset=utf-8');  
        header('Content-Disposition: attachment; filename=called_customers.csv');  
        $output = fopen("php://output", "w");  
        fputcsv($output, $column);
        foreach ($result as $ckey => $cvalue) {
            fputcsv($output, $cvalue);
        }
        fclose($output);
    }
}  