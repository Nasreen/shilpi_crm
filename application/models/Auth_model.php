<?php

class Auth_model extends CI_Model {

    function __construct() {
        $this->table_name = "login";
        parent::__construct();
    }

    function validateAdminLogin() {
        $this->load->config('admin_validationrules', TRUE);
        $this->form_validation->set_message('verifyCredentials', 'Invalid Email Or Password');
        $this->form_validation->set_rules($this->config->item('login', 'admin_validationrules'));
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    function upload_pic($data)
    {
        $response=array();
        CreateFolderbyname("profile_pic");
        $uplaodFileName = RenameUploadFile($data['name']);
        $_FILES['userfile']['name']= date("YmdHis")."_".$uplaodFileName;
        $_FILES['userfile']['type']= $data['type'];
        $_FILES['userfile']['tmp_name']= $data['tmp_name'];
        $_FILES['userfile']['error']= $data['error'];
        $_FILES['userfile']['size']= $data['size'];    

        $this->upload->initialize(set_upload_options('profile_pic'));
        $upload=$this->upload->do_upload();
        if($upload)
        {
            $filename=$_FILES['userfile']['name'];
            if($this->db->update("user_master",array("profile_pic"=>$filename),array("user_master_id"=>$this->session->userdata("user_master_id"))))
            {

                $response['status']="success";
                $response['data']="Profile pic uploaded successfully";
                $this->session->set_userdata('profile_pic',$filename);
                $response['file_path']=$filename;
            }
        }else{
            $response['status']="error";
            $response['data']="Oops! Error.  Please try again later!!!";
        }
        return $response;
    }
}
