 <?php

class Manage_user_model extends CI_Model {

    function __construct() {
    	parent::__construct();
        $this->table_name='users';
    }
    function validateData() {
        $this->form_validation->set_rules($this->config->item('manage_user', 'admin_validationrules'));
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    function validateAccessData()
    {
        $response=array();
        $response['status']=TRUE;
        $postData=$this->input->post('user_access[controller_id]');
        if(sizeof($postData)==0){
            $response['status']=FALSE;
            $response['controller_id']="Please select at least one master";
        }
        return $response;
    }
    public function get($params,$id)
    {
        $result = array();
        $data = array();
        $counter = 0;
        $cnt = $params['start'];
        $result['totalRecords']= sizeof($this->getData($id,$params,'count'));
        $fetch_result = $this->getData($id,$params,'');
        if (sizeof($fetch_result) > 0) {
            foreach ($fetch_result as $key => $value) {
                $status='INACTIVE';
                $status_class='btn-warning';
                if($value['status']==0){
                    $status_class='btn-danger';
                    $status='ACTIVE';
                }
                $data[$counter]['sr'] = $cnt+1;
                $data[$counter]['name'] = $value['name'];
                $data[$counter]['email'] = $value['email'];
                $data[$counter]['no_clients'] = @$value['no_clients'];
                $action = '<a class="btn btn-sm btn-info waves-effect waves-light" href="' . ADMIN_PATH . 'manage_user/edit/' .$value['id'] . '">EDIT</a>&nbsp;<a class="'.@$status_class.' btn-sm btn" href="javascript:void(0);" onclick="confirmInactive('.$value['id'].','.$value['status'].',\'manage_user\',\''.$status.'\')">'.$status.'</a>';

                $data[$counter]['action'] =$action;
                $counter++;
                $cnt++;
            }
        }
        $result['list']=$data;
        
        return $result;
    }
    public function getData($id,$params,$type)
    {
        $this->db->where('user_role !=','Admin');
        $this->db->or_where('user_role IS NULL');
        $this->db->select("u.*,count(c.id) as no_clients") ;
        $this->db->from($this->table_name." u");
        $this->db->join("clients c",'u.id=c.key_ac_manager','left');
        $this->db->order_by("u.id",'desc');
        $this->db->group_by('u.id');
        if($type=='count')
        {
            $this->db->limit($params['length'],$params['start']);
        }
        $result = $this->db->get()->result_array();
        return $result;
    }
    public function getById($id)
    {
        $result=$this->db->get_where($this->table_name,array('id'=>$id))->row_array();
        return $result;
    }
    public function save($id='',$data)
    {
        $data['encrypted_password'] =md5($data['encrypted_password']);
        if($id=="")
        {
            $data['created_at']=date('Y-m-d H:i:s');
            $this->db->insert($this->table_name,$data);
            $user_id=$this->db->insert_id();
            $action="add";
        }else{
            $data['updated_at']=date('Y-m-d H:i:s');
            $this->db->update($this->table_name,$data,array('id'=>$id));
            $user_id=$id;
            $action="update";
        }
        /*Save User Access Controller */
        $this->saveUserAccess($user_id,$this->input->post('user_access[controller_id]'),$action);

        return get_successMsg($id);
    }
    public function change_status($data)
    {
        $response_data=array();
        $update_arr =array(
            'status'=>($data['status']==0) ? 1 : 0
        );
        $result=$this->db->update($this->table_name,$update_arr,array("id"=>$data['id']));
        if($result){
            $response_data['status']='success';
            $response_data['data']='status changed successfully.';
        }else{
            $response_data['status']='error';
            $response_data['data']='Error !!';
        }
        return $response_data;
    }
    public function getMasters($id="")
    {
        $module=array();
        if($id==""){
            $this->db->select("*");
            $this->db->from("controllers");
            $result=$this->db->get()->result_array();
            foreach ($result as $key => $value) {
               $module[$value['master_controller_name']][]=$value;
            }
        }else{
            $this->db->where('user_id',$id);
            $this->db->select("controller_id");
            $this->db->from("user_controllers");
            $result = $this->db->get()->result_array();
            $module = array_map(function ($ar) {return $ar['controller_id'];}, $result);
        }
        return $module;
    }
    public function saveUserAccess($user_id,$data,$action)
    {
        if($action=="update"){
            $this->db->delete('user_controllers',array('user_id'=>$user_id));
        }
        if(sizeof($data)>0){
            foreach($data as $key => $value) {
               $user_controller=array(
                    'user_id'=>$user_id,
                    'controller_id'=>$key,
                );
                $this->db->insert('user_controllers',$user_controller);
            }
        }
    }
}    