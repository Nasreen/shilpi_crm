<?php

class Call_visit_model extends CI_Model {

  function __construct() {
      parent::__construct();
  }
  public function getCall($params)
  {
      $result = array();
      $data = array();
      $counter = 0;
      $cnt = isset($params['start']) ? $params['start']:0;
      $result['totalRecords']= sizeof($this->getCallData($params,'count'));
      $fetch_result = $this->getCallData($params,'');
      if (sizeof($fetch_result) > 0) {
          foreach ($fetch_result as $key => $value) {
          
              $data[$counter]['sr'] = $cnt+1;
              $data[$counter]['company'] = $value['company'];
              $data[$counter]['name'] = $value['name'];
              $data[$counter]['description'] = $value['description'];
              $data[$counter]['follow_up_date'] = ($value['follow_up_date']!="") ? date('d-m-Y',strtotime($value['follow_up_date'])):'-';
              $action = '<a class="btn btn-sm btn-warning waves-effect waves-light" href="' . ADMIN_PATH . 'follow_up/update_call/' .$value['client_id'] . '/'.$value['id'].'">CALL</a>';
              $data[$counter]['action'] =$action;
              $counter++;
              $cnt++;
          }
      }
      $result['list']=$data;
      return $result;
  }
  public function getCallData($params,$type='')
  {
    if(isset($params['type']) && $params['type']==1)
    {
      $this->db->where('f.follow_up_date <',date('Y-m-d'));
    }else if(isset($params['type']) && $params['type']==2){
      $this->db->where('f.follow_up_date ',date('Y-m-d'));
    }else if(isset($params['type']) && $params['type']==3){
      $this->db->where('f.follow_up_date ',date('Y-m-d',strtotime('+1 days')));
    }else if(isset($params['type']) && $params['type']==4){
      $start_date = date( 'Y-m-d', strtotime( 'monday next week' ) );
      $end_date = date( 'Y-m-d', strtotime( 'saturday next week' ) );
      $this->db->where("f.follow_up_date between '".$start_date."' and '".$end_date."'");
    }else if(isset($params['type']) && $params['type']==5){
      $start_date=date('Y-m-d');
      $end_date =date('Y-m-d', strtotime('+5 days'));
      $this->db->where("f.follow_up_date between '".$start_date."' and '".$end_date."'");
    }
    if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
      $this->db->where('key_ac_manager',$_SESSION['user_id']);
    }
    $this->db->where(array('f.status'=>'Open','f.type'=>2));
    $this->db->select("f.id,c.id as client_id,c.company,c.name,f.description,f.follow_up_date") ;
    $this->db->from("follow_ups f");
    $this->db->join('clients c','c.id=f.client_id','left');
    $this->db->order_by("f.id",'desc');
    $this->db->group_by("f.client_id");
    if($type!='count')
    {
        $this->db->limit($params['length'],$params['start']);
    }
    $result = $this->db->get()->result_array();
    return $result;
  }
  public function getVisit($params)
  {
      $result = array();
      $data = array();
      $counter = 0;
      $cnt = isset($params['start']) ? $params['start']:0;
      $result['totalRecords']= sizeof($this->getVisitData($params,'count'));
      $fetch_result = $this->getVisitData($params,'');
      if (sizeof($fetch_result) > 0) {
          foreach ($fetch_result as $key => $value) {
          
              $data[$counter]['sr'] = $cnt+1;
              $data[$counter]['company'] = $value['company'];
              $data[$counter]['name'] = $value['name'];
              $data[$counter]['description'] = $value['description'];
              $data[$counter]['visit_date'] = ($value['visit_date']!="") ? date('d-m-Y',strtotime($value['visit_date'])):'-';
              $action = '<a class="btn btn-sm btn-warning waves-effect waves-light" href="' . ADMIN_PATH . 'follow_up/update_visit/' .$value['client_id'] . '/'.$value['id'].'">VISIT</a>';
              $data[$counter]['action'] =$action;
              $counter++;
              $cnt++;
          }
      }
      $result['list']=$data;
      return $result;
  }
  public function getVisitData($params,$type='')
  {
    $start_date=date('Y-m-d');
    if(isset($params['type']) && $params['type']==1)
    {
        $end_date =date('Y-m-d', strtotime('+5 days'));
        $this->db->where("v.visit_date between '".$start_date."' and '".$end_date."'");
        $this->db->where(array('v.status'=>'Open','v.type'=>'1','visit_done'=>0));
    }else if(isset($params['type']) && $params['type']==2){
        $end_date =date('Y-m-d', strtotime('-30 days'));
        //$this->db->where("v.visit_date between '".$end_date."' and '".$start_date."'");
        $this->db->where("v.visit_date <='".$end_date."'");
        $this->db->where(array('v.status'=>'Open','v.type'=>'1','visit_done'=>1));
    }else if(isset($params['type']) && $params['type']==3){
        $this->db->where(array('v.type'=>'1','visit_done'=>1));
    }else if(isset($params['type']) && $params['type']==4){
        $this->db->where(array('v.type'=>'2','visit_done'=>1));
    }else if(isset($params['type']) && $params['type']==5){
        $this->db->where(array('v.status'=>'Open','v.type'=>'1','visit_done'=>1));
        $this->db->where("datediff('".$start_date."', v.visit_date) =30");
    }else if(isset($params['type']) && $params['type']==6){
        $this->db->where(array('v.status'=>'Open','v.type'=>'1','visit_done'=>1));
        $this->db->where("datediff('".$start_date."', v.visit_date) =23");
    }else if(isset($params['type']) && $params['type']==7){
        $this->db->where(array('v.status'=>'Open','v.type'=>'1','visit_done'=>1));
        $this->db->where("datediff('".$start_date."', v.visit_date) =29");
    }
    if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
      $this->db->where('key_ac_manager',$_SESSION['user_id']);
    }
    //$this->db->where(array('v.status'=>'Open','v.type'=>'1'));
    $this->db->select("v.id,c.id as client_id,c.company,c.name,v.description,MAX(v.visit_date) as visit_date") ;
    $this->db->from("visits v");
    $this->db->join('clients c','c.id=v.client_id','left');
    $this->db->order_by("v.visit_date",'desc');
    //$this->db->order_by("v.id",'desc');
    $this->db->group_by("v.client_id");
    if($type!='count')
    {
        $this->db->limit($params['length'],$params['start']);
    }
    $result = $this->db->get()->result_array();
    // echo $this->db->last_query();
    // print_r($result);
    // exit;
    return $result;
  }
}  