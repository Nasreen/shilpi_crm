 <?php

class Source_model extends CI_Model {

    function __construct() {
    	parent::__construct();
        $this->table_name='sources';
    }
    function validateData() {
        $this->form_validation->set_rules($this->config->item('source', 'admin_validationrules'));
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    public function get($params,$id)
    {
        $result = array();
        $data = array();
        $counter = 0;
        $cnt = $params['start'];
        $result['totalRecords']= sizeof($this->getData($id,$params,'count'));
        $fetch_result = $this->getData($id,$params,'');
        if (sizeof($fetch_result) > 0) {
            foreach ($fetch_result as $key => $value) {
            
                $data[$counter]['sr'] = $cnt+1;
                $data[$counter]['name'] = $value['name'];
                $action = '<a class="btn btn-sm btn-sm btn-info waves-effect waves-light" href="' . ADMIN_PATH . 'source/edit/' .$value['id'] . '">EDIT</a>&nbsp;';

                $data[$counter]['action'] =$action;
                $counter++;
                $cnt++;
            }
        }
        $result['list']=$data;
        
        return $result;
    }
    public function getData($id,$params,$type)
    {
        $this->db->select("s.*") ;
        $this->db->from($this->table_name." s");
        $this->db->order_by("s.id",'desc');
        if($type=='count')
        {
            $this->db->limit($params['length'],$params['start']);
        }
        $result = $this->db->get()->result_array();
        return $result;
    }
    public function getById($id)
    {
        $result=$this->db->get_where($this->table_name,array('id'=>$id))->row_array();
        return $result;
    }
    public function save($id='',$data)
    {
        if($id=="")
        {
            $data['created_at']=date('Y-m-d H:i:s');
            $this->db->insert($this->table_name,$data);
        }else{
            $data['updated_at']=date('Y-m-d H:i:s');
            $this->db->update($this->table_name,$data,array('id'=>$id));
        }
        return get_successMsg($id);
    }
}    