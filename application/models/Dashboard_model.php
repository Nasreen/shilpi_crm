<?php

class Dashboard_model extends CI_Model {

  function __construct() {
      parent::__construct();
  }
  public function getClient($column='',$value='')
  {
  		if($column!=""){
  			$this->db->where($column,$value);
  		}
      if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
          $this->db->where('key_ac_manager',$_SESSION['user_id']);
      }
  		$this->db->select('count(id) as count');
  		$this->db->from('clients');
  		$result =$this->db->get()->row_array();
  		return $result;
  }
  public function getReminder($column)
  {
      $start_date=date('m-d');
      $end_date =date('m-d', strtotime('+10 days'));
      if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
          $this->db->where('key_ac_manager',$_SESSION['user_id']);
      }
      $this->db->where("DATE_FORMAT(".$column.",'%m-%d') between '".$start_date."' and '".$end_date."'");
      $this->db->select("count(id) as count");
      $this->db->from("clients");
      $result = $this->db->get()->row_array();
      return $result;
  }
  public function getAppointment($type='')
  {
      $start_date=date('Y-m-d');
      if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
          $this->db->where('created_by',$_SESSION['user_id']);
      }
      if($type==1){
        $end_date =date('Y-m-d', strtotime('+5 days'));
        $this->db->where("visit_date between '".$start_date."' and '".$end_date."'");
        $this->db->where("v.visit_done",0);
      }else if($type==2){
        $this->db->where("v.visit_done",1);
        $end_date =date('Y-m-d', strtotime('-30 days'));
        //$this->db->where("visit_date between '".$end_date."' and '".$start_date."'");
        $this->db->where("v.visit_date <='".$end_date."'");
      }else if($type==5){
        $this->db->where("v.visit_done",1);
        $this->db->where("datediff('".$start_date."', v.visit_date) =30");
      }else if($type==6){
        $sql="count(Distinct client_id) as count,datediff($start_date,v.visit_date) as c_days";
        $this->db->where("v.visit_done",1);
        $this->db->where("datediff('".$start_date."', v.visit_date) =23");
      }else if($type==7){
        $this->db->where("v.visit_done",1);
        $this->db->where("datediff('".$start_date."', v.visit_date) =29");
      }
      $this->db->where(array('v.status'=>'Open','v.type'=>'1'));
      $this->db->select('count(Distinct client_id) as count');
      $this->db->from('visits v');
      $result =$this->db->get()->row_array();
      return $result;
  }
  public function getCallAppointment()
  {
    if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
      $this->db->where('created_by',$_SESSION['user_id']);
    }
    $start_date=date('Y-m-d');
    $end_date =date('Y-m-d', strtotime('+5 days'));
    $this->db->where("follow_up_date between '".$start_date."' and '".$end_date."'");
    $this->db->where(array('status'=>'Open','type'=>2));
    $this->db->select('count(Distinct client_id) as count');
    $this->db->from('follow_ups f');
    $result =$this->db->get()->row_array();
    return $result;
  }
  public function getCall($date,$type="")
  {
      if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
          $this->db->where('created_by',$_SESSION['user_id']);
      }
      if($type==""){
        $this->db->where(array('status'=>'Open','type'=>2));
        $this->db->where(array('follow_up_date'=>$date));
      }else{
        $this->db->where(array('status'=>'Open','type'=>1));
      }
      $this->db->select('count(Distinct client_id) as count');
      $this->db->from('follow_ups f');
      $result =$this->db->get()->row_array();
      return $result;
  }
  public function getPendingCall()
  {
    if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
        $this->db->where('created_by',$_SESSION['user_id']);
    }
    $this->db->where(array('status'=>'Open','type'=>2));
    $this->db->where('follow_up_date <',date('Y-m-d'));
    $this->db->select('count(Distinct client_id) as count');
    $this->db->from('follow_ups');
    $result =$this->db->get()->row_array();
    return $result['count'];
  }
  public function getNextWeekCall()
  {
    if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
        $this->db->where('created_by',$_SESSION['user_id']);
    }
    $start_date = date( 'Y-m-d', strtotime( 'monday next week' ) );
    $end_date = date( 'Y-m-d', strtotime( 'saturday next week' ) );
    $this->db->where(array('status'=>'Open','type'=>2));
    $this->db->where("follow_up_date between '".$start_date."' and '".$end_date."'");
    $this->db->select('count(Distinct client_id) as count');
    $this->db->from('follow_ups');
    $result =$this->db->get()->row_array();
    return $result;
  }
  public function getVisit($type)
  {
      if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
          $this->db->where('created_by',$_SESSION['user_id']);
      }
      $this->db->where(array('v.visit_done'=>1,'type'=>$type));
      $this->db->select('count(Distinct client_id) as count');
      $this->db->from('visits v');
      $result =$this->db->get()->row_array();
      return $result;
  }
  public function getIncompleteForm()
  {
      if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
        $this->db->where('key_ac_manager',$_SESSION['user_id']);
      }
      $this->db->where('((person_1_name is null or person_2_name is null) or (person_1_designation is  null or person_2_designation is null) or (person_1_email_id is  null or person_2_email_id is null) or (person_1_mobile_no is null or person_2_mobile_no is null) or (person_1_birth_date="0000-00-00" or person_1_birth_date="0000-00-00"))');
      $this->db->select('count(id) as count');
      $this->db->from('clients');
      $result =$this->db->get()->row_array();
      return $result;
  }
  public function getClientByCategoy()
  {
      if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
          $this->db->where('key_ac_manager',$_SESSION['user_id']);
      }
      $this->db->select('category,count(id) as count');
      $this->db->from('clients');
      $this->db->group_by('category');
      $result =$this->db->get()->result_array();
      return $result;
  }
}  