<?php

class Import_excel_model extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  public function validate_client()
  {
    $response=array();
    $response['status']=TRUE;

    $file_ext =($_FILES['file']['name']!="") ? pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION): '';

    if(isset($_FILES['file']['name']) && $_FILES['file']['name']==""){
        $response['file']='Please select file to upload';
        $response['status']=FALSE;
    }else if($file_ext!="" && $file_ext!='xlsx' && $file_ext!='xls'){
        $response['file']='Invalid file format. File format should be xls Or xlsx';
        $response['status']=FALSE;
    }else if($_FILES['file']['name']!="" && ($file_ext=='xls' || $file_ext=='xlsx')){
      $result = $this->excel_lib->import('clients',$_FILES['file']);
      if(isset($result['status']) && $result['status']=='failure'){
        $response=$this->get_differnce_msg($result['difference']);
        $response['status']=FALSE;
      }else{
        $response=$result;
        $response['status']=TRUE;
        $response['other_validation']=TRUE;
      }
    }
    return $response;
  }
   private function get_differnce_msg($diff){
    $error=array();
    foreach ($diff as $key => $value) {
      $error[$key] =$value.' Column not found in excel' ;
    }
    return array_filter($error);
  }
}  