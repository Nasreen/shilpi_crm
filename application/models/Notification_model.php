<?php

class Notification_model extends CI_Model {

  function __construct() {
      parent::__construct();
  }
  public function get_reminder($params)
  {
      $result = array();
      $data = array();
      $counter = 0;
      $cnt = $params['start'];
      $result['totalRecords']= sizeof($this->getData($params,''));
      $fetch_result = $this->getData($params,'count');
      if (sizeof($fetch_result) > 0) {
          foreach ($fetch_result as $key => $value) {
              $status_class='purple';
              $data[$counter]['sr'] = $cnt+1;
              $data[$counter]['company'] = $value['company'];
              $data[$counter]['name'] = $value['name'];
              $data[$counter]['email_id'] = $value['email_id'];
              $data[$counter]['mobile_no'] = $value['mobile_no'];
              $data[$counter]['event_date'] =($params['type']=='birth_date') ? date('d-m-Y',strtotime($value['birth_date'])) : date('d-m-Y',strtotime($value['showroom_anniversary']));
              $counter++;
              $cnt++;
          }
      }
      $result['list']=$data;
      return $result;
  }

  public function getData($params,$type)
  {
    $start_date=date('m-d');
    $end_date =date('m-d', strtotime('+10 days'));
    $sql="id,company,name,email_id,mobile_no,birth_date,showroom_anniversary";
      if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
          $this->db->where('key_ac_manager',$_SESSION['user_id']);
      }
      $column=$params['type'];
    $this->db->where("DATE_FORMAT(".$column.",'%m-%d') between '".$start_date."' and '".$end_date."'");
    $this->db->select($sql);
    $this->db->from("clients");
    $this->db->order_by("id",'desc');
    if($type=='count')
    {
        $this->db->limit($params['length'],$params['start']);
    }
    $result = $this->db->get()->result_array();
    return $result;
  }

  public function get_call_log($params)
  {
      $result = array();
      $data = array();
      $counter = 0;
      $cnt = $params['start'];
      $result['totalRecords']= sizeof($this->getCallData($params,''));
      $fetch_result = $this->getCallData($params,'count');
      if (sizeof($fetch_result) > 0) {
          foreach ($fetch_result as $key => $value) {
              $status_class='purple';
              $data[$counter]['sr'] = $cnt+1;
              $data[$counter]['company'] = $value['company'];
              $data[$counter]['name'] = $value['name'];
              $data[$counter]['email_id'] = $value['email_id'];
              $data[$counter]['mobile_no'] = $value['mobile_no'];
              $data[$counter]['created_at'] =date('d-m-Y',strtotime($value['created_at']));
              $data[$counter]['follow_up_date'] =date('d-m-Y',strtotime($value['follow_up_date']));
              $data[$counter]['description'] = $value['description'];
              $action = '<a class="btn btn-sm btn-warning waves-effect waves-light" href="'.ADMIN_PATH . 'follow_up/create/' .$value['client_id'] . '">CALL</a>';
              $data[$counter]['action'] =$action;
              $counter++;
              $cnt++;
          }
      }
      $result['list']=$data;
      return $result;
  }
  public function getCallData($params,$type)
  {
      if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
          $this->db->where('created_by',$_SESSION['user_id']);
      }
      if(isset($params['start_date']) && $params['start_date']!="" && isset($params['end_date']) && $params['end_date']!=""){
        $start_date=date('Y-m-d 00:00:00',strtotime($params['start_date']));
        $end_date=date('Y-m-d 23:59:59',strtotime($params['end_date']));
        $this->db->where("f.created_at BETWEEN '".$start_date."' and '".$end_date."'");
      }

      //$this->db->where(array('f.status'=>'Open','f.type'=>1));
      $this->db->select('c.company,c.name,c.email_id,c.mobile_no,f.follow_up_date ,f.description,f.client_id,f.created_at');
      $this->db->from('follow_ups f');
      $this->db->where("f.id IN (SELECT MAX(id) AS id FROM follow_ups where status = 'Open' AND `type` = '1' GROUP BY client_id)");
      $this->db->join("clients c","c.id=f.client_id",'left');
      //$this->db->order_by("f.id",'desc');
      $this->db->group_by("f.client_id");
      if($type=='count')
      {
        $this->db->limit($params['length'],$params['start']);
      }
      $result =$this->db->get()->result_array();
      //echo $this->db->last_query();die();
      return $result;
  }
}  