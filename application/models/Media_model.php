 <?php

class Media_model extends CI_Model {

    function __construct() {
    	parent::__construct();
    }
    function validateFileData($data)
    {
        $respons =array();
        $response['status']=TRUE;
        foreach ($data['name'] as $key => $value) {
           $image_ext = pathinfo($value, PATHINFO_EXTENSION); 
            if($value!="" && ($image_ext != "jpg" && $image_ext != "jpeg" && $image_ext != "png")){
                $response['status']=FALSE;   
                $response[$key]= "Invalid file format"; 
            }
        }
        return $response;
    }
    function save_attachment($folder,$file)
    {
        /*echo "<pre>";
        print_r($file);die;*/
        CreateFolderbyname($folder);
        if(isset($file['name']) && $file['name']!="")
        {
            $file_name =  date("YmdHis")."_".RenameUploadFile($file['name']);
            $_FILES['userfile']['name']= $file_name;
            $_FILES['userfile']['type']= $file['type'];
            $_FILES['userfile']['tmp_name']= $file['tmp_name'];
            $_FILES['userfile']['error']= $file['error'];
            $_FILES['userfile']['size']= $file['size'];
            $this->upload->initialize(set_upload_options($folder));
            $upload=$this->upload->do_upload();
            if($upload)
            {
                return $file_name;
            }else{
                return '';
            }
        }
    }
}    