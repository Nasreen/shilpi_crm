<?php

class Email_model extends CI_Model {

  function __construct() {
      parent::__construct();
      $this->load->library('email');
  }
  function validateData($type='')
  {
      $this->form_validation->set_rules($this->config->item($type, 'admin_validationrules'));
      if ($this->form_validation->run() == FALSE) {
          return FALSE;
      } else {
          return TRUE;
      }
  }
  function validateClientData()
  {
    $response=array();
    $postData=$this->input->post();
    if(isset($postData['client'])){
      $response['status']=TRUE;
    }else{
      $response['status']=FALSE;
      $response['client']='Please select at least one of the client to send an email';
    }
    return $response;
  }
  public function client_email($data)
  {
      $attach =$this->media_model->save_attachment('email',$_FILES['file']);
      if($attach!=""){
         $attach=set_realpath('uploads/email/').$attach;
         chmod($attach,0777);
      }
    $response=array();
    if(isset($data['select_all']) && $data['select_all']!=""){
      if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
          $this->db->where('key_ac_manager',$_SESSION['user_id']);
      }
      $postData=$this->input->post('client');
     
      if(isset($postData['company']) && $postData['company']!=""){
        $this->db->where('company',$postData['company']);
      }else if(isset($postData['name']) && $postData['name']!=""){
        $this->db->where('name',$postData['name']);
      }else if(isset($postData['mobile_no']) && $postData['mobile_no']!=""){
        $this->db->where('mobile_no',$postData['mobile_no']);
      }else if(isset($postData['city']) && $postData['city']!=""){
        $this->db->where('city',$postData['city']);
      }
      $this->db->select('id,name,email_id');
      $this->db->from('clients');
      $result = $this->db->get()->result_array();
      foreach ($result as $key => $value) {
        if(isset($value['email_id']) && $value['email_id']!=""){
          $result=$this->send($data['subject'],$data['email_body'],$value['email_id'],$value['name'],'email/client_email',$attach);
        }
      }
    }else if(sizeof($data['client_id'])>0){
      foreach ($data['client_id'] as $key => $value) {
        $result=$this->send($data['subject'],$data['email_body'],$data['client_email_id'][$key],$data['client_name'][$key],'email/client_email',$attach);
      }
    }
    $response['status']='success';
    $response['data']='Email sent Successfully';
    return $response;
  }
  public function send($subject,$email_body,$email_id,$name ,$view,$attachment='')
  {
      $data=array();
      $data['email_id']=$email_id;
      $data['subject']=$subject;
      $data['name']=($name!="" && $name!="NO NAME") ? $name : $email_id;
      $data['email_body']=$email_body;

      $mail = new PHPMailer;
      $mail->IsSMTP();
      $mail->Host = 'smtp.gmail.com';
      $mail->SMTPAuth = true; 
      $mail->Username   = "nasreen@ascratech.com";
      $mail->Password   = "nasreengazi";
      $mail->SMTPSecure = 'tls';
      $mail->Port = 587;
      $mail->From = 'nasreen@ascratech.com';
      $mail->FromName = 'Shilpi CRM';
      $message = $this->load->view($view,$data,true);
      $to = $email_id;
      $mail->addAddress($to);
      $mail->IsHTML(true); 
      if($attachment!="")
        $mail->AddAttachment($attachment);
      $mail->Subject = $subject;
      $mail->Body = $this->email->full_html($subject, $message);
      if($mail->send()){
          return true;
      }
      else
          echo $mail->ErrorInfo;
  }
}  