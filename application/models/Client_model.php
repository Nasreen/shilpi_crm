 <?php

class Client_model extends CI_Model {

    function __construct() {
    	parent::__construct();
        $this->table_name='clients';
        $this->table_name1='sources';
        $this->table_name2='users';
    }
    function validateData($data='',$type='') {
        if($type!="" && $type == true){
            $_POST['client'] = $data;
        }
        $this->form_validation->set_rules($this->config->item('client', 'admin_validationrules'));
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    public function get($params,$id='')
    {
        $result = array();
        $data = array();
        $counter = 0;
        $cnt = $params['start'];
        $replace_id = @$_SESSION['replace_id'][$params['randomnumber']];
        /* Get data filterwise */
        if(isset($params['filter_type']) && $params['filter_type']==1){
            $result['totalRecords']= sizeof($this->getData($params,'count'));
            $fetch_result = $this->getData($params,'');
        }else{
            $result['totalRecords']= sizeof($this->getReportData($params,'count'));
            $fetch_result = $this->getReportData($params,'');
        }
        if (sizeof($fetch_result) > 0) {
            foreach ($fetch_result as $key => $value) {
                $action2='';
                $description_txt ='No call yet';
                if($value['description']=="" && $value['call_count']>0 && $params['filter_type']==1){
                    $value['description']='-';
                }
                if($params['filter_type']==2){
                    $description_txt='';
                }
                $data[$counter]['sr'] = $cnt+1;
                $data[$counter]['name'] = $value['name'].$value['company']."<br>".$value['mobile_no'];
                if($id=="email"){
                    $action = '<div class="form-group"><div class="checkbox checkbox-primary"><input name="client['.$value['id'].']" id="active'.$value['id'].'" type="checkbox" value="'.$value['id'].'" client_name="'.$value['name'].'" client_email_id="'.$value['email_id'].'"><label for="active'.$value['id'].'"></label>
                    </div></div>';
                }else{
                    $data[$counter]['city'] = $value['city'];
                    $data[$counter]['state'] = $value['state'];
                    /*$data[$counter]['mobile_no'] =$value['mobile_no'] ;
                    $data[$counter]['company'] = $value['company']."<br>".$value['mobile_no'];*/
                    $data[$counter]['description'] = ($value['description']!="") ? $value['description']:$description_txt ;
                    $data[$counter]['follow_up_date'] = ($value['follow_up_date']!="") ? date('d-m-Y',strtotime($value['follow_up_date'])):'No call yet';
                    $data[$counter]['call_count'] = $value['call_count'];
                    $action = '<a class="btn btn-sm btn-info waves-effect waves-light" href="' . ADMIN_PATH . 'client/edit/' .$value['id'] . '">EDIT CLIENT</a>&nbsp;';
                    if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='data_entry_operator'){
                        $action.='<a class="btn btn-sm btn-warning waves-effect waves-light" href="' . ADMIN_PATH . 'follow_up/create/' .$value['id'] . '">CALL/VISIT</a>&nbsp;';
                    }
                    if(isset($_SESSION['user_role']) && $_SESSION['user_role']=='Admin'){
                       $action.='<a class="btn btn-sm btn-danger waves-effect waves-light" href="javascript:void(0);" onclick="confirmDelete('.$value['id'].',\'client\','.$value['call_count'].')">DELETE</a>';
                    }
                }
                if($replace_id==""){
                    $action2  = '<a href="javascript:void(0);" curent_id="'.$value["id"].'" onclick=replace_data('.$value['id'].',"client") id="replace_client'.$value["id"].'">Replace & Remove</a>';
                }else if($replace_id!=$value["id"]){
                    $action2  = '<a curent_id="'.$value["id"].'" onclick=replace_with_this('.$value['id'].',"client") class="btn btn-sm btn-danger waves-effect waves-light" id="replace_client'.$value["id"].'">Select</a>';
                }
                
                $data[$counter]['remove_replace'] =$action2;
                $data[$counter]['action'] =$action;
                $counter++;
                $cnt++;
            }
        }
        $result['list']=$data;
        return $result;
    }
    public function getData($params,$type)
    {
        $search_arr =array('category','target','source','city','name','state','company');
        foreach ($search_arr as $skey => $svalue) {
            if(isset($params[$svalue]) && $params[$svalue]!=""){
                if($svalue=='name' || $svalue=="company"){
                    $this->db->like($svalue,trim($params[$svalue]));
                }else{
                    $this->db->where($svalue,trim($params[$svalue]));
                }
            }
        }
        if(isset($params['call_filter']) && $params['call_filter']=='pending'){
            $this->db->where('f.client_id is null');
        }else if(isset($params['form_filter']) && $params['form_filter']=='incomplete'){
             $this->db->where('(person_1_name is null or person_2_name is null) or (person_1_designation is  null or person_2_designation is null) or (person_1_email_id is  null or person_2_email_id is null) or (person_1_mobile_no is null or person_2_mobile_no is null) or (person_1_birth_date="0000-00-00" or person_1_birth_date="0000-00-00")');
        }
        if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
            $this->db->where('key_ac_manager',$_SESSION['user_id']);
        }
        $this->db->select("c.*,count(f.id) as call_count,f2.description,f2.follow_up_date") ;
        $this->db->from($this->table_name." c");
        $this->db->join('follow_ups f','f.client_id=c.id','left');
        $this->db->join('follow_ups f2','f2.id = (SELECT MAX(f1.id) FROM follow_ups as f1 WHERE f1.client_id=f.client_id)','left');
        $this->db->order_by("c.id",'desc');
        $this->db->group_by("c.id");
        if($type!='count')
        {
            $this->db->limit($params['length'],$params['start']);
        }
        $result = $this->db->get()->result_array();
        //echo $this->db->last_query();die;
        return $result;
    }
    public function getReportData($params,$type)
    {
        if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
            $this->db->where('key_ac_manager',$_SESSION['user_id']);
        }
        if($params['report_type']==1){
            if(isset($params['start_date']) && $params['start_date']!="" && isset($params['end_date']) && $params['end_date']!=""){
                 $this->db->where("f2.follow_up_date BETWEEN '".date('Y-m-d',strtotime($params['start_date']))."' AND '".date('Y-m-d',strtotime($params['end_date']))."'");
            }
            $this->db->select("c.*,count(f.id) as call_count,f2.description,f2.follow_up_date") ;
            $this->db->from("follow_ups f");
            $this->db->join('clients c','c.id=f.client_id','left');
            $this->db->join('follow_ups f2','f2.id = (SELECT MAX(f1.id) FROM follow_ups as f1 WHERE f1.client_id=f.client_id)','left');
            $this->db->order_by("f.id",'desc');
            $this->db->group_by("f.client_id");
        }else{
            if(isset($params['start_date']) && $params['start_date']!="" && isset($params['end_date']) && $params['end_date']!=""){
                 $this->db->where("v2.visit_date BETWEEN '".date('Y-m-d',strtotime($params['start_date']))."' AND '".date('Y-m-d',strtotime($params['end_date']))."'");
            }
            $this->db->select("c.*,count(v.id) as call_count,v2.description,v2.visit_date as follow_up_date") ;
            $this->db->from("visits v");
            $this->db->join('clients c','c.id=v.client_id','left');
            $this->db->join('visits v2','v2.id = (SELECT MAX(v1.id) FROM visits as v1 WHERE v1.client_id=v.client_id)','left');
            $this->db->order_by("v.id",'desc');
            $this->db->group_by("v.client_id");
        }
        if($type!='count')
        {
            $this->db->limit($params['length'],$params['start']);
        }
        $result = $this->db->get()->result_array();
      
        return $result;
    }
    public function getById($id)
    {
        $result=$this->db->get_where($this->table_name,array('id'=>$id))->row_array();
        return $result;
    }
    public function getSource($name='')
    {
        if($name==""){
            $result=$this->db->get($this->table_name1)->result_array();
        }else{
            $this->db->where('name',trim($name));
            $this->db->select('id');
            $this->db->from($this->table_name1);
            $response =$this->db->get()->row_array();
            $result=$response['id'];
        }
        return $result;
    }
    public function getKeymanager()
    {
        if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
            $this->db->where('id',$_SESSION['user_id']);
        }
        $this->db->where('user_role!=','Admin');
        $this->db->where('status', 1);
        $result=$this->db->get($this->table_name2)->result_array();
        return $result;
    }
    public function getDesignation()
    {
        $result=$this->db->get('designations')->result_array();
        return $result;
    }
    public function save($id='',$data,$type='')
    {
        if($type!='' && $type==TRUE){
            foreach ($data['result'] as $key => $value) {
                if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
                    $value['key_ac_manager'] =$_SESSION['user_id'];
                }
                if($value['source']!=""){
                    $value['source']=$this->getSource($value['source']);
                }
                $value['start_date']=($value['start_date']!="" && $value['start_date']!='0000-00-00') ? date('Y-m-d',strtotime($value['start_date'])) :'0000-00-00';
                $value['birth_date']=($value['birth_date']!="" && $value['birth_date']!='0000-00-00') ? date('Y-m-d',strtotime($value['birth_date'])) :'0000-00-00';
                $value['target']=(isset($value['target']) && $value['target']!="") ? $value['target']: 0;
                $value['person_2_birth_date']=($value['person_2_birth_date']!="" && $value['person_2_birth_date']!='0000-00-00') ? date('Y-m-d',strtotime($value['person_2_birth_date'])) :'0000-00-00';
                $value['person_1_birth_date']=($value['person_1_birth_date']!="" && $value['person_1_birth_date']!='0000-00-00') ? date('Y-m-d',strtotime($value['person_1_birth_date'])) :'0000-00-00';
                $value['showroom_anniversary']=($value['showroom_anniversary']!="" && $value['showroom_anniversary']!='0000-00-00') ? date('Y-m-d',strtotime($value['showroom_anniversary'])) :'0000-00-00';
                $value['created_at']=date('Y-m-d H:i:s');
                $this->db->insert($this->table_name,$value);
            }
        }else{
            if(isset($_SESSION['user_role']) && $_SESSION['user_role']!='Admin'){
                $data['key_ac_manager'] =$_SESSION['user_id'];
            }
            $data['start_date']=($data['start_date']!="" && $data['start_date']!='0000-00-00') ? date('Y-m-d',strtotime($data['start_date'])) :'0000-00-00';
            $data['birth_date']=($data['birth_date']!="" && $data['birth_date']!='0000-00-00') ? date('Y-m-d',strtotime($data['birth_date'])) :'0000-00-00';
            $data['person_2_birth_date']=($data['person_2_birth_date']!="" && $data['person_2_birth_date']!='0000-00-00') ? date('Y-m-d',strtotime($data['person_2_birth_date'])) :'0000-00-00';
            $data['person_1_birth_date']=($data['person_1_birth_date']!="" && $data['person_1_birth_date']!='0000-00-00') ? date('Y-m-d',strtotime($data['person_1_birth_date'])) :'0000-00-00';
            $data['showroom_anniversary']=($data['showroom_anniversary']!="" && $data['showroom_anniversary']!='0000-00-00') ? date('Y-m-d',strtotime($data['showroom_anniversary'])) :'0000-00-00';
            if($id=="")
            {
                $data['created_at']=date('Y-m-d H:i:s');
                $this->db->insert($this->table_name,$data);
            }else{
                $data['updated_at']=date('Y-m-d H:i:s');
                $this->db->update($this->table_name,$data,array('id'=>$id));
            }
        }
        return get_successMsg($id);
    }
    public function delete($data)
    {
        $response_data=array();
        $result=$this->db->delete($this->table_name,array("id"=>$data['id']));
        if($result){
            $this->db->delete('visits',array('client_id'=>$data['id']));
            $response_data['status']='success';
            $response_data['data']='Client deleted successfully.';
        }else{
            $response_data['status']='error';
            $response_data['data']='Error !!';
        }
        return $response_data;
    }
    public function getCategory()
    {
       $result= $this->db->get('categories')->result_array();
       return $result;
    }
    public function replace($replace_id,$id)
    {
        $follow_ups_data=$this->db->get_where('follow_ups',array('client_id'=>$replace_id))->result_array();
        if(sizeof($follow_ups_data)>0){
            foreach ($follow_ups_data as $key => $value) {
                $update_arr=array(
                    'client_id'=>$id,
                    'updated_at'=>date('Y-m-d H:i:s'),
                );
               $this->db->update('follow_ups',$update_arr,array('id'=>$value['id']));
            }
        }
        $visits_data=$this->db->get_where('visits',array('client_id'=>$replace_id))->result_array();
        if(sizeof($visits_data)>0){
            foreach ($visits_data as $key1 => $value1) {
                $update_arr=array(
                    'client_id'=>$id,
                    'updated_at'=>date('Y-m-d H:i:s'),
                );
               $this->db->update('visits',$update_arr,array('id'=>$value1['id']));
            }
        }
        $this->db->delete('clients',array('id'=>$replace_id));
    }
}    