 <?php

class Follow_up_model extends CI_Model {

    function __construct() {
    	parent::__construct();
        $this->table_name='follow_ups';
        $this->table_name1='clients';
    }
    function validateData($type) {
        $this->form_validation->set_rules($this->config->item($type, 'admin_validationrules'));
        if ($this->form_validation->run() == FALSE) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    public function get($params)
    {
        $result = array();
        $data = array();
        $counter = 0;
        $cnt = $params['start'];
        $result['totalRecords']= sizeof($this->getData($params,'count'));
        $fetch_result = $this->getData($params,'');
        // print_r($result['totalRecords']); echo count($fetch_result); exit;
        if (sizeof($fetch_result) > 0) {
            foreach ($fetch_result as $key => $value) {
                $type='';
                $visit='-';
                $data[$counter]['description'] = ($value['description']!="") ? $value['description']:'-' ;
                $data[$counter]['visit_date'] = (isset($value['visit_date']) && $value['visit_date']!="") ? date('d-m-Y',strtotime($value['visit_date'])):date('d-m-Y',strtotime($value['follow_up_date']));
                if(isset($value['visit_date']) && $value['visit_date']!=""){
                    $type='<span class="label label-success">Visit</span>';
                    if($value['visit_done']==1){
                        $visit_done='Visit Done';
                        $visit_class='label-danger';
                    }else{
                        $visit_done='Appointment';
                        $visit_class='label-default'; 
                    }
                    $visit='<span class="label '.@$visit_class.'">'.@$visit_done.'</span>';
                    $visit_type=($value['type']==1) ? 'Office Visit' : 'Marketing Visit';
                }else{
                    $type='<span class="label label-info">Call</span>';
                    $visit_type=($value['type']==1) ? 'Call Done' : 'Appointment';
                }
                $data[$counter]['type'] = $type;
                $data[$counter]['visit'] = $visit;
                $data[$counter]['visit_type'] = $visit_type;

                $counter++;
                $cnt++;
            }
        }
        $result['list']=$data;
        
        return $result;
    }
    public function getData($params,$limit="")
    {
        $follow_ups=array();
        $visits =array();
        $result =array();
        $follow_ups=$this->fetchData($params,$limit,'follow_ups');
        $visits=$this->fetchData($params,$limit,'visits');
        
        $result =array_merge_recursive($follow_ups, $visits);
        $visit_date = array_column($result, 'visit_date');
        if(!empty($visit_date)){
            usort($result, array($this, "sort_by_column")); 
        }
        if(empty($limit)){
           $result = array_slice($result, $params['start'], $params['length']);
        }
        
        return $result;
    }
    public function fetchData($params,$limit,$table_name)
    {
        if ($table_name=="follow_ups") {
            $this->db->select('*,follow_up_date as visit_date_sort');
        }else{
            $this->db->select('*,visit_date as visit_date_sort'); 
        }
        $this->db->order_by("id",'desc');
        // if(empty($limit)){
        //     $this->db->limit($params['length'],$params['start']);
        // }
        $result=$this->db->get_where($table_name,array('client_id' =>$params['client_id']))->result_array();
        return $result;
    }
    public function getClient($client_id)
    {
        $result=$this->db->get_where($this->table_name1,array("id"=>$client_id))->row_array();
        return $result;
    }
    public function save($type,$data)
    {
        if($type=="visits"){
            $id=$this->input->post('v_id');
            $data['created_by']=$_SESSION['user_id'];
            $data['visit_date']=($data['visit_date']!="" && $data['visit_date']!='0000-00-00') ? date('Y-m-d',strtotime($data['visit_date'])) :'0000-00-00';
        }else{
            $id=$this->input->post('f_id');
            $data['created_by']=$_SESSION['user_id'];
            $data['follow_up_date']=($data['follow_up_date']!="" && $data['follow_up_date']!='0000-00-00') ? date('Y-m-d',strtotime($data['follow_up_date'])) :'0000-00-00';
        }
        $data['created_at']=date('Y-m-d H:i:s');
        $res=$this->db->insert($type,$data);
        // echo $id; print_r($data); die();
        if($id!=""){
            $this->db->update($type,array('status'=>'Close'),array('id'=>$id));
        }
        if($res){
            return get_successMsg();
        }else{
            return get_errorMsg();
        }
    }

    private function sort_by_column($a,$b,$col_name='visit_date_sort'){
        if ($a[$col_name] == $b[$col_name]) {
            return 0;
        }
        return ($a[$col_name] > $b[$col_name]) ? -1 : 1;
    }
}    