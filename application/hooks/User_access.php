<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_access{
	protected $ci;
	public function __construct()
	{
        global $CI;
        $this->ci = $CI;
 	}

	public function index()
	{
		$result = $this->get_user_access();
		if(!empty($result)){
			foreach ($result as $key => $value) {
				foreach($value as $skey => $svalue) {
					$controllers[] = $svalue['name'];
				}
			}
			$controllers = $this->filter_data($controllers);
			if(!in_array(strtolower($this->ci->uri->segment(1)),$controllers) && !empty($this->ci->uri->segment(1))){
				//echo "You don't have a permission to access this page";die;
				redirect(ADMIN_PATH.'permission');die;
			}
		}
	}
	private function get_user_access()
	{
		$result=array();
		$navigation=get_menu();
		if(isset($_SESSION['user_role']) && $_SESSION['user_role']=='Admin'){
            $result=$navigation;
		}else{
			$user_menu=get_menu_role_wise(@$_SESSION['user_role']);
			if(sizeof($user_menu)>0){
				foreach($user_menu as $key => $value){
			        if(isset($navigation[$value])){
			          $result[$value]=$navigation[$value];
			        }
			    }
			}
		}
		return $result;
	}
	private function filter_data($array){
		array_push($array, 'auth','permission','export_excel','import_excel','call_visit','notification','follow_up');
		array_push($array, 'Auth','Permission','Export_excel','Import_excel','call_visit','Notification','Follow_up');
		if(isset($_SESSION['user_role']) && $_SESSION['user_role']=='Admin'){
			array_push($array,'manage_user');
		}
	    $array = array_filter($array, 'strlen');  //removes null values but leaves "0"
	    $array = array_filter($array);
	    $strtolower = array_map('strtolower',$array);
	    $strtolower = array_map('trim',$strtolower);
	    return $strtolower;
  	}
}
?>