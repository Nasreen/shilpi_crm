<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {
	public function __construct() {
        
        parent::__construct();
        $this->load->model(array('client_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        get_action_access('client',$_SESSION['user_role']);
        $this->load->config('admin_validationrules', TRUE);
    }
    public function index($filter='')
    {
        $data = array();
        $data['main_title'] = 'Clients';
        $data['page_title'] = 'Clients';
        $data['head_title'] = 'All Clients';
        $data['source'] = $this->client_model->getSource();
        $data['categories'] = $this->client_model->getCategory();
        $data['states'] = get_state();
        
        /* Dashboard Client Filters */
        $data['target_filter'] = isset($_GET['target']) ? $_GET['target']: '';
        $data['category_filter'] = isset($_GET['category']) ? $_GET['category']: '';
        $data['call_filter']= isset($_GET['calls']) ? $_GET['calls']: '';
        $data['form_filter']= isset($_GET['form']) ? $_GET['form']: '';

        $list=$this->input->post('list');
        if($list !="")
        {   
            $params = $_REQUEST;
            $queryRecords = $this->client_model->get($params,$filter); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('client/index',$data);
        } 
    }
    public function create()
    {
        $data = array();
        $data['main_title'] = 'Clients';
        $data['page_title'] = 'Clients';
        $data['head_title'] = 'Add Client';
        $data['source'] = $this->client_model->getSource();
        $data['categories'] = $this->client_model->getCategory();
        $data['key_manager'] = $this->client_model->getKeymanager();
        $data['designation'] = $this->client_model->getDesignation();
        $data['country'] = get_country();

        $this->view->render('client/add',$data);
    }
    public function edit($id)
    {
        $data = array();
        $data['main_title'] = 'Clients';
        $data['page_title'] = 'Clients';
        $data['head_title'] = 'Edit Client';
        $data['source'] = $this->client_model->getSource();
        $data['categories'] = $this->client_model->getCategory();
        $data['key_manager'] = $this->client_model->getKeymanager();
        $data['client']=$this->client_model->getById($id);
        $data['from_page']=get_last_url_segment($_SERVER['HTTP_REFERER']);
        $data['designation'] = $this->client_model->getDesignation();
        $data['country'] = get_country();

        $this->view->render('client/edit',$data);
    }
    public function store()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post('client');
            $response_data = $this->client_model->save('',$postData);
        }
        echo json_encode($response_data);
    }
    public function update()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post();
            $response_data = $this->client_model->save($postData['id'],$postData['client']);
        }
        echo json_encode($response_data);
    }
    public function validation()
    {
        $postData=$this->input->post('client');
        $response_data =array();
        $validation_result=$this->client_model->validateData('','');
        if($validation_result == FALSE)
        {
            $response_data['status'] = 'failure';
            $response_data['data'] = '';
            $response_data['error'] = array(
                'category'=>strip_tags(form_error('client[category]')),
                'name'=>strip_tags(form_error('client[name]')),
                'company'=>strip_tags(form_error('client[company]')),
                'mobile_no'=>strip_tags(form_error('client[mobile_no]')),
                'email_id'=>strip_tags(form_error('client[email_id]')),
                'pincode'=>strip_tags(form_error('client[pincode]')),
                'landline_no2'=>strip_tags(form_error('client[landline_no2]')),
                'landline_no1'=>strip_tags(form_error('client[landline_no1]')),
                'person_2_email_id'=>strip_tags(form_error('client[person_2_email_id]')),
                'person_2_mobile_no'=>strip_tags(form_error('client[person_2_mobile_no]')),
                'person_1_email_id'=>strip_tags(form_error('client[person_1_email_id]')),
                'person_1_mobile_no'=>strip_tags(form_error('client[person_1_mobile_no]')),
                'source'=>strip_tags(form_error('client[source]')),
                'referred_mobile_no_1'=>strip_tags(form_error('client[referred_mobile_no_1]')),
                'referred_mobile_no_2'=>strip_tags(form_error('client[referred_mobile_no_2]')),
            );
        }
        /*if($postData['mobile_no']=="" && $postData['landline_no1']==""){
            $response_data['status'] = 'failure';
            $response_data['error']['mobile_no']='Please Enter Mobile No/Landline 1';
            $response_data['error']['landline_no1']='Please Enter Mobile No/Landline 1';
        }*/
        return $response_data;
    }
    public function import_excel()
    {
        $data = array();
        $data['main_title'] = 'Clients';
        $data['page_title'] = 'Import Clients';
        $data['head_title'] = 'Import Clients';
       
        $this->view->render('client/import',$data);
    }

    public function send_email()
    {
        $data = array();
        $data['main_title'] = 'Clients';
        $data['page_title'] = 'Client Emails';
        $data['head_title'] = 'Send Email';
        $data['states'] = get_state();
        
        $list=$this->input->post('list');
        if($list !="")
        {   
            $params = $_REQUEST;
            $queryRecords = $this->client_model->get($params,'email'); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('client/send_email',$data);
        } 
    }

    public function delete()
    {
        $postData=$this->input->post();
        $response_data=$this->client_model->delete($postData);
        echo json_encode($response_data);
    }

    public function getCityByState()
    {
        $state=$this->input->post('state');
        $added_city=$this->input->post('city');
        $city = get_city($state);
        $html='<option value="">Select City</option>';

        foreach ($city as $key => $value) {
            $selected="";
            if($added_city!="" && $added_city==$value['city_name']){
                $selected="selected";
            }
            $html.='<option value="'.$value['city_name'].'" '.$selected.'>'.$value['city_name'].'</option>';
        }
        echo json_encode(array('city'=>$html));
    }
    public function getStateByCountry()
    {
        $country=$this->input->post('country');
        $added_state=$this->input->post('state');
        $state = get_state($country);
        $html='<option value="">Select State</option>';

        foreach ($state as $key => $value) {
            $selected="";
            if($added_state!="" && $added_state==$value['name']){
                $selected="selected";
            }
            $html.='<option value="'.$value['name'].'" '.$selected.'>'.$value['name'].'</option>';
        }
        echo json_encode(array('states'=>$html));
    }

    public function set_id()
    {
        $arr['replace_id'] = array($this->input->post('randomnumber') => $this->input->post('replace_id'));
        $this->session->set_userdata($arr);
    }
    public function replace()
    {
        $replace_id = @$_SESSION['replace_id'][$this->input->post('randomnumber')];
        if(!empty($replace_id)){
            $this->client_model->replace($replace_id,$this->input->post('id'));
        }
        $arr['replace_id'] = array($this->input->post('randomnumber') => "");
        $this->session->set_userdata($arr);
        echo "success";
    }
}    