<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_user extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('manage_user_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        }
        get_action_access('source',$_SESSION['user_role']); 
        $this->load->config('admin_validationrules', TRUE);
    }
    public function index()
    {
        $data = array();
        $data['main_title'] = 'Manage User';
        $data['page_title'] = 'Manage User';
        $data['head_title'] = 'All Users';
        $list=$this->input->post('list');
        if($list !="")
        {   
            $params = $_REQUEST;
            $queryRecords = $this->manage_user_model->get($params,''); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('manage_user/index',$data);
        } 
    }
    public function create()
    {
        $data = array();
        $data['main_title'] = 'Manage User';
        $data['page_title'] = 'Manage User';
        $data['head_title'] = 'Add User';
        $data['master']=$this->manage_user_model->getMasters();

        $this->view->render('manage_user/add',$data);
    }
    public function edit($id)
    {
        $data = array();
        $data['main_title'] = 'Manage User';
        $data['page_title'] = 'Manage User';
        $data['head_title'] = 'Edit User';
        $data['user']=$this->manage_user_model->getById($id);
        $data['master']=$this->manage_user_model->getMasters();
        $data['user_master'] = $this->manage_user_model->getMasters($id);
       
        $this->view->render('manage_user/edit',$data);
    }
    public function store()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post('user');
            $response_data = $this->manage_user_model->save('',$postData);
        }
        echo json_encode($response_data);
    }
    public function update()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post();
            $response_data=$this->manage_user_model->save($postData['id'],$postData['user']);
        }
        echo json_encode($response_data);
    }
    public function validation()
    {
        $response_data =array();
        $validation_result=$this->manage_user_model->validateData();
        //$master_validation_result=$this->manage_user_model->validateAccessData();

        if($validation_result == FALSE || (isset($master_validation_result['status']) && $master_validation_result['status']==FALSE))
        {
            $response_data['status'] = 'failure';
            $response_data['data'] = '';
            $response_data['error'] = array(
                'name'=>strip_tags(form_error('user[name]')),
                'email'=>strip_tags(form_error('user[email]')),
                'password'=>strip_tags(form_error('user[encrypted_password]')),
                'confirm_password'=>strip_tags(form_error('confirm_password')),
                'user_role'=>strip_tags(form_error('user[user_role]')),
            );
            $response_data['error']['controller_id']=@$master_validation_result['controller_id'];
        }
        return $response_data;
    }
    public function change_status()
    {
        $postData=$this->input->post();
        $response_data=$this->manage_user_model->change_status($postData);
        echo json_encode($response_data);
    }
}    