<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Follow_up extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('follow_up_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        get_action_access('follow_up',$_SESSION['user_role']);
        $this->load->config('admin_validationrules', TRUE);
    }
    public function index()
    {
        $params = $_REQUEST;
        /*echo "<pre>";
        print_r($params);die;*/
        $queryRecords = $this->follow_up_model->get($params); 
        echo $result=query_record($queryRecords,$params);
    }
    public function create($client_id)
    {
        $data = array();
        $data['main_title'] = 'Clients';
        $data['page_title'] = 'Follow up';
        $data['head_title'] = 'Add Follow up';
        $data['client'] = $this->follow_up_model->getClient($client_id);
        $data['from_page']=get_last_url_segment($_SERVER['HTTP_REFERER']);
        $this->view->render('follow_up/add',$data);
    }
    public function update_visit($client_id,$id)
    {
        $data = array();
        $data['v_id'] = $id;
        $data['main_title'] = 'Clients';
        $data['page_title'] = 'Follow up';
        $data['head_title'] = 'Add Follow up';
        $data['client'] = $this->follow_up_model->getClient($client_id);
        $data['from_page']=get_last_url_segment($_SERVER['HTTP_REFERER'],2);
        /*echo "<pre>";
        print_r($data);die;*/
        $this->view->render('follow_up/add',$data);
    }
    public function update_call($client_id,$id)
    {
        $data = array();
        $data['f_id'] = $id;
        $data['main_title'] = 'Clients';
        $data['page_title'] = 'Follow up';
        $data['head_title'] = 'Add Follow up';
        $data['client'] = $this->follow_up_model->getClient($client_id);
        $data['from_page']=get_last_url_segment($_SERVER['HTTP_REFERER'],2);
        $this->view->render('follow_up/add',$data);
    }
    public function store_visit()
    {
        $data = array();
        $response_data = $this->validation('visit');
        if(empty($response_data))
        {
            $postData=$this->input->post('visit');
            $response_data = $this->follow_up_model->save('visits',$postData);
        }
        echo json_encode($response_data);
    }
    public function store_follow_up()
    {
        $data = array();
        $response_data = $this->validation('follow_up');
        if(empty($response_data))
        {
            $postData=$this->input->post('follow_up');
            $response_data = $this->follow_up_model->save('follow_ups',$postData);
        }
        echo json_encode($response_data);
    }
    public function validation($type)
    {
        $response_data =array();
        $validation_result=$this->follow_up_model->validateData($type);
        if($validation_result == FALSE)
        {
            $response_data['status'] = 'failure';
            $response_data['data'] = '';
            $response_data['error'] = array(
                'visit_date'=>strip_tags(form_error('visit[visit_date]')),
                'visit_type'=>strip_tags(form_error('visit[type]')),
                'follow_up_date'=>strip_tags(form_error('follow_up[follow_up_date]')),
            );
        }
        return $response_data;
    }
}    