<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('auth_model'));
        $this->load->helper(array('common_helper','core_helper'));
    }

    public function index()
    {
        if ($this->session->userdata('logged_in') == TRUE) {
            redirect(ADMIN_PATH . 'dashboard');
        } else {
            if ($this->input->post('submit') != '') {
                    $login_validation_result = $this->auth_model->validateAdminLogin();
                    if ($login_validation_result == FALSE) {
                        $this->load->view('auth/login');
                    }
                    else{
                        $getuserdetails = userdataFromUsername($this->input->post('username'));
                        $get_default_cntrl = get_default_controller($getuserdetails['id'],$getuserdetails['user_role']);
                        $session_data = array(
                            'user_id' => $getuserdetails['id'],
                            'first_name' => $getuserdetails['first_name'],
                            'last_name' => $getuserdetails['last_name'],
                            'email' => $getuserdetails['email'],
                            'logged_in' => TRUE,
                            'user_role'=>$getuserdetails['user_role'],
                        );
                        $this->session->set_userdata($session_data);
                        if ($this->session->userdata('logged_in') == TRUE) {
                                redirect(ADMIN_PATH . $get_default_cntrl);
                    }
                }
            }else{
                $data['page_title'] = 'Sign In';
                $this->load->view('auth/login',$data);
            }
            
        }
    }   
    public function logout() {
        session_destroy();
        $data['page_title'] = 'Sign In';
        $this->load->view('auth/login',$data);
    }
}