<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Call_visit extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('call_visit_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        $this->load->config('admin_validationrules', TRUE);
    }
    public function call_status($type='')
    {
        $data = array();
        $head_title='';
        $list=$this->input->post('list');

        if($type==1){
            $head_title='Pending Calls';
        }else if($type==2){
            $head_title='Todays calls';
        }else if($type==3){
            $head_title='Tomorrows calls';
        }else if($type==4){
            $head_title='Next weeks calls';
        }else if($type==5){
            $head_title='Appointments for next 5 days';
        }
        $data['main_title'] = 'Dashboard';
        $data['page_title'] = 'Call Status';
        $data['head_title'] = $head_title;
        $data['type'] = (isset($type) && $type!="") ? $type :'';
       if($list !="")
        {   
            $params = $_REQUEST;
            $queryRecords = $this->call_visit_model->getCall($params); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('called_customer/call_index',$data);
        } 
    }

    public function visit_status($type='')
    {
        $data = array();
        $head_title='';
        $list=$this->input->post('list');

        if($type==1){
            $head_title='Appointments for next 5 days';
        }else if($type==2){
            $head_title='Last 30 days no visit clients';
        }else if($type==3){
            $head_title='Visit done by clients';
        }else if($type==4){
            $head_title='Marketing Visit Done';
        }else if($type==5){
            $head_title='Todays calls';
        }else if($type==6){
            $head_title='7 days calls';
        }else if($type==7){
            $head_title='Tomorrows calls';
        }
        $data['main_title'] = 'Dashboard';
        $data['page_title'] = 'Visit Status';
        $data['head_title'] = $head_title;
        $data['type'] = (isset($type) && $type!="") ? $type :'';

       if($list !="")
        {   
            $params = $_REQUEST;
            $queryRecords = $this->call_visit_model->getVisit($params); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('visited_customer/visit_index',$data);
        } 
    }
}    