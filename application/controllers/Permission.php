<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends CI_Controller {
    public function index()
    {
        $data = array();
        $data['main_title'] = 'Access Denied';
        $data['page_title'] = 'Access Denied';
        $data['head_title'] = 'Access Denied';

        $this->view->render('errors/access',$data);
    }
}    