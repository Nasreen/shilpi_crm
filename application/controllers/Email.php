<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('email_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        $this->load->config('admin_validationrules', TRUE);
    }
    public function send_client_email()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post();
            $response_data = $this->email_model->client_email($postData);
        }
        echo json_encode($response_data); 
    }
    public function validation()
    {
        $response_data =array();
        $validation_result=$this->email_model->validateData('client_email');
        $other_validation =$this->email_model->validateClientData();
        if($validation_result == FALSE || $other_validation['status']==FALSE)
        {
            $response_data['status'] = 'failure';
            $response_data['data'] = '';
            $response_data['error'] = array(
                'subject'=>strip_tags(form_error('subject')),
                'email_body'=>strip_tags(form_error('email_body')),
                'client'=>@$other_validation['client'],
            );
        }
        return $response_data;
    }
}    