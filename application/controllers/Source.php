<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Source extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('source_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        get_action_access('source',$_SESSION['user_role']);
        $this->load->config('admin_validationrules', TRUE);
    }
    public function index()
    {
        $data = array();
        $data['main_title'] = 'Source Master';
        $data['page_title'] = 'Source Master';
        $data['head_title'] = 'All Sources';
        $list=$this->input->post('list');
        if($list !="")
        {   
            $params = $_REQUEST;
            $queryRecords = $this->source_model->get($params,''); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('source/index',$data);
        } 
    }
    public function create()
    {
        $data = array();
        $data['main_title'] = 'Source Master';
        $data['page_title'] = 'Source Master';
        $data['head_title'] = 'Add Source';
        
        $this->view->render('source/add',$data);
    }
    public function edit($id)
    {
        $data = array();
        $data['main_title'] = 'Source Master';
        $data['page_title'] = 'Source Master';
        $data['head_title'] = 'Edit Source';
        $data['source']=$this->source_model->getById($id);
        $this->view->render('source/edit',$data);
    }
    public function store()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post('source');
            $response_data = $this->source_model->save('',$postData);
        }
        echo json_encode($response_data);
    }
    public function update()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post();
            $response_data = $this->source_model->save($postData['id'],$postData['source']);
        }
        echo json_encode($response_data);
    }
    public function validation()
    {
        $response_data =array();
        $validation_result=$this->source_model->validateData();
        if($validation_result == FALSE)
        {
            $response_data['status'] = 'failure';
            $response_data['data'] = '';
            $response_data['error'] = array(
                'name'=>strip_tags(form_error('source[name]')),
            );
        }
        return $response_data;
    }
}    