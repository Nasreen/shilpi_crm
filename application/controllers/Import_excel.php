<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Import_excel extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('import_excel_model','client_model'));
        $this->load->library(array('excel_lib'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        $this->load->config('admin_validationrules', TRUE);
    }
    public function store_client()
    {
        $data = array();
        $response_data = $this->client_validation();
        if(empty($response_data))
        {
            $postData= $this->excel_lib->import('clients',$_FILES['file']);
            $response_data = $this->client_model->save('',$postData,true);
        }
        echo json_encode($response_data);
    }
    public function client_validation()
    {
        $response_data =array();
        $validation_result=$this->import_excel_model->validate_client();
        if($validation_result['status'] == FALSE)
        {
            unset($validation_result['status']);
            $response_data['status'] = 'failure';
            $response_data['data'] = '';
            $response_data['error'] = $validation_result;
        }else if(isset($validation_result['other_validation']) && $validation_result['other_validation'] == TRUE){
            foreach ($validation_result['result'] as $key => $value) {
               $other_validation = $this->client_model->validateData($value,true);
                if($other_validation == FALSE)
                {
                    $response_data['status'] = 'failure';
                    $response_data['data'] = '';
                    $response_data['error'][$key] = array(
                        'category'=>strip_tags(form_error('client[category]')),
                        'name'=>strip_tags(form_error('client[name]')),
                        'company'=>strip_tags(form_error('client[company]')),
                        'mobile_no'=>strip_tags(form_error('client[mobile_no]')),
                        'email_id'=>strip_tags(form_error('client[email_id]')),
                        'pincode'=>strip_tags(form_error('client[pincode]')),
                        'landline_no2'=>strip_tags(form_error('client[landline_no2]')),
                        'landline_no1'=>strip_tags(form_error('client[landline_no1]')),
                        'person_2_email_id'=>strip_tags(form_error('client[person_2_email_id]')),
                        'person_2_mobile_no'=>strip_tags(form_error('client[person_2_mobile_no]')),
                        'person_1_email_id'=>strip_tags(form_error('client[person_1_email_id]')),
                        'person_1_mobile_no'=>strip_tags(form_error('client[person_1_mobile_no]')),
                        'person_1_birth_date'=>strip_tags(form_error('client[person_1_birth_date]')),
                        'person_2_birth_date'=>strip_tags(form_error('client[person_2_birth_date]')),
                        'start_date'=>strip_tags(form_error('client[start_date]')),
                        'birth_date'=>strip_tags(form_error('client[birth_date]')),
                        'showroom_anniversary'=>strip_tags(form_error('client[showroom_anniversary]')),
                        'source'=>strip_tags(form_error('client[source]')),
                        'target'=>strip_tags(form_error('client[target]')),
                        'referred_mobile_no_1'=>strip_tags(form_error('client[referred_mobile_no_1]')),
                        'referred_mobile_no_2'=>strip_tags(form_error('client[referred_mobile_no_2]')),
                    );
                }
                /*if($value['mobile_no']=="" && $value['landline_no1']==""){
                    $response_data['status'] = 'failure';
                    $response_data['error'][$key]['mobile_no']='Please Enter Mobile No/Landline 1';
                    $response_data['error'][$key]['landline_no1']='Please Enter Mobile No/Landline 1';
                }*/
            }
        }
        return $response_data;
    }
}    