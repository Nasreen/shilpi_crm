<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('categories_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        }
        get_action_access('categories',$_SESSION['user_role']);  
        $this->load->config('admin_validationrules', TRUE);
    }
    public function index()
    {
        $data = array();
        $data['main_title'] = 'Categories';
        $data['page_title'] = 'Categories';
        $data['head_title'] = 'All Categories';
        $list=$this->input->post('list');
        if($list !="")
        {   
            $params = $_REQUEST;
            $queryRecords = $this->categories_model->get($params,''); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('categories/index',$data);
        } 
    }
    public function create()
    {
        $data = array();
        $data['main_title'] = 'Categories';
        $data['page_title'] = 'Categories';
        $data['head_title'] = 'Add Category';
        
        $this->view->render('categories/add',$data);
    }
    public function edit($id)
    {
        $data = array();
        $data['main_title'] = 'Categories';
        $data['page_title'] = 'Categories';
        $data['head_title'] = 'Edit Category';
        $data['categories']=$this->categories_model->getById($id);
        
        $this->view->render('categories/edit',$data);
    }
    public function store()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post('categories');
            $response_data = $this->categories_model->save('',$postData);
        }
        echo json_encode($response_data);
    }
    public function update()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post();
            $response_data = $this->categories_model->save($postData['id'],$postData['categories']);
        }
        echo json_encode($response_data);
    }
    public function validation()
    {
        $response_data =array();
        $validation_result=$this->categories_model->validateData();
        if($validation_result == FALSE)
        {
            $response_data['status'] = 'failure';
            $response_data['data'] = '';
            $response_data['error'] = array(
                'name'=>strip_tags(form_error('categories[name]')),
            );
        }
        return $response_data;
    }
}    