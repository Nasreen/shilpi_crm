<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('dashboard_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        get_action_access('dashboard',$_SESSION['user_role']); 
        $this->load->config('admin_validationrules', TRUE);
    }
    public function index()
    {
        $data = array();
        $data['main_title'] = 'Dashboard';
        $data['page_title'] = 'Dashboard';
        $data['head_title'] = 'Dashboard';
        $data['total_client']= $this->dashboard_model->getClient();
        /*$data['category_a_client']= $this->dashboard_model->getClient('category','A');
        $data['category_b_client']= $this->dashboard_model->getClient('category','B');
        $data['category_c_client']= $this->dashboard_model->getClient('category','C');*/
        $data['category_wise_clients']= $this->dashboard_model->getClientByCategoy();
        $data['targeted_client']= $this->dashboard_model->getClient('target',6);
        $data['incomplete_forms']= $this->dashboard_model->getIncompleteForm();
        
        /* CALLS */
        $data['pending_calls']=$this->dashboard_model->getPendingCall();
        $data['todays_call']= $this->dashboard_model->getCall(date('Y-m-d'));
        $data['tomorrows_call']= $this->dashboard_model->getCall(date('Y-m-d',strtotime('+1 days')));
        $data['next_week_call']= $this->dashboard_model->getNextWeekCall();
        $data['call_appointment']= $this->dashboard_model->getCallAppointment();
        $data['call_done']= $this->dashboard_model->getCall(date('Y-m-d'),'created_at');
        
        /* VISITS */
        $data['client_visit_done']=$this->dashboard_model->getVisit(1);
        $data['last_30days_visits']= $this->dashboard_model->getAppointment(2);
        $data['appointment']= $this->dashboard_model->getAppointment(1);
        $data['marketing_visit_done']=$this->dashboard_model->getVisit(2);
        $data['last_visits']= $this->dashboard_model->getAppointment(5);
        $data['last_21days_visits']= $this->dashboard_model->getAppointment(6);
        $data['last_29days_visits']= $this->dashboard_model->getAppointment(7);
        
        /* REMINDERS */
        $data['birthday_reminder'] = $this->dashboard_model->getReminder('birth_date');
        $data['anniversary_reminder'] = $this->dashboard_model->getReminder('showroom_anniversary');
        if($_SESSION['user_role']=='Admin'){
            $this->view->render('dashboard/index',$data);
        }else if($_SESSION['user_role']=='data_entry_operator'){
            $this->view->render('dashboard/data_entry_index',$data);
        }else if($_SESSION['user_role']=='caller'){
            $this->view->render('dashboard/caller_index',$data);
        }else{
            $this->view->render('dashboard/outdoor_visit_index',$data);
        }
    }
}    