<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Called_customer extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('called_customer_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        get_action_access('called_customer',$_SESSION['user_role']); 
        $this->load->config('admin_validationrules', TRUE);
    }
    public function index($start_date='',$end_date='')
    {
        $data = array();
        $list=$this->input->post('list');
        
        $data['main_title'] = 'Clients';
        $data['page_title'] = 'Called Customer';
        $data['head_title'] = 'All Called Customers';

        $data['start_date'] = (isset($_GET['start_date'])) ? $_GET['start_date'] : '';
        $data['end_date'] = (isset($_GET['end_date'])) ? $_GET['end_date'] : '';
        $data['call']=(isset($_GET['call'])) ? $_GET['call'] : '';
        
        if($list !="")
        {   
            $params = $_REQUEST;
            $start_date=$this->input->post('start_date');
            $end_date =$this->input->post('end_date');
            $queryRecords = $this->called_customer_model->get($params,@$start_date,@$end_date); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('called_customer/index',$data);
        } 
    }
}    