<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Designation extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('designation_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        get_action_access('designation',$_SESSION['user_role']); 
        $this->load->config('admin_validationrules', TRUE);
    }
    public function index()
    {
        $data = array();
        $data['main_title'] = 'Designation Master';
        $data['page_title'] = 'Designation';
        $data['head_title'] = 'All Designations';
        $list=$this->input->post('list');
        if($list !="")
        {   
            $params = $_REQUEST;
            $queryRecords = $this->designation_model->get($params,''); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('designation/index',$data);
        } 
    }
    public function create()
    {
        $data = array();
        $data['main_title'] = 'Designation Master';
        $data['page_title'] = 'Designation';
        $data['head_title'] = 'Add Designation';
        
        $this->view->render('designation/add',$data);
    }
    public function edit($id)
    {
        $data = array();
        $data['main_title'] = 'Designation Master';
        $data['page_title'] = 'Designation';
        $data['head_title'] = 'Edit Designation';
        $data['designation']=$this->designation_model->getById($id);
        $this->view->render('designation/edit',$data);
    }
    public function store()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post('designations');
            $response_data = $this->designation_model->save('',$postData);
        }
        echo json_encode($response_data);
    }
    public function update()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post();
            $response_data = $this->designation_model->save($postData['id'],$postData['designations']);
        }
        echo json_encode($response_data);
    }
    public function validation()
    {
        $response_data =array();
        $validation_result=$this->designation_model->validateData();
        if($validation_result == FALSE)
        {
            $response_data['status'] = 'failure';
            $response_data['data'] = '';
            $response_data['error'] = array(
                'name'=>strip_tags(form_error('designations[name]')),
            );
        }
        return $response_data;
    }
}    