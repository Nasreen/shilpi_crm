<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visited_customer extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('visited_customer_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        get_action_access('visited_customer',$_SESSION['user_role']);
        $this->load->config('admin_validationrules', TRUE);
    }
    public function index($start_date='',$end_date='')
    {
        $data = array();
        $list=$this->input->post('list');
        
        $data['main_title'] = 'Clients';
        $data['page_title'] = 'Visited Customer';
        $data['head_title'] = 'All Visited Customers';
        $data['start_date'] = (isset($_GET['start_date'])) ? $_GET['start_date'] : '';
        $data['end_date'] = (isset($_GET['end_date'])) ? $_GET['end_date'] : '';
        $data['visit_date'] =(isset($_GET['visit_date'])) ? $_GET['visit_date'] : '';

        if($list !="")
        {   
            $params = $_REQUEST;
            $start_date=$this->input->post('start_date');
            $end_date =$this->input->post('end_date');
            $queryRecords = $this->visited_customer_model->get($params,@$start_date,@$end_date); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('visited_customer/index',$data);
        } 
    }
}    