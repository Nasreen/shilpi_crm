<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export_excel extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('export_excel_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        $this->load->config('admin_validationrules', TRUE);
    }
    public function export_client()
    {
        $data=$this->input->post();
        if($data['filter_type']==1){
            $postData=$this->input->post('client');
            $response=$this->export_excel_model->exportClients($postData); 
        }else if($data['filter_type']==2 && $data['report']['type']==1 ){
            $postData=$this->input->post('report');
            $response=$this->export_excel_model->exportCalledClients($postData); 
        }else{
            $postData=$this->input->post('report');
            $response=$this->export_excel_model->exportVisitedClients($postData); 
        } 
    }
    public function export_sample($file_name)
    {
        $file = './uploads/sample/'.$file_name;
         if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
    }
    public function export_visited_client()
    {
        $postData=$this->input->post();
        $response=$this->export_excel_model->exportVisitedClients($postData);
    }
    public function export_called_client()
    {
        $postData=$this->input->post();
        $response=$this->export_excel_model->exportCalledClients($postData);
    }
}    