<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('notification_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        $this->load->config('admin_validationrules', TRUE);
    }
    public function reminder()
    {
        $data = array();
        $list=$this->input->post('list');
        $data['main_title'] = 'Notification';
        $data['page_title'] = 'Reminder';
        $data['head_title'] = (isset($_GET['type']) && $_GET['type']=="birth_date") ? 'Bithday Reminder' :'Anniversary Reminder';
        $data['type'] = isset($_GET['type']) ? $_GET['type'] :'';
         if($list !="")
        {
            $params = $_REQUEST;
            $queryRecords = $this->notification_model->get_reminder($params); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('notification/birth_anniversary_index',$data);
        }
    }
    public function call_log()
    {
        $data = array();
        $list=$this->input->post('list');
        $data['main_title'] = 'Notification';
        $data['page_title'] = 'Call Log';
        $data['head_title'] = 'Call Log';
        $data['start_date'] = (isset($_GET['start_date'])) ? $_GET['start_date'] : '';
        $data['end_date'] = (isset($_GET['end_date'])) ? $_GET['end_date'] : '';
         if($list !="")
        {
            $params = $_REQUEST;
            $queryRecords = $this->notification_model->get_call_log($params); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('notification/call_log_index',$data);
        }
    }
}    