<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class States extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('states_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        $this->load->config('admin_validationrules', TRUE);
    }
    public function index()
    {
        $data = array();
        $data['main_title'] = 'States Master';
        $data['page_title'] = 'States';
        $data['head_title'] = 'All States';
        $list=$this->input->post('list');
        if($list !="")
        {   
            $params = $_REQUEST;
            $queryRecords = $this->states_model->get($params,''); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('states/index',$data);
        } 
    }
    public function create()
    {
        $data = array();
        $data['main_title'] = 'States Master';
        $data['page_title'] = 'States';
        $data['head_title'] = 'Add State';
        $data['country']=$this->states_model->getDetails('countries');

        $this->view->render('states/add',$data);
    }
    public function edit($id)
    {
        $data = array();
        $data['main_title'] = 'States Master';
        $data['page_title'] = 'States';
        $data['head_title'] = 'Edit State';
        $data['states']=$this->states_model->getById($id);
        $data['country']=$this->states_model->getDetails('countries');

        $this->view->render('states/edit',$data);
    }
    public function store()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post('states');
            $response_data = $this->states_model->save('',$postData);
        }
        echo json_encode($response_data);
    }
    public function update()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post();
            $response_data = $this->states_model->save($postData['id'],$postData['states']);
        }
        echo json_encode($response_data);
    }
    public function validation()
    {
        $response_data =array();
        $validation_result=$this->states_model->validateData();
        if($validation_result == FALSE)
        {
            $response_data['status'] = 'failure';
            $response_data['data'] = '';
            $response_data['error'] = array(
                'name'=>strip_tags(form_error('states[name]')),
                'country'=>strip_tags(form_error('states[country]')),
            );
        }
        return $response_data;
    }
}    