<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Countries extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model(array('countries_model'));
        if($this->session->userdata('logged_in') !== TRUE)
        {
            redirect(ADMIN_PATH.'auth');
        } 
        $this->load->config('admin_validationrules', TRUE);
    }
    public function index()
    {
        $data = array();
        $data['main_title'] = 'Countries Master';
        $data['page_title'] = 'Countries';
        $data['head_title'] = 'All Countries';
        $list=$this->input->post('list');
        if($list !="")
        {   
            $params = $_REQUEST;
            $queryRecords = $this->countries_model->get($params,''); 
            echo $result=query_record($queryRecords,$params);
        }else{
            $this->view->render('countries/index',$data);
        } 
    }
    public function create()
    {
        $data = array();
        $data['main_title'] = 'Countries Master';
        $data['page_title'] = 'Countries';
        $data['head_title'] = 'Add Country';
        
        $this->view->render('countries/add',$data);
    }
    public function edit($id)
    {
        $data = array();
        $data['main_title'] = 'Countries Master';
        $data['page_title'] = 'Countries';
        $data['head_title'] = 'Edit Country';
        $data['countries']=$this->countries_model->getById($id);
        $this->view->render('countries/edit',$data);
    }
    public function store()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post('countries');
            $response_data = $this->countries_model->save('',$postData);
        }
        echo json_encode($response_data);
    }
    public function update()
    {
        $data = array();
        $response_data = $this->validation();
        if(empty($response_data))
        {
            $postData=$this->input->post();
            $response_data = $this->countries_model->save($postData['id'],$postData['countries']);
        }
        echo json_encode($response_data);
    }
    public function validation()
    {
        $response_data =array();
        $validation_result=$this->countries_model->validateData();
        if($validation_result == FALSE)
        {
            $response_data['status'] = 'failure';
            $response_data['data'] = '';
            $response_data['error'] = array(
                'name'=>strip_tags(form_error('countries[name]')),
            );
        }
        return $response_data;
    }
}    