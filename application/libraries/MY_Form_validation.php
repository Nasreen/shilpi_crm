<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

    function verifyCredentials() {
        $ci = & get_instance();
        $ci->db->select('*');
        $ci->db->where('email', $ci->input->post('username'));
        $ci->db->where('encrypted_password', MD5($ci->input->post('password')));
        $query = $ci->db->get('users')->row_array();
        if (count($query) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function password_check($str) {
        if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
            return TRUE;
        }
        return FALSE;
    }

    function check_source_name(){
        $ci = & get_instance();
        $postData = $ci->input->post();

        $ci->db->select('*');
        $ci->db->where('name', $ci->input->post('source[name]'));
        if(isset($postData['id']) && $postData['id']!="")
            $ci->db->where('id !=', $postData['id']);
        $query = $ci->db->get('sources')->row_array();
        if (count($query) > 0) {
            return false;
        } else {
            return true;
        }
    }
    function check_showroom_name(){
        $ci = & get_instance();
        $postData = $ci->input->post();

        $ci->db->select('*');
        $ci->db->where('company', $postData['client']['company']);
        $ci->db->where('mobile_no', $ci->input->post('client[mobile_no]'));
        if(isset($postData['id']) && $postData['id']!="")
            $ci->db->where('id !=', $postData['id']);
        $query = $ci->db->get('clients')->row_array();
        if (count($query) > 0) {
            return false;
        } else {
            return true;
        }
    }

    function check_category()
    {
        $ci = & get_instance();
        $category=array('A','B','C');
        $postData = $ci->input->post('client');
        if(isset($postData['category']) && $postData['category']!=""){
            if(in_array($postData['category'], $category)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    function check_user() 
    {
        $ci = & get_instance();
        $postData=$ci->input->post();
        $ci->db->select('*');
        $ci->db->where('email', $ci->input->post('user[email]'));
        if(isset($postData['id']) && $postData['id']!="")
            $ci->db->where('id !=', $postData['id']);
        $query = $ci->db->get('users')->row_array();
        if (count($query) > 0) {
            return false;
        } else {
            return true;
        }
    }
    function check_source_exist()
    {
        $ci = & get_instance();
        $postData=$ci->input->post();
        $query=$ci->db->get_where('sources',array('name'=>$postData['client']['source']))->row_array();
        if(isset($postData['id'])){
            return true;
        }else{
            if(count($query) == 0) {
                return false;
            } else {
                return true;
            }
        }
    }
    function check_target()
    {
        $ci = & get_instance();
        $postData=$ci->input->post('client');
        if(isset($postData['target']) && $postData['target']!=""){
            $target = array('0','1','2','3','4','5','6');
            if(!in_array($postData['target'], $target)){
                return false;
            }else{
                return true;
            }
        }else{
            return true;
        }
    }
    function check_designation_name(){
        $ci = & get_instance();
        $postData = $ci->input->post();

        $ci->db->select('*');
        $ci->db->where('name', $ci->input->post('designations[name]'));
        if(isset($postData['id']) && $postData['id']!="")
            $ci->db->where('id !=', $postData['id']);
        $query = $ci->db->get('designations')->row_array();
        if (count($query) > 0) {
            return false;
        } else {
            return true;
        }
    }
    function check_category_name(){
        $ci = & get_instance();
        $postData = $ci->input->post();

        $ci->db->select('*');
        $ci->db->where('name', $ci->input->post('categories[name]'));
        if(isset($postData['id']) && $postData['id']!="")
            $ci->db->where('id !=', $postData['id']);
        $query = $ci->db->get('categories')->row_array();
        if (count($query) > 0) {
            return false;
        } else {
            return true;
        }
    }
    function validate_table_name()
    {
        $ci = & get_instance();
        $postData = $ci->input->post();
        $post_id = @$ci->input->post('id');
        $table_name = $ci->input->post('table_name');
        $ci->db->where('name',trim($postData[$table_name]['name']));
        if($post_id !='')         
            $ci->db->where('id !=',$post_id);

        $res = $ci->db->get($table_name)->row_array(); 
        if(empty($res))
            return TRUE;
        else
            return FALSE;
    }
}//class