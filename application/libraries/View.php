<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class View 
{
  public $current_site = "";
  public function __construct() {
    $this->ci = & get_instance();
    $this->current_site = $_SERVER['REQUEST_URI'];
  }

  public function render($view, $data = array()){
    $this->loadHeader($data);
    $this->loadNavigation($data);
    $this->loadContentView($view);
    $this->loadFooter();
  }

  private function loadHeader($data){
      $this->ci->load->view('common/header',$data);        
  }
  private function loadNavigation($data){
    $user_access_controller=array();
    $navigation=get_menu();
    if($_SESSION['user_role']=='Admin'){
      $data['navigation']=$navigation;
    }else if(isset($_SESSION['user_role'])) {
      $user_menu=get_menu_role_wise($_SESSION['user_role']);
      foreach($user_menu as $key => $value) {
        if(isset($navigation[$value])){
          $user_access_controller[$value]=$navigation[$value];
        }
      }
      $data['navigation']=$user_access_controller;
    }
    $this->ci->load->view('common/navigation',$data);         
  }

  private function loadContentView($view){
    $this->ci->load->view($view);
  }
  
  private function loadFooter(){
    $this->ci->load->view('common/footer');    
  }
}
