<?php

class Dropbox {

//    use Kunnu\Dropbox\DropboxApp;

    private $client_id = 'kuvx7jfaz6volky';
    private $client_secret = 'ct73miwxe8d0m93';

    function __construct() {
        require_once 'vendor/autoload.php';

        $this->ci = & get_instance();
        $this->access_token = $this->ci->config->item('access-token', 'Dropbox');

        $this->dropbox_client = $this->getClient();
    }

    function getClient() {

        if ($this->checkAccessToken()) {
            $app = new Kunnu\Dropbox\DropboxApp($this->client_id, $this->client_secret, $this->access_token);
        } else {
            $app = new Kunnu\Dropbox\DropboxApp($this->client_id, $this->client_secret);
        }

        return new Kunnu\Dropbox\Dropbox($app);
    }

    function authorize() {

        $authHelper = $this->dropbox_client->getAuthHelper();
        $callbackUrl = 'http://localhost/shilpi_dropbox/dropbox_test/callback';
        if (isset($_GET['code']) && isset($_GET['state'])) {
            $code = $_GET['code'];
            $state = $_GET['state'];
            $accessToken = $authHelper->getAccessToken($code, $state, $callbackUrl);
            echo $accessToken->getToken();
        } else {
            $authUrl = $authHelper->getAuthUrl($callbackUrl);
            redirect($authUrl);
        }
    }

    function checkAccessToken() {
        if (isset($this->access_token) && '' != $this->access_token) {
            $_SESSION['access-token'] = $this->access_token;
            return true;
        } else {
            return false;
        }
    }

    function getFiles() {
        $listFolderContents = $this->dropbox_client->listFolder("/");
        $items = $listFolderContents->getItems();
        $items->all();
        $items->first();
        $items->last();
        echo '<pre>';
        print_r($items);
        echo '</pre>';
    }

    function get($path) {
        $entry = array();
        $entry = $this->dropbox_client->listFolder('/' . $path);
        return $entry;
    }
    public function getMetadataExists($searchQuery){
        $searchResults = $this->dropbox_client->search("/", $searchQuery, ['start' => 0, 'max_results' => 5]);
        $items = $searchResults->getItems();
        $abc = $items->all();
        if(!empty($abc)){
            return true;
        }
        return false;

    }

    public function getThumbnail($path) {
        $size = 'small'; //Default size

        $format = 'jpeg'; //Default format

        $file = $this->dropbox_client->getThumbnail($path, $size, $format);

        return $file->getContents();
    }
    public function download_files($path,$upload_path){
        $file = $this->dropbox_client->download($path);
        //File Contents
        $contents = $file->getContents();
        //Save file contents to disk
        $metadata = $file->getMetadata();
        $name = $metadata->getName();
        $name_arr = explode('/', $path);
       
        file_put_contents($upload_path.$name, $contents);
        return $name;
    }

}
