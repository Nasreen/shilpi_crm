function clientfilterFunction(dataTable)
{
    $('#search-input-click').on('click', function () {
        $("#filter_type").val(1);
        mobile_validation();
        setClientTableHeader();
        if($("#validation_flag").val()==1){
            if(page_title=="Clients"){
                if($("#category").val()=="" && $("#target").val()=="" && $("#source").val()=="" && $("#city").val()=="" && $("#name").val()=="" && $("#states").val()=="" && $("#company").val()==""){
                    $('#search_error').text('Please select one field atleast');
                    return false;
                }else{
                    $('#search_error').text('');
                }
                $('#clientTable').DataTable().destroy();
                client_table_data();
            }else{
                if($("#city").val()=="" && $("#name").val()=="" && $("#mobile_no").val()=="" && $("#company").val()==""){
                    $('#search_error').text('Please select one field atleast');
                    return false;
                }else{
                    $('#search_error').text('');
                }
                $('#clientEmailTable').DataTable().destroy();
                client_email_table_data();
            }
        }else{
            return false;
        }
    });

    $('#report-search-input-click').on('click', function () {
        $("#filter_type").val(2);
        $('#report_error').text('');
        startend_date_validation();
        if($("#validation_flag").val()==1){
            setClientTableHeader();
            if($("#report_type").val()==""){
                $('#report_error').text('Please select Report Type');
                    return false;
            }else{
               $('#clientTable').DataTable().destroy();
                client_table_data();
            }
        }else{
            return false;
        }
    });
}

if (page_title == "Source Master") {
    dataTable=$('#sourceTable').DataTable({
        "info":false,
       "bLengthChange": false,
        bFilter: false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: urls + "source",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}

if (page_title == "Clients") {
    client_table_data();
    clientfilterFunction(dataTable);
}

function client_table_data()
{
    $('#clientTable').DataTable().clear().destroy();
    dataTable=$('#clientTable').DataTable({
        "info":false,
       "bLengthChange": false,
        bFilter: true,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list',randomnumber:randomnumber,category:$("#category").val(),target:$("#target").val(),source:$("#source").val(),city:$("#city").val(),name:$("#name").val(),state:$("#states").val(),company:$("#company").val(),filter_type:$("#filter_type").val(),report_type:$("#report_type").val(),start_date:$("#start_date").val(),end_date:$("#end_date").val(),call_filter:$("#call_filter").val(),form_filter:$("#form_filter").val()},
            url: urls + "client",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },   
    });
    dataTable.page.len(50).draw();
    return dataTable;
}

if (page_title == "Follow up") {
    dataTable=$('#visitTable').DataTable({
        "info":false,
       "bLengthChange": false,
        bFilter: false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        'iDisplayLength':5,
        "ajax": {
            data: {list: 'list',client_id:$("#visit_client_id").val()},
            url: urls + "follow_up",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}

if (page_title == "Visited Customer") {
    dataTable=$('#visitedTable').DataTable({
        "info":false,
       "bLengthChange": false,
        bFilter: false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list',start_date:$("#start_date").val(),end_date:$("#end_date").val(),visit_date:$("#visit_date").val()},
            url: urls + "visited_customer",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}

if (page_title == "Client Emails") {
    client_email_table_data();
    clientfilterFunction();
}
function client_email_table_data()
{
    var rows_selected = [];
    table=$('#clientEmailTable').DataTable({
        "info":false,
        "bLengthChange": false,
         bFilter: false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        'iDisplayLength':20,
        "ajax": {
            data: {list: 'list',filter_type:$("#filter_type").val(),city:$("#city").val(),mobile_no:$("#mobile_no").val(),company:$("#company").val(),name:$("#name").val()},
            url: urls + "client/send_email",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            updateDataTableSelectAllCtrl(table);
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
        'rowCallback': function(row, data, dataIndex){
         // Get row ID
         var rowId = data[0];
         // If row ID is in the list of selected row IDs
         if($.inArray(rowId, rows_selected) !== -1){
            $(row).find('input[type="checkbox"]').prop('checked', true);
            $(row).addClass('selected');
         }
         check = $("#select_all").is(":checked");
         if(check){
            $(row).find('input[type="checkbox"]').prop('checked', true);
            $(row).addClass('selected');
         }
        }
    });

    // Handle click on checkbox
    $('#clientEmailTable tbody').on('click', 'input[type="checkbox"]', function(e)
    {
        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();

        // Get row ID
        var rowId = data[0];

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);
        var client_id=$(this).val();
        var client_name=$(this).attr('client_name');
        var client_email_id=$(this).attr('client_email_id');
        // If checkbox is checked and row ID is not in list of selected row IDs
        if(this.checked && index === -1)
        {
            rows_selected.push(rowId);
          // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        }else if (!this.checked && index !== -1){
            rows_selected.splice(index, 1);
        }
        if(this.checked){
            $row.addClass('selected');
            $("#emailClientForm").append('<input type="hidden" value="'+client_id+'" id="'+client_id+'" name="client_id['+client_id+']"><input type="hidden" id="client_name_'+client_id+'" name="client_name['+client_id+']" value="'+client_name+'"><input type="hidden" id="client_email_'+client_id+'" name="client_email_id['+client_id+']" value="'+client_email_id+'">');
        } else {
            $row.removeClass('selected');
            $("#"+client_id).remove();
            $("#client_name_"+client_id).remove();
            $("#client_email_"+client_id).remove();
        }
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    $('#clientEmailTable').on('click', 'tbody td, thead th:first-child', function(e){
      $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // Handle click on "Select all" control
    $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
      if(this.checked){
         $('#clientEmailTable tbody input[type="checkbox"]:not(:checked)').trigger('click');
      } else {
         $('#clientEmailTable tbody input[type="checkbox"]:checked').trigger('click');
      }
      e.stopPropagation();
    });
}
function updateDataTableSelectAllCtrl(table)
{
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody input[type="checkbox"]', $table);
   var $chkbox_checked    = $('tbody input[type="checkbox"]:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);
   
   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }
    // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }
   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
}

if (page_title == "Called Customer") {
    dataTable=$('#calledTable').DataTable({
        "info":false,
       "bLengthChange": false,
        bFilter: false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list',start_date:$("#start_date").val(),end_date:$("#end_date").val(),call:$("#call").val()},
            url: urls + "called_customer",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}

if (page_title == "Manage User") {
    dataTable=$('#userTable').DataTable({
        "info":false,
        "bLengthChange": false,
        'bFilter': false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: urls + "manage_user",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}

if (page_title == "Reminder") {
    dataTable=$('#reminderTable').DataTable({
        "info":false,
        "bLengthChange": false,
        'bFilter': false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list',type:$("#type").val()},
            url: urls + "notification/reminder",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}
if (page_title == "Call Status") {
    dataTable=$('#callStatusTable').DataTable({
        "info":false,
        "bLengthChange": false,
        'bFilter': false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list',type:$("#type").val()},
            url: urls + "call_visit/call_status",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}
if (page_title == "Visit Status") {
    dataTable=$('#visitStatusTable').DataTable({
        "info":false,
        "bLengthChange": false,
        'bFilter': false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list',type:$("#type").val()},
            url: urls + "call_visit/visit_status",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}
if (page_title == "Designation") {
    dataTable=$('#designationTable').DataTable({
        "info":false,
       "bLengthChange": false,
        bFilter: false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: urls + "designation",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}
if (page_title == "Call Log") {
    dataTable=$('#callLogTable').DataTable({
        "info":false,
       "bLengthChange": false,
        bFilter: false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        'iDisplayLength':5,
        "ajax": {
            data: {list: 'list',client_id:$("#client_id").val(),start_date:$("#start_date").val(),end_date:$("#end_date").val()},
            url: urls + "notification/call_log",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}
if (page_title == "Categories") {
    dataTable=$('#categoriesTable').DataTable({
        "info":false,
       "bLengthChange": false,
        bFilter: false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: urls + "categories",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}
if (page_title == "Countries") {
    dataTable=$('#countriesTable').DataTable({
        "info":false,
       "bLengthChange": false,
        bFilter: false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: urls + "countries",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}
if (page_title == "States") {
    dataTable=$('#statesTable').DataTable({
        "info":false,
       "bLengthChange": false,
        bFilter: false,
        "bSort": false,
        "bProcessing": false,
        "serverSide": true,
        "ajax": {
            data: {list: 'list'},
            url: urls + "states",
            type: "post",
            error: function () {
            }
        },
        "language": {
            "sEmptyTable": "<i>No Data Found.</i>",
            "search": '<i class="fa fa-search"></i>',
            "searchPlaceholder": "Search Name"
        }, 
        "fnDrawCallback": function() {
            $('.dataTables_paginate').hide();
            var page_length=0;
            $('.paginate_button').each(function(i,e){
                page_length=i;
            });
            if(page_length>=3){
                $('.dataTables_paginate').show();
            }
        },
    });
}

/*by default 50*/
// if (dataTable) {
//     dataTable.page.len(10).draw();
//        //$('.dataTables_filter').remove();
// }