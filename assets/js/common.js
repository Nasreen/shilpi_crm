$(window).load(function(){
  $(".preloader").fadeOut();
  $(".ajaxLoader").fadeOut();
});

$(document).ready(function($) {
 /* var h = $('.topbar').outerHeight(); 
  $(".content-page").css('margin-top',h);*/
  $('.dataTables_wrapper .row').find('div').first().remove();
  
  if(head_title=="All Clients" && ($("#target_filter").val()!="" || $("#category_filter").val()!="")){
    $('#search-input-click').click();
  }
  
  jQuery('.dp').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true,
        orientation: "top auto",
    });

  /* For Filter Funnel   */
    $(".select2").select2();
    $("#select2-results-2").niceScroll();
    $("#select2-results-4").niceScroll();
});

function error_msg(data,type)
{
    $(".errors").html('');
    $(".text-required" ).each(function( index ) {
      $( this ).html("");
    });
    for(var key in data) 
    {
      if(type=="class"){
        if($.isNumeric(key)){
          for(var key1 in data[key]) 
          {
            if(data[key][key1]!=""){
              var line_no =" On Line No "+(parseInt(key)+parseInt(1));
              $(".errors").append('<p class="text-danger">'+data[key][key1]+line_no+'</p');
            }
          }
        }else{
          $(".errors").append('<p class="text-danger">'+data[key]+'</p');
        }
      }else{
        $("#"+key+"_error").html(data[key]);
      }  
    }
}

/*  MsgPopup Start   */
function showMsgPopup(title,text,success_url,type)
{
    var shortCutFunction = "success";
    var timeColse=10000;
    if(type=="error")
        var shortCutFunction = "error";
    if(success_url=="")
        timeColse=2000;

    var msg = "<p>"+text+'</p>';
    var title = title;
    var $showDuration = 300;
    var toastIndex = 0;
     
    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": timeColse,
      "extendedTimeOut": 0,
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut",
      "tapToDismiss": false
    };
    var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
    if(success_url!=''){
        $(".ajaxLoader").fadeIn();
        setTimeout(function () {
            close_toast_popup(success_url) ;  
        }, 1000);
    }
}

function close_toast_popup(success_url)
{
    window.location.href=success_url;
}

function mobile_validation()
{
    if(page_title=="Clients"){
      $("#validation_flag").val(1);
      return true;
    }
    var mobNum = $("#mobile_no").val();
    var filter = /^\d*(?:\.\d{1,2})?$/;
    if(mobNum!=""){
      if (filter.test(mobNum)) {
       if(mobNum.length==10){
        $("#validation_flag").val(1);
        $("#mobile_no_error").html("");
       } else {
          $("#validation_flag").val(0);
          $("#mobile_no_error").html("Please enter 10  digit mobile number");
          return false;
        }
      }
      else {
        $("#validation_flag").val(0);
        $("#mobile_no_error").html("Not a valid number");
        return false;
     }
    }else{
      $("#validation_flag").val(1);
      $("#mobile_no_error").html("");
    }
}

function change_visitdate_label(label)
{
    $("#visit_date_label").text(label);
}

function startend_date_validation()
{
  $("#end_date_error").text("");
  $("#validation_flag").val(1);
  var start_date = $("#start_date").val().split("-").reverse().join("-");
  var end_date= $("#end_date").val().split("-").reverse().join("-");

  if($("#start_date").val()!="" && $("#end_date").val()==""){
    $("#end_date_error").text("Please select End Date");
    $("#validation_flag").val(0);
    return false;
  }else if($("#end_date").val()!="" && $("#start_date").val()==""){
    $("#end_date_error").text("Please select Start Date");
    $("#validation_flag").val(0);
    return false;
  }else if($("#end_date").val()!="" && $("#start_date").val()!="" && Date.parse(start_date) > Date.parse(end_date)){
      $("#end_date_error").text("End Date should be greater than Start Date.");
      $("#validation_flag").val(0);
      return false;
  }
  
}



$(document).ready(function(){
  $("#hmenuicon").click(function(){
    $("#sidbarLeft").animate({left:"0"});
    $("#fixBack").fadeIn(500);
    $("body").addClass("fixed");
    $(this).hide();
    $("#hmenuiconClose").fadeIn();
  });
  $("#hmenuiconClose").click(function(){
    $("#sidbarLeft").animate({left:"-300"});
    $("body").removeClass("fixed");
    $(this).hide();
    $("#hmenuicon").fadeIn();
    $("#fixBack").fadeOut(500);
  });

  $("#fixBack").click(function(){
    $("#sidbarLeft").animate({left:"-300"});
    $(this).fadeOut(500);
    $("#hmenuiconClose").hide();
    $("#hmenuicon").show();
    $("body").removeClass("fixed");
  });

});

function clear_filters(){
  var urls =window.location.href.split('?')[0];
  var current_url = window.location.href;
  window.location.href=urls;
}
