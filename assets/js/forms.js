var is_type = '';
var page_count = 0;
var object = {};
var load_more = 0;
var page_reload = true;

function login(urls){
     $.ajax({
        url : urls+"login/auth",
        type: "POST",
        dataType: "json",
        data : $('form#loginform').serialize(),
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                error_msg(data.error);
            }
            else
            {
                if(data.status=='error')
                    $("#invalid_error").html('<b class="text-danger">'+data.data+'</b>');
                else{
                    window.location.href=urls+'Collection';
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {

        }
    });
}
function save_source(action)
{
    $(".ajaxLoader").fadeIn();
    $.ajax({
        url : urls+"source/"+action,
        type: "POST",
        dataType: "json",
        data : $('form#sourceForm').serialize(),
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                $(".ajaxLoader").fadeOut();
                error_msg(data.error,'id');
            }
            else
            {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }
                else{
                    showMsgPopup(data.status,data.data,urls+'source','normal');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
}

function save_client(action)
{
    var r_url=$('#client_redirect_url').val();
    $(".ajaxLoader").fadeIn();
    $.ajax({
        url : urls+"client/"+action,
        type: "POST",
        dataType: "json",
        data : $('form#clientFrom').serialize(),
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                $(".ajaxLoader").fadeOut();
                error_msg(data.error,'id');
            }
            else
            {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }
                else{
                    showMsgPopup(data.status,data.data,r_url,'normal');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
}

function save_import_client(action)
{
    var formData = new FormData(document.querySelector("form"));
    $(".ajaxLoader").fadeIn();
    $.ajax({
        url : urls+"import_excel/"+action,
        type: "POST",
        dataType: "json",
        data : formData,
        processData: false,  
        contentType: false,
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                $("#file").val('');
                $(".ajaxLoader").fadeOut();
                error_msg(data.error,'class');
            }
            else
            {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }
                else{
                    showMsgPopup(data.status,data.data,urls+'client','normal');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
}

function save_visit(action)
{
    $(".ajaxLoader").fadeIn();
    var client_id=$("#visit_client_id").val();
    $.ajax({
        url : urls+"follow_up/"+action,
        type: "POST",
        dataType: "json",
        data : $('form#visitForm').serialize(),
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                $(".ajaxLoader").fadeOut();
                error_msg(data.error,'id');
            }
            else
            {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }
                else{
                    showMsgPopup(data.status,data.data,urls+'follow_up/create/'+client_id,'normal');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
}
function save_follow_up(action)
{
    var client_id=$("#visit_client_id").val();
    $(".ajaxLoader").fadeIn();
    $.ajax({
        url : urls+"follow_up/"+action,
        type: "POST",
        dataType: "json",
        data : $('form#followupForm').serialize(),
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                $(".ajaxLoader").fadeOut();
                error_msg(data.error,'id');
            }
            else
            {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }
                else{
                    showMsgPopup(data.status,data.data,urls+'follow_up/create/'+client_id,'normal');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
}
function save_client_email(action)
{
    var formData = new FormData(document.querySelector("form"));
    $(".ajaxLoader").fadeIn();
    $.ajax({
        url : urls+"email/"+action,
        type: "POST",
        dataType: "json",
        data : formData,
        processData: false,  
        contentType: false,
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                $(".ajaxLoader").fadeOut();
                error_msg(data.error,'id');
            }
            else
            {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }
                else{
                    showMsgPopup(data.status,data.data,urls+'client','normal');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
}

function export_submit(method,page)
{
    $("#end_date_error").text("");
    if(method=="get" && ($("#start_date").val()=="" || $("#end_date").val()=="")){
        $("#end_date_error").text("Please select Start & End Date");
        return false;
    }
    $("#allcp-form").attr('method',method);
    $("#allcp-form").attr('action', urls+page).submit();
}

function save_user(action)
{
    $(".ajaxLoader").fadeIn();
    $.ajax({
        url : urls+"manage_user/"+action,
        type: "POST",
        dataType: "json",
        data : $('form#userForm').serialize(),
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                $(".ajaxLoader").fadeOut();
                error_msg(data.error,'id');
            }
            else
            {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }
                else{
                    showMsgPopup(data.status,data.data,urls+'manage_user','normal');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
}

function confirmInactive(id,status,controller_name,model_status)
{
    $("#model_status").text(model_status);
    $("#myModal").modal();
    $("#model_btn").unbind().click(function(){
        $(".ajaxLoader").fadeIn();
       $.ajax({
            type: "POST",
            data:{id:id,status:status},
            dataType: "json",
            url: urls + controller_name +"/change_status/",
            cache: false,
            success: function (data) {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }   
                else{
                    showMsgPopup(data.status,data.data,urls+controller_name,'normal');
                }
            }
        }); 
    });
}
function confirmDelete(id,controller_name,call_count)
{
    if(controller_name=="client"){
        if(call_count>0){
           $(".ajaxLoader").fadeOut();
           $("#delete_model_btn").attr('disabled','disabled');
           showMsgPopup("Error",'You can not delete this client.','','error'); 
           return false;
        }else{
            $("#delete_model_btn").removeAttr('disabled');
        }
    }
    $("#myDeleteModal").modal();
    $("#delete_model_btn").unbind().click(function(){
        $(".ajaxLoader").fadeIn();
       $.ajax({
            type: "POST",
            data:{id:id},
            dataType: "json",
            url: urls + controller_name +"/delete/",
            cache: false,
            success: function (data) {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }   
                else{
                    showMsgPopup(data.status,data.data,urls+controller_name,'normal');
                }
            }
        }); 
    });
}

function setClientTableHeader(){
    if($("#report_type").val()=="2"){
        $("#report_description").text('Visit Description');
        $("#report_date").text('Visit Date');
        $("#report_count").text('Visit Count');
    }else{
        $("#report_description").text('Call Description');
        $("#report_date").text('Calls Date');
        $("#report_count").text('Call Count');
    }
}

function save_designation(action)
{
    $(".ajaxLoader").fadeIn();
    $.ajax({
        url : urls+"designation/"+action,
        type: "POST",
        dataType: "json",
        data : $('form#designationForm').serialize(),
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                $(".ajaxLoader").fadeOut();
                error_msg(data.error,'id');
            }
            else
            {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }
                else{
                    showMsgPopup(data.status,data.data,urls+'designation','normal');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
}

$(".check_all").change(function(event) {
    var table= $(event.target).closest('table');
    $('td input:checkbox',table).prop('checked',this.checked);
});

function getCityByState(id)
{
    var state=$(id).val();
    if(head_title=="Edit Client"){
        if($("#added_city").val()=="" || $("#added_city").val()==undefined){
            var city="";
            var state=$(id).val();
        }else{
           var city=$("#added_city").val();
            if($(id).val()=="")
                var state=$("#added_state").val();
            else
                var state=$(id).val();
        }
    }
    if($(id).val()!="" || city!=""){
        $.ajax({
            url : urls+"client/getCityByState",
            type: "POST",
            dataType: "json",
            data : {state:state,city:city},
            success: function(data, textStatus, jqXHR)
            {
                $("#city").html(data.city);
                $("#client_city").html(data.city);
            }
        });
    }
}

function save_category(action)
{
    $(".ajaxLoader").fadeIn();
    $.ajax({
        url : urls+"categories/"+action,
        type: "POST",
        dataType: "json",
        data : $('form#categoriesForm').serialize(),
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                $(".ajaxLoader").fadeOut();
                error_msg(data.error,'id');
            }
            else
            {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }
                else{
                    showMsgPopup(data.status,data.data,urls+'categories','normal');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
}

function getStateByCountry(id)
{
    if(head_title=="Edit Client"){
        if($("#added_state").val()=="" || $("#added_state").val()==undefined){
            var state="";
        }else{
           var state=$("#added_state").val();
        }
    }
    if($(id).val()!=""){
        $.ajax({
            url : urls+"client/getStateByCountry",
            type: "POST",
            dataType: "json",
            data : {country:$(id).val(),state:state},
            success: function(data, textStatus, jqXHR)
            {
                $("#client_state").html(data.states);
            }
        });
    }
}

function save_countries(action)
{
    $(".ajaxLoader").fadeIn();
    $.ajax({
        url : urls+"countries/"+action,
        type: "POST",
        dataType: "json",
        data : $('form#countriesForm').serialize(),
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                $(".ajaxLoader").fadeOut();
                error_msg(data.error,'id');
            }
            else
            {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }
                else{
                    showMsgPopup(data.status,data.data,urls+'countries','normal');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
}

function save_states(action)
{
    $(".ajaxLoader").fadeIn();
    $.ajax({
        url : urls+"states/"+action,
        type: "POST",
        dataType: "json",
        data : $('form#statesForm').serialize(),
        success: function(data, textStatus, jqXHR)
        {
            if(data.status=='failure')
            {
                $(".ajaxLoader").fadeOut();
                error_msg(data.error,'id');
            }
            else
            {
                if(data.status=='error'){
                    $(".ajaxLoader").fadeOut();
                    showMsgPopup("Error",data.data,'','error');
                }
                else{
                    showMsgPopup(data.status,data.data,urls+'states','normal');
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown){}
    });
}

/* Replace master data*/
function replace_data(id,controller_name){
  $('.loader').show();
  $.ajax({
    url :urls+controller_name+"/set_id",
    type: "POST",
    data : {randomnumber:randomnumber,replace_id:id},
    success: function(data)
    { 
      $('.loader').hide();
      var page_info_data = dataTable.page.info();
      page_info = page_info_data.page*10;
      table_order = dataTable.order();
      oSearch = $('.dataTables_filter input').val();
      client_table_data();
    },
  });
}

function replace_with_this(id,controller_name){
  !function ($) {
    "use strict";

    var SweetAlert = function () {
    };
    var title = "Are you sure?";
    var msg = "You will not be able to recover this data!";
    var c_button = "Yes, replace with this!";
    var can_button = "No, cancel plx!";
    var showCancelButton = true;

    SweetAlert.prototype.init = function () {
      swal({
        title: title,
        text: msg,
        type: "warning",
        showCancelButton: showCancelButton,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: c_button,
        cancelButtonText: can_button,
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
        if (isConfirm) {
          if(c_button===false){
            swal.close();
          }
          else{
            $('.loader').show();
            $.ajax({
            url :urls+controller_name+"/replace",
            type: "POST",
            data : {randomnumber:randomnumber,id:id},
            success: function(data) { 
              $('.loader').hide();
              var page_info_data = dataTable.page.info();
              page_info = page_info_data.page*10;
              table_order = dataTable.order();
              oSearch = $('.dataTables_filter input').val();
              client_table_data();
              swal("Replaced!", "Your data has been replaced.", "success");
            },
          });
        }          
        } else {
          swal.close();
        }
      });
    },
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
} (window.jQuery),

//initializing 
    function ($) {
        "use strict";
        $.SweetAlert.init()
    }(window.jQuery);
}