-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (i686)
--
-- Host: dbinstanonymous.cy9evftloyct.ap-southeast-1.rds.amazonaws.com    Database: shilpi_crm_staging
-- ------------------------------------------------------
-- Server version	5.5.53-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=357 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (2,'PORT BLAIR '),(3,'SOUTH ANDAMANS'),(4,'ANANTHPUR '),(5,'CHITTOOR'),(6,'GODAVARI'),(7,'GUNTUR'),(8,'KADAPA '),(9,'KAKINADA '),(10,'KURNOOL'),(11,'NARSAPETH'),(12,'NELLORE '),(13,'PALAMNER '),(14,'PRODDATIR '),(15,'RAJAMUNDRY '),(16,'SRIKAKULAM '),(17,'VIJAYAWADA'),(18,'VIJAYWADA '),(19,'VISAKHAPATNAM'),(20,'VIZIANAGARAM '),(21,'WARANGAL '),(22,'GUWAHATI '),(23,'KARIMGANJ '),(24,'SILCHAR '),(25,'BUXAR'),(26,'DARBHANGA '),(27,'GAYA'),(28,'KATIHAR '),(29,'MUZAFFARPUR '),(30,'PATNA '),(31,'BASTAR '),(32,'BHILAI'),(33,'BILASPUR'),(34,'DHAMTARI'),(35,'DURG'),(36,'JAGDALPUR'),(37,'JASHPUR '),(38,'KAWARDHA '),(39,'KORBA '),(40,'RAIGHADH'),(41,'RAIPUR'),(42,'RAJNANDGAON '),(43,'SURGUJA '),(44,'MAPUSA'),(45,'MARGAO '),(46,'PANAJI '),(47,'PANJIM '),(48,'PONDA '),(49,'AHMEDABAD '),(50,'AMRELI '),(51,'ANAND'),(52,'ANKLESHWAR '),(53,'AURANGABAD'),(54,'BARDOLI'),(55,'BASANKANTHA'),(56,'BHARUCH '),(57,'BHAVNAGAR'),(58,'BHUJ'),(59,'BHUJ-KACHCHH'),(60,'CHIKLI'),(61,'GANDHINAGAR'),(62,'HIMATNAGAR '),(63,'JAMNAGAR'),(64,'JUNAGADH'),(65,'KOSAMBA'),(66,'MAHISAGAR'),(67,'MAHUA'),(68,'MEHSANA '),(69,'MORBI '),(70,'NADIAD '),(71,'NAVSARI'),(72,'PADRA'),(73,'PANCHMAHAL '),(74,'PATAN '),(75,'PETLAD'),(76,'PORBANDAR'),(77,'RAJKOT '),(78,'SURAT'),(79,'VADODRA '),(80,'VALSAD'),(81,'VAPI'),(82,'VERAVAL'),(83,'VIYARA'),(84,'AMBALA '),(85,'BAHADURGARH '),(86,'GURGAON'),(87,'HISAR'),(88,'KARNAL '),(89,'PANCHKULA '),(90,'PANIPAT '),(91,'PINJORE '),(92,'ROHTAK '),(93,'SIRSA '),(94,'SONEPAT '),(95,'YAMUNA NAGAR'),(96,'SHIMLA '),(97,'JAMMU'),(98,'JAMSHEDPUR'),(99,'DALTONGANJ '),(100,'DHANBAD '),(101,'EAST SINGHBHUM'),(102,'RANCHI '),(103,'ANKOLA'),(104,'BAGALKOT'),(105,'BANGALORE '),(106,'BELGAUM '),(107,'BELLARY '),(108,'BIJAPUR'),(109,'CHAMRAJNAGAR'),(110,'CHITRADURGA '),(111,'DAKSHINA KANNADA'),(112,'DHARWAD '),(113,'GADAG'),(114,'GULBARGA '),(115,'HASSAN '),(116,'HUBLI '),(117,'KGF '),(118,'KOLAR '),(119,'KUNDAPUR '),(120,'MANGALORE'),(121,'MOODABIDRI'),(122,'MYSORE '),(123,'NAGERCOIL '),(124,'RAICHUR '),(125,'SANKARKOVIL '),(126,'TUMKUR '),(127,'UTTAR KANNADA'),(128,'YESHWANTHPUR'),(129,'NAIROBI '),(130,'KOCHI'),(131,'COCHIN'),(132,'KANNUR '),(133,'KOTTAYAM'),(134,'KOYILANDI'),(135,'KOZHIKODE '),(136,'KOZIKODE'),(137,'MALAPPURAM '),(138,'PALAKKAD'),(139,'THIRUVANANTHAPURAM'),(140,'THIRUVANTHAPURAM'),(141,'THRISSUR'),(142,'BETUL '),(143,'BHOPAL'),(144,'CHHINDWARA '),(145,'DEWAS '),(146,'GUNA '),(147,'GWALIOR '),(148,'HARDA '),(149,'HOSHANGABAD'),(150,'INDORE '),(151,'ITARSI'),(152,'JABALPUR '),(153,'KHARGONE '),(154,'NEEMUCH '),(155,'RATLAM'),(156,'REWA'),(157,'SATNA '),(158,'SHAHDOL '),(159,'SHAJAPUR '),(160,'SHIVPURI'),(161,'SINGROULI'),(162,'UJJAIN '),(163,'VIDISHA '),(164,'AHMADNAGAR'),(165,'AHMEDNAGAR'),(166,'AKOLA'),(167,'AMRAVATI'),(168,'ANDHERI'),(169,'BARAMATI'),(170,'BED '),(171,'BEED '),(172,'BELAPUR'),(173,'BHOSARI '),(174,'BULDHANA '),(175,'CHANDRAPUR'),(176,'DHULE'),(177,'DHULIA '),(178,'GONDIA'),(179,'ISLAMPUR'),(180,'JALGAON'),(181,'JALNA '),(182,'JUNNAR '),(183,'KALYAN'),(184,'KANDHAR '),(185,'KANPUTR'),(186,'KARAD '),(187,'KHAMGAON'),(188,'KOLHAPUR'),(189,'LATUR '),(190,'MAHAD'),(191,'MUMBAI'),(192,'NAGPUR'),(193,'NANDED '),(194,'NANDURBAR '),(195,'NASHIK'),(196,'NAVI MUMBAI '),(197,'PALGHAR'),(198,'PARBHANI '),(199,'PUNE'),(200,'PUSAD '),(201,'RAIGAD '),(202,'RATNAGIRI'),(203,'SANGLI'),(204,'SATARA'),(205,'SHAHADA'),(206,'SHIRUR '),(207,'SINDHUDURG'),(208,'SOLAPUR'),(209,'THANE'),(210,'ULHASNAGAR '),(211,'WARDHA'),(212,'YAVATMAL '),(213,'SHILLONG '),(214,'BIRAT NAGAR'),(215,'KATHMANDU '),(216,'CENTRAL DELHI '),(217,'DELHI'),(218,'EAST DELHI '),(219,'KAROL BAGH'),(220,'NEW DELHI'),(221,'NORTH DELHI'),(222,'NORTH EAST DELHI '),(223,'NORTH WEST DELHI '),(224,'RAJOURI GARDEN '),(225,'SOUTH DELHI '),(226,'WEST DELHI '),(227,'AUCKLAND '),(228,'BOLANGIR'),(229,'ANGUL '),(230,'BALANGIR '),(231,'BARGARH '),(232,'BERHAMPUR '),(233,'BHUBANESWAR '),(234,'BRAHMAPUR'),(235,'CUTTACK '),(236,'GANJAM'),(237,'JAGATSINGHPUR'),(238,'KALAHANDI '),(239,'KHORDA '),(240,'KORAPUT '),(241,'MAYURBHANJ'),(242,'MAYURBHNAJ'),(243,'NAUPADA '),(244,'ROURKELA'),(245,'SAMBALPUR '),(246,'AMRITSAR'),(247,'BATALA'),(248,'BATHINDA '),(249,'CHANDIGARH '),(250,'FARIDKOT '),(251,'HOSIARPUR'),(252,'JALANDHER '),(253,'KAPURTHALA '),(254,'KHANNA '),(255,'KOTAKPURA'),(256,'LUDHIANA '),(257,'MANSA '),(258,'MUKTSAR'),(259,'PATIALA '),(260,'ABOHAR '),(261,'BARNALA '),(262,'FATEHGARH SAHIB '),(263,'PATHANKOT'),(264,'AJMER '),(265,'BALOTRA'),(266,'BANSWARA '),(267,'BARMER '),(268,'BHILWARA'),(269,'BUNDI '),(270,'CHITTORGARH '),(271,'DUNGARPUR'),(272,'GANGANAGAR'),(273,'HANUMANGARH '),(274,'JAIPUR'),(275,'JODHPUR '),(276,'KOTA '),(277,'NIMABAHERA '),(278,'PALI'),(279,'UDAIPUR'),(280,'CHENNAI'),(281,'COIMBATORE '),(282,'CUDDALORE '),(283,'DINDIGUL '),(284,'KANYAKUMARI '),(285,'KARUR '),(286,'MADURAI '),(287,'MAYILADUTHURAI'),(288,'PONDICHERRY'),(289,'SALEM '),(290,'THENI '),(291,'THOOTHUKUDI'),(292,'TIRUCHIRAPALLI'),(293,'TIRUNELVELI '),(294,'TIRUPUR'),(295,'TRIVANDRUM'),(296,'VELLORE '),(297,'VILLUPURAM '),(298,'HYDERABAD'),(299,'KHAMMAM'),(300,'SECUNDERABAD'),(301,'WARNGAL'),(302,'AGARTALA'),(303,'DEHRADUN'),(304,'HARIDWAR'),(305,'NAINITAL '),(306,'AGRA'),(307,'ALIGARH'),(308,'ALLAHABAD'),(309,'BADAUN '),(310,'BANDA '),(311,'BAREILLY '),(312,'BULANDSHAHR'),(313,'GAUTAM BUDDHA NAGAR '),(314,'GONDA '),(315,'GORAKHPUR'),(316,'JAUNPUR '),(317,'JHANSI '),(318,'KANPUR'),(319,'KANPUR,'),(320,'LACKNOW'),(321,'LAKHIMPUR KHERI '),(322,'LALITPUR'),(323,'LUCKNOW '),(324,'MATHURA '),(325,'MEERUT '),(326,'MEERUT CANTT'),(327,'MEERUT CITY '),(328,'MIRZAPUR '),(329,'MORADABAD'),(330,'MUZAFFARNAGAR '),(331,'NOIDA'),(332,'PILIBHIT '),(333,'RAEBARELI'),(334,'SONEBHADRA '),(335,'VARANASI '),(336,'ASANSOL '),(337,'HOWRAH '),(338,'KOLKATA'),(339,'REWARI '),(340,'PUTTUR'),(341,'NARSINGHPUR '),(342,'NAVIMUMBAI'),(343,'OSMANABAD '),(344,'PEN'),(345,'SAANGLI'),(346,'SAHADA'),(347,'SHRIRAMPUR'),(348,'VASHI'),(349,'SOUTH WEST DELHI '),(350,'MOGA '),(351,'ROPAR '),(352,'KOVILPATTI'),(353,'SECUNDRABAD'),(354,'VARANSI'),(355,'MIDNAPUR'),(356,'CHEMBUR');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grade` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `area` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_no2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `landline_no1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `landline_no2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_of_joining` date DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `showroom_anniversary` date DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lead_owner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lead_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_1_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_2_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showroom_name_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `person_1_designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_2_designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_1_mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_2_mobile_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_1_email_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `person_2_email_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_manager` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `key_ac_manager` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_clients_on_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (2,'neeraj','neeraj@ascratech.com','0','7666755466','',NULL,'2017-10-26 16:09:16','2017-10-28 09:16:06','ascra',NULL,0,'','Navi Mumbai','',NULL,'','',NULL,'0000-00-00','0000-00-00',NULL,NULL,NULL,'analyst',NULL,'','Open','C','','',NULL,'2017-10-26','','','','','','','',NULL,''),(3,'deepak','deepak@gmail.com','6','1234567893','',NULL,'2017-10-26 17:18:54','2017-10-28 14:17:56','honeywell',NULL,0,'','','',NULL,'','',NULL,'0000-00-00','0000-00-00',NULL,NULL,NULL,'',NULL,'1','Open','B','','',NULL,'2017-10-11','','','','','','','',NULL,'10'),(4,'Wilfred','wilfred@ascratech.com','3','9870462817','gfhfgj',NULL,'2017-10-27 11:48:40','2017-10-28 11:55:02','shilpi',NULL,400230,'gsdfgdf','fdsghdf','jgh',NULL,'2222222222222','222222222',NULL,'2017-10-20','2017-11-01',NULL,NULL,NULL,'Account Manager',NULL,'3','Open','A','gfh','dgdsgdfg',NULL,'2017-10-27','gfdhgdh','gdfsgdf','1111111111','2222222222','wilfred@ascratech.com','wilfred@ascratech.com','gdgds',NULL,'10'),(5,'alvin','neeraj@ascratech.com','6','4364631302','',NULL,'2017-10-27 15:40:50','2017-10-27 17:22:39','IT',NULL,0,'','','',NULL,'','',NULL,'0000-00-00','0000-00-00',NULL,NULL,NULL,'TESTER',NULL,'','Open','A','neeraj','',NULL,'2017-10-14','','','','','neeraj@ascratech.com','neeraj@ascratech.com','',NULL,''),(7,'solitair','niju@gmail.com','6','4561238977','',NULL,'2017-10-27 17:31:31','2017-10-28 14:35:06','rupa',NULL,0,'','','',NULL,'','',NULL,'0000-00-00','0000-00-00',NULL,NULL,NULL,'artist',NULL,'2','Open','A','','',NULL,'2017-10-13','','','','','','','',NULL,'10'),(8,'KC Kundan','p@test.com','5','1234567890',NULL,NULL,'2017-10-27 18:47:51','0000-00-00 00:00:00','Kundan Jwellers',NULL,NULL,NULL,'Mumbai',NULL,NULL,NULL,NULL,NULL,'2018-01-18','2014-02-15',NULL,NULL,NULL,'Designer',NULL,'3',NULL,'C',NULL,NULL,NULL,'2017-08-18',NULL,NULL,'1234567890',NULL,NULL,NULL,NULL,NULL,NULL),(10,'neeraj','neeraj@ascratech.com','6','7666755466','rupa solitare',NULL,'2017-10-28 08:32:51','2017-10-28 14:21:35','tata',NULL,174030,'mhape','navi mumbai','mh',NULL,'1234567899','2365456562',NULL,'0000-00-00','0000-00-00',NULL,NULL,NULL,'analyst',NULL,'2','Open','C','kunj','ram',NULL,'2017-10-26','sr. analyist','analyst','7666788766','7666792220','so@gmail.com','solitare@gmail.com','ok',NULL,''),(11,'alvin','alvin@gmail.com','5','7656755466','rupa solitare',NULL,'2017-10-28 09:01:10','2017-10-28 09:08:14','tata',NULL,174030,'mhape','navi mumbai','mh',NULL,'1234567899','2365456562',NULL,'1991-08-22','2017-10-25',NULL,NULL,NULL,'engg.',NULL,'2','Open','B','kunj','ram',NULL,'2017-10-25','sr. analyist','analyst','7666788766','7666792220','so@gmail.com','solitare@gmail.com','ok',NULL,''),(12,'alvin','alvin@gmail.com','5','7656755466','rupa solitare',NULL,'2017-10-28 14:33:04','2017-10-30 10:39:43','tata birla',NULL,174030,'mhape','navi mumbai','mh',NULL,'1234567899','2365456562',NULL,'1991-08-22','2017-10-25',NULL,NULL,NULL,'engg.',NULL,'2','Open','A','kunj','ram',NULL,'2017-10-25','sr. analyist','analyst','7666788766','7666792220','so@gmail.com','solitare@gmail.com','ok',NULL,''),(13,'a','a@a.com','6','9999999999','',NULL,'2017-10-28 20:48:18','0000-00-00 00:00:00','SLP',NULL,0,'','','',NULL,'','',NULL,'0000-00-00','0000-00-00',NULL,NULL,NULL,'a',NULL,'','Open','A','w','',NULL,'2017-10-28','','','','','','','',NULL,'');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `follow_ups`
--

DROP TABLE IF EXISTS `follow_ups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `follow_ups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci,
  `follow_up_date` date DEFAULT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_follow_ups_on_lead_id` (`lead_id`),
  KEY `index_follow_ups_on_client_id` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `follow_ups`
--

LOCK TABLES `follow_ups` WRITE;
/*!40000 ALTER TABLE `follow_ups` DISABLE KEYS */;
INSERT INTO `follow_ups` VALUES (1,'ewff','2017-09-27',NULL,'2017-10-26 17:49:57','0000-00-00 00:00:00',0,2,'Open'),(2,'hdjghjhgjhg','2017-10-27',NULL,'2017-10-27 12:01:17','0000-00-00 00:00:00',0,4,'Open'),(3,'testing','2017-10-27',NULL,'2017-10-27 12:30:30','0000-00-00 00:00:00',0,3,'Open'),(4,'','2017-10-06',NULL,'2017-10-27 17:50:21','0000-00-00 00:00:00',0,7,'Open'),(5,'','2017-10-04',NULL,'2017-10-27 17:50:35','0000-00-00 00:00:00',0,7,'Open'),(6,'','2017-10-11',NULL,'2017-10-28 14:36:10','0000-00-00 00:00:00',0,12,'Open');
/*!40000 ALTER TABLE `follow_ups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads`
--

DROP TABLE IF EXISTS `leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `client_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_leads_on_client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads`
--

LOCK TABLES `leads` WRITE;
/*!40000 ALTER TABLE `leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources`
--

DROP TABLE IF EXISTS `sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources`
--

LOCK TABLES `sources` WRITE;
/*!40000 ALTER TABLE `sources` DISABLE KEYS */;
INSERT INTO `sources` VALUES (1,'alvin','2017-10-26 13:08:23','0000-00-00 00:00:00'),(2,'Test','2017-10-26 16:36:06','2017-10-26 16:36:45'),(3,'Facebook','2017-10-27 11:44:15','0000-00-00 00:00:00'),(4,'Linkedin','2017-10-27 11:44:25','0000-00-00 00:00:00'),(5,'social media','2017-10-27 11:44:36','0000-00-00 00:00:00'),(6,'Visit','2017-10-27 11:44:45','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `sources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_expires_at` datetime DEFAULT NULL,
  `user_role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0-->for Inactive,1-->For Active',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin@shilpi.com','5f4dcc3b5aa765d61d8327deb882cf99',NULL,NULL,NULL,8,'2017-05-26 13:00:39','2017-04-26 12:11:14','110.227.255.154','120.63.178.6','2017-04-21 13:31:42','2017-05-26 13:00:39',NULL,NULL,NULL,'admin',NULL,'Admin',1),(10,'neeraj@ascratech.com','40be4e59b9a2a2b5dffb918c0e86b3d7',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'2017-10-26 13:38:31','0000-00-00 00:00:00',NULL,NULL,NULL,'neeraj',NULL,'User',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visits`
--

DROP TABLE IF EXISTS `visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8_unicode_ci,
  `visit_date` date DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `visit_done` tinyint(1) DEFAULT NULL COMMENT '0-False,1-True',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visits`
--

LOCK TABLES `visits` WRITE;
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
INSERT INTO `visits` VALUES (2,'','2017-09-27',3,'Open','2017-10-26 17:49:07','0000-00-00 00:00:00',0,1),(3,'gser','2017-10-10',2,'Open','2017-10-26 17:54:03','0000-00-00 00:00:00',0,0),(4,'nhgdkngfj','2017-10-26',4,'Open','2017-10-27 12:00:54','0000-00-00 00:00:00',0,1),(5,'jhgjhg','2017-10-31',4,'Open','2017-10-27 12:01:39','0000-00-00 00:00:00',0,0),(6,'','2017-10-18',4,'Open','2017-10-27 12:01:49','0000-00-00 00:00:00',0,0),(7,'few','2017-10-10',4,'Open','2017-10-27 12:10:30','0000-00-00 00:00:00',0,0),(8,'','2017-10-05',7,'Open','2017-10-27 17:50:11','0000-00-00 00:00:00',0,1),(9,'','2017-10-04',7,'Open','2017-10-28 14:35:33','0000-00-00 00:00:00',0,1),(10,'','2017-10-05',12,'Open','2017-10-28 14:36:02','0000-00-00 00:00:00',0,1),(11,'','2017-10-19',12,'Open','2017-10-28 14:38:26','0000-00-00 00:00:00',0,0);
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-30 13:22:08
